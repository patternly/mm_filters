package com.marketmemory.filters;

import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.FilterResult;

public abstract class TechnicalFilterIntersect extends Filter {

	/*
	 * Calculates all the results for the filter.
	 */
	public abstract FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception;
}
