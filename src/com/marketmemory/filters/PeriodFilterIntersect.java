package com.marketmemory.filters;

import java.util.ArrayList;
import java.util.List;

import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;

public abstract class PeriodFilterIntersect  extends Filter {
	
	protected abstract List<DateList> buildFilterDateList(BarIndexSeries indexSeries) throws Exception;

	/**
	 * Intersect a list of dateIndex to a list of dateIndex. Update the given BarIndexSeries, and return it.
	 * 
	 * @param original
	 * @param secondList
	 * @return
	 */
	public static BarIndexSeries periodIntersect(BarIndexSeries original, List<DateList> secondList) {
		
		if (original.getDateIndexesList() == null || (secondList.size() == 0)) {
			original.setDateIndexesList(secondList);
			return original;
		}
		
		if (original.getDateIndexesList().size() == 0) {
			return original;
		}
		
		List<DateList> firstList = original.getDateIndexesList();
		
		int firstPeriodLength = firstList.get(0).size();
		int secondPeriodLength = secondList.get(0).size();
		
		int minPeriodLength = ((firstPeriodLength  <= secondPeriodLength) ? firstPeriodLength : secondPeriodLength);
		
		List<DateList> resultList = new ArrayList<DateList>();
		
		int start = 0;
		for (int i = 0; i<firstList.size(); i++) {
			
			// Check if the current list is already after last item in the second list.
			if (firstList.get(i).getStart().isAfter(secondList.get(secondList.size() -1).getEnd())) {
				break;
			}
			
			// Check if the end of current list is before the first item in secondList.
			if (firstList.get(i).getEnd().isBefore(secondList.get(start).getStart())) 
				continue;
			
			for (int j = start; j<secondList.size(); j++) {
				//Check current item in secondList is still before the item in firstList.
				if(secondList.get(j).getEnd().isBefore(firstList.get(i).getStart())) {
					start = j;
					continue;
				}
				
				// Check current item in secondList is already after the item in firstList.
				if(secondList.get(j).getStart().isAfter(firstList.get(i).getEnd())) {
					break;
				}
				
				DateList current = firstList.get(i).intersect(secondList.get(j));
				
				if (current.size() == minPeriodLength) {
					resultList.add(current);
				}
			}
		}
		
		original.setDateIndexesList(resultList);
		return original;
	}
	
}