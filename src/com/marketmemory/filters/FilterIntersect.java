package com.marketmemory.filters;

import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.FilterResult;

public abstract class FilterIntersect extends Filter {
	
	/*
	 * Calculates all the results for the filter.
	 */
	public abstract FilterResult buildFilterResults(BarIndexSeries indexSeries) throws Exception;
}
