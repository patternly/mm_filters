package com.marketmemory.filters.event;

import com.marketmemory.filters.FilterIntersect;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.DatabaseIO;


public class EconomicEvent extends FilterIntersect {
	
	private String eventName;
	private DatabaseIO dbIO;
	
	/**
	 * 
	 * @param previousFilter
	 * @param eventName
	 * @param eventDBSeting The DBSetting by which we can building connection to get economic event days.
	 */
	public EconomicEvent(String eventName, DatabaseIO dbIO) {
		this.eventName = eventName;
		this.dbIO = dbIO;
	}
	
	private DateList buildFilterDateList(BarIndexSeries indexSeries) throws Exception {
		return dbIO.selectEconomicEvent(eventName, indexSeries.getStart(), indexSeries.getEnd());
	}

	@Override
	public FilterResult buildFilterResults(BarIndexSeries indexSeries) throws Exception {
		DateList dateList = buildFilterDateList(indexSeries);
		return new FilterResult(dateList, "EconomicEvent");
	}
}
