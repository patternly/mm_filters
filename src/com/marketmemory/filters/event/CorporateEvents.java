package com.marketmemory.filters.event;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

import com.marketmemory.filters.FilterIntersect;
import com.patternly.datasource.SymbolDataManager;
import com.patternly.datasource.barchart.CsvDividendBean;
import com.patternly.datasource.barchart.CsvEarningBean;
import com.patternly.datasource.barchart.CsvSplitBean;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.CorporateEventsType;


/**
 * The Corporate Events filter gets the Earnings, Dividend, Split, or
 * ReverseSplit days, and add those days to result date list.
 * 
 * @author Michael
 * 
 */
public class CorporateEvents extends FilterIntersect {

	private SymbolDataManager sdManager;
	private CorporateEventsType ceventsType;

	public CorporateEvents(SymbolDataManager sdManager, CorporateEventsType ceventsType) {
		this.sdManager = sdManager;
		this.ceventsType = ceventsType;
	}

	
	private DateList buildFilterDateList(BarIndexSeries indexSeries) throws Exception {
		DateList result = new DateList();
		List<DateTime> eventDates = new ArrayList<DateTime>();
		
		if (ceventsType.equals(CorporateEventsType.Earnings)) {
			eventDates = getEarningDates(indexSeries.getStart(), indexSeries.getEnd());
		} else if (ceventsType.equals(CorporateEventsType.Dividend)) {
			eventDates = getDividendDates(indexSeries.getStart(), indexSeries.getEnd());
		} else {
			eventDates = getSplitDates(indexSeries.getStart(), indexSeries.getEnd());
		}

		for (DateTime date : eventDates) {
			result.add(date);
		}

		return result;
	}

	private List<DateTime> getEarningDates(DateTime start, DateTime end) throws Exception {
		List<DateTime> earningDates = new ArrayList<DateTime>();
		List<CsvEarningBean> earninglist = sdManager.getEarnings(start, end);

		for (CsvEarningBean earning : earninglist) {
			earningDates.add(earning.getDatetime());
		}

		return earningDates;
	}

	private List<DateTime> getDividendDates(DateTime start, DateTime end) throws Exception {
		List<DateTime> dividendDates = new ArrayList<DateTime>();
		List<CsvDividendBean> dividendlist = sdManager.getDividend(start, end);

		for (CsvDividendBean dividend : dividendlist) {
			dividendDates.add(dividend.getDatetime());
		}

		return dividendDates;
	}

	private List<DateTime> getSplitDates(DateTime start, DateTime end) throws Exception {
		List<DateTime> splitDates = new ArrayList<DateTime>();
		List<CsvSplitBean> earninglist = sdManager.getSplit(start, end);

		for (CsvSplitBean split : earninglist) {
			if (getSplitType(split.getSplit()).equals(ceventsType)) {
				splitDates.add(split.getDatetime());
			}
		}

		return splitDates;
	}

	/**
	 * Get the split type form given split string. When split into more shares,
	 * the split type is "Slipt", Example: "1-2". When split into less shares,
	 * the split type is "ReverseSlipt", Example: "2-1".
	 * 
	 * @param split
	 * @return CorporateEventsType
	 */
	private CorporateEventsType getSplitType(String split) {
		// parse the given string into two integers.
		int splitFrom = Integer.parseInt(split.substring(0, split.indexOf('-')));
		int splitTo = Integer.parseInt(split.substring(split.indexOf('-') + 1, split.length()));

		if (splitFrom < splitTo) {
			return CorporateEventsType.Split;
		}

		return CorporateEventsType.ReverseSplit;
	}

	@Override
	public FilterResult buildFilterResults(BarIndexSeries indexSeries) throws Exception {
		DateList dateList = buildFilterDateList(indexSeries);
		return new FilterResult(dateList, "CorporateEvents");
	}

}
