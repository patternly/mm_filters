package com.marketmemory.filters.event;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import com.marketmemory.filters.FilterIntersect;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;


public class OptionExpiration extends FilterIntersect {
	public OptionExpiration() {
	}
	
	private DateTime getThirdFriday(DateTime dt) {
		DateTime firstFriday = dt.dayOfMonth().withMinimumValue().withDayOfWeek(DateTimeConstants.FRIDAY);
		if(!firstFriday.monthOfYear().equals(dt.monthOfYear())) {
			firstFriday = firstFriday.plusDays(7);
		}
		DateTime thirdFriday = firstFriday.plusWeeks(2);
		
		return thirdFriday;
	}
	
	private DateList buildFilterDateList(BarIndexSeries indexSeries) {
		DateTime start = indexSeries.getStart();
		DateList result = new DateList();
		DateTime tmp = getThirdFriday(start);
		if(!tmp.isBefore(start)) 
			result.add(tmp);
		tmp = getThirdFriday(tmp.plusMonths(1));
		while(!tmp.isAfter(indexSeries.getEnd())) {
			result.add(tmp);
			tmp = getThirdFriday(tmp.plusMonths(1));
		}
		
		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarIndexSeries indexSeries) throws Exception {
		DateList dateList = buildFilterDateList(indexSeries);
		return new FilterResult(dateList, "OptionExpiration");
	}
}
