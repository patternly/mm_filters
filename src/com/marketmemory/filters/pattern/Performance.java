package com.marketmemory.filters.pattern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.marketmemory.filters.FilterIntersect;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.PeriodType;
import com.patternly.util.SimplePeriod;

/**
 * The Performance Filter calculates the Rate Of Change
 * ((last day closing/first day closing)-1)*100 for different time period, and build a date list
 * with last day of each period dates that the corresponding rate of change fits
 * user settings.
 * 
 * @author Michael
 * 
 */
public class Performance extends FilterIntersect {
	private double percentage;
	private SimplePeriod period;
	private double recoveryPercentage = 0.0;
	
	// 200 trading days are approximate 1 year.
	final int MAX_LOOK_BACK_PERIOD = 200;

	public Performance(SimplePeriod period, double percentage) throws Exception {

		if (period.getValue() < 1)
			throw new IllegalArgumentException("The time period must be greater than or equal to 1");
		this.period = period;

		this.percentage = percentage;
	}
	
	public Performance(SimplePeriod period, double percentage, double recoveryPercentage) throws Exception {

		if (period.getValue() < 1)
			throw new IllegalArgumentException("The time period must be greater than or equal to 1");
		this.period = period;
		
		this.percentage = percentage;
		
		if ((percentage > 0 && recoveryPercentage > 0) || (percentage < 0 && recoveryPercentage < 0)) {
			throw new IllegalArgumentException("The performance percentage and recoveryPercentage should be opposite sign.");
		}
		
		this.recoveryPercentage = recoveryPercentage;
	}

	private DateList buildFilterDateList(BarIndexSeries indexSeries) throws Exception {
		DateList result = new DateList();
		BarDataWrapper rawData = indexSeries.getBarSeries();
		
		// The closeIndex is to save the original index in rawData.
		int[] closeIndex = new int[rawData.size()];

		double[] out = computeRateOfChange(rawData, closeIndex);
		
		List<Integer> resultIndexStart = new ArrayList<Integer>();
		List<Integer> resultIndexEnd = new ArrayList<Integer>();
		
		for (int i = 0; i < out.length; i++) {

			if ((percentage >= 0 && percentage < out[i]) ||
					(percentage < 0 && percentage > out[i])) {

				result.add(rawData.getDateTime()[closeIndex[i]]);				
				resultIndexStart.add(closeIndex[i - period.getValue() + 1]);
				resultIndexEnd.add(i);				
			}
		}
		
		if (recoveryPercentage != 0.0 && result.size()>0) {
			result = applyRecoveryPercentage(result, rawData, resultIndexStart, resultIndexEnd);
		}

		return result;
	}

	private DateList applyRecoveryPercentage(DateList result, BarDataWrapper rawData, List<Integer> resultIndexStart,
			List<Integer> resultIndexEnd) throws Exception {
		
		double[] rawClose = rawData.getClose();
		
		// If two period overlaps, remove the first period if it starts lower than second period in negative performance
		// or highter than second period in positive performance.
		for(int i = 1; i < result.size(); i++) {
			
			if (resultIndexStart.get(i) < resultIndexEnd.get(i-1)) {
				
				if ((percentage >= 0 && rawClose[resultIndexStart.get(i)] < rawClose[resultIndexStart.get(i-1)]) ||
						(percentage < 0 && rawClose[resultIndexStart.get(i)] > rawClose[resultIndexStart.get(i-1)])) {
					
					result.remove(i-1);
					resultIndexStart.remove(i-1);
					resultIndexEnd.remove(i-1);
					i--;
				}
			}
		}
		
		for(int i = 1; i < result.size(); i++) {
			if(resultIndexStart.get(i) - resultIndexStart.get(i-1) < MAX_LOOK_BACK_PERIOD) {
				if(!meetsRecoveryPercentage(rawClose, resultIndexStart.get(i), resultIndexStart.get(i-1))) {
					result.remove(i);
					resultIndexStart.remove(i);
					resultIndexEnd.remove(i);
					i--;
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Check if the given period meets recovery percentage.
	 * 
	 * @param rawClose
	 * @param secondStart
	 * @param firstStart
	 * @return
	 */
	private boolean meetsRecoveryPercentage(double[] rawClose, int secondStart, int firstStart) {
		for(int i = secondStart-firstStart; i>=1; i--) {
			
			int periodCount = secondStart-firstStart - i + 1;
			
			for (int j=firstStart; j<firstStart+periodCount; j++) {
				
				double change = (rawClose[j+i] / rawClose[j] - 1) * 100;
				
				if ((percentage >= 0 && change <= recoveryPercentage) ||
						(percentage < 0 && change >= recoveryPercentage)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Compute the rate of change based on time period, and return an array of
	 * percentage results for each time period. The length of the result array
	 * is different from the rawData. i.e. for time period is one week, the
	 * result only have a percentage change for each week.
	 * 
	 * @param rawData
	 * @param openIndex
	 * @param closeIndex
	 * @return
	 * @throws Exception
	 */
	private double[] computeRateOfChange(BarDataWrapper rawData, int[] closeIndex) throws Exception {
		double[] out = new double[rawData.size()];
		double[] periodClose = new double[rawData.size()];

		int periodCount = buildPeriodArrays(rawData, periodClose, closeIndex);
		int offset = period.getValue();

		for (int i = 0; i < periodCount-offset; i++) {
			out[i + offset] = ((periodClose[i + offset] / periodClose[i]) - 1) * 100;
		}

		return Arrays.copyOf(out, periodCount);
	}
	
	/**
	 * Build close arrays for each time period.
	 * 
	 * @param rawData
	 * @param periodClose
	 * @param periodIndex
	 * @return
	 * @throws Exception
	 */
	private int buildPeriodArrays(BarDataWrapper rawData, double[] periodClose, int[] periodIndex) throws Exception {
		int count = 0;
		DateTime currentDate = rawData.getDateTime()[0];

		for (int i = 1; i < rawData.size(); i++) {
			if (isInSamePeriod(rawData.getDateTime()[i], currentDate))
				continue;

			periodClose[count] = rawData.getClose()[i - 1];
			periodIndex[count] = i - 1;

			currentDate = rawData.getDateTime()[i];
			count++;
		}
		periodClose[count] = rawData.getClose()[rawData.size() - 1];
		periodIndex[count] = rawData.size() - 1;
		count++;

		return count;
	}

	/**
	 * Check the two given dates are in same time period.
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	private boolean isInSamePeriod(DateTime date1, DateTime date2) {
		LocalDate ldate1 = date1.toLocalDate();
		LocalDate ldate2 = date2.toLocalDate();

		if (period.getType() == PeriodType.WEEK) {
			return (ldate1.withDayOfWeek(1).equals(ldate2.withDayOfWeek(1)));

		} else if (period.getType() == PeriodType.MONTH) {
			return ldate1.withDayOfMonth(1).equals(ldate2.withDayOfMonth(1));
		}

		// Period type is day in this case.
		return false;
	}

	@Override
	public FilterResult buildFilterResults(BarIndexSeries indexSeries) throws Exception {
		if(indexSeries.size() ==0) 
			return new FilterResult(indexSeries.getDateList(), "Performance");
		DateList dateList = buildFilterDateList(indexSeries);
		return new FilterResult(dateList, "Performance");
	}

}
