package com.marketmemory.filters.pattern;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.CandlestickPatternsType;


/**
 * The Candlestick Patterns filter finds the user specified patterns and add
 * corresponding dates to result date list.
 * 
 * @author Michael
 * 
 */
public class CandlestickPatterns extends TechnicalFilterIntersect {

	private TAlib ta;
	private CandlestickPatternsType patternName;
	private String patternfilterType;

	public CandlestickPatterns(TAlib ta, CandlestickPatternsType patternType,
			String patternfilterType) {
		this.ta = ta;
		this.patternName = patternType;

		if (patternfilterType.toLowerCase().equals("bearish")) {
			this.patternfilterType = "bearish";
		} else if (patternfilterType.toLowerCase().equals("bullish")) {
			this.patternfilterType = "bullish";
		} else if (patternfilterType.toLowerCase().equals("both")) {
			this.patternfilterType = "both";
		} else {
			throw new IllegalArgumentException("No such candlestick pattern filter type: " + patternfilterType);
		}
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();

		Map<String, Object> taInputs = new HashMap<String, Object>();
		taInputs.put("patterntype", this.patternName);

		Map<String, Object> outputs = ta.compute(taInputs);

		int[] taOutput;

		if (outputs.size() > 0) {
			taOutput = (int[]) outputs.get(patternName.toString());
		} else {
			taOutput = new int[] {};
		}

		for (int i = 0; i < ta.getResultLength(); i++) {
			int index = i + ta.getIndexBegin();

			if ((patternfilterType.equals("bearish") && taOutput[i] < 0) ||
					(patternfilterType.equals("bullish") && taOutput[i] > 0) ||
					(patternfilterType.equals("both") && taOutput[i] != 0)) {
				result.add(rawData.getDateTime()[index]);
			}
		}

		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		return new FilterResult(dateList, "CandlestickPatterns");
	}
}
