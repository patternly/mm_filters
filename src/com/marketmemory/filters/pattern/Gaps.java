package com.marketmemory.filters.pattern;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;

public class Gaps extends TechnicalFilterIntersect {
	private double gapUp;
	private double gapDown;
	private TAlib ta;
	private double[] gaps;
	
	
	public Gaps(TAlib ta, double gapUp, double gapDown)
			throws Exception {
		this.ta = ta;
		this.gapUp = gapUp;
		this.gapDown = gapDown;
		if (gapDown == emptyValue && gapUp == emptyValue)
			throw new Exception("No such type exists in gap filter");
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		this.gaps = ta.compute(1);

		for (int i = 0; i < this.gaps.length; i++) {
			int index = i + 1;
			if ((gapDown == emptyValue && gapUp != emptyValue && this.gaps[i] >= gapUp) ||
					(gapDown != emptyValue && gapUp == emptyValue && this.gaps[i] <= gapDown) ||
					((gapDown != emptyValue && gapUp != emptyValue) && (this.gaps[i] <= gapUp && this.gaps[i] >= gapDown))) {
				result.add(rawData.getDateTime()[index]);
			}
			else {
			}
		}
		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		
		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("gapResult", this.gaps);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "Gaps");
	}
}
