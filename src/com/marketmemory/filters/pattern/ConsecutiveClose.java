package com.marketmemory.filters.pattern;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.marketmemory.filters.FilterIntersect;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.PeriodType;
import com.patternly.util.SimplePeriod;

/**
 * The Consecutive Close filter calculates the rate of change of consecutive
 * up/down for periods of days. It build a data list with dates that the
 * corresponding rate of change fits user settings.
 * 
 * @author Michael
 * 
 */
public class ConsecutiveClose extends FilterIntersect {

	private double percentage;
	private SimplePeriod consecutivePeriod;
	private boolean directionUp;

	public ConsecutiveClose(SimplePeriod consecutivePeriod, boolean direction, double percentage) {

		if (consecutivePeriod.getValue() < 1)
			throw new IllegalArgumentException("The time period must be greater than or equal to 1");
		this.consecutivePeriod = consecutivePeriod;

		if (percentage < 0)
			throw new IllegalArgumentException("The percentage must be greater than or equal to 0");
		this.percentage = percentage;

		this.directionUp = direction;
	}

	private DateList buildFilterDateList(BarIndexSeries indexSeries) throws Exception {
		DateList result = new DateList();
		BarDataWrapper rawData = indexSeries.getBarSeries();

		int[] conscloseIndex = new int[rawData.size()];
		int[] periodIndex = new int[rawData.size()];
		double[] out = computeConsecutivePeriodChange(rawData, conscloseIndex, periodIndex);

		for (int i = 0; i < out.length; i++) {
			if ((directionUp && percentage < out[i]) ||
					(!directionUp && ((0 - percentage) > out[i]))) {
				
				// Add last day of each period.
				result.add(rawData.getDateTime()[periodIndex[conscloseIndex[i] + consecutivePeriod.getValue() - 1]]);
			}
		}

		return result;
	}

	/**
	 * Compute the rate of change for consecutive up/down period days.
	 * 
	 * @param rawData
	 * @param conscloseIndex
	 * @return
	 * @throws Exception
	 */
	private double[] computeConsecutivePeriodChange(BarDataWrapper rawData, int[] conscloseIndex, int[] periodIndex)
			throws Exception {
		double[] periodClose = new double[rawData.size()];

		int periodCount = buildPeriodCloseArray(rawData, periodClose, periodIndex);

		int consPeriodCount = getConsecutivePeriodIndexes(conscloseIndex, periodClose, periodCount);
		double[] out = new double[consPeriodCount];

		for (int i = 0; i < consPeriodCount; i++) {
			out[i] = (periodClose[conscloseIndex[i] + consecutivePeriod.getValue() - 1]
					/ periodClose[conscloseIndex[i] - 1] - 1) * 100;
		}

		return out;
	}

	/**
	 * Get the index of all consecutive up/down period from closing array.
	 * 
	 * @param rawData
	 * @param closeIndex
	 * @return
	 * @throws Exception
	 */
	private int getConsecutivePeriodIndexes(int[] closeIndex, double[] periodClose, int periodCount) throws Exception {

		int count = 0;
		boolean consecutive = true;

		int offset = consecutivePeriod.getValue();

		for (int i = 1; i <= periodCount - offset; i++) {

			for (int j = 0; j < offset; j++) {
				if ((directionUp && periodClose[i - 1 + j] > periodClose[i + j]) ||
						(!directionUp && periodClose[i - 1 + j] < periodClose[i + j])) {
					consecutive = false;
					break;
				}
			}

			if (consecutive) {
				closeIndex[count] = i;
				count++;
			}

			consecutive = true;
		}

		return count;
	}

	private int buildPeriodCloseArray(BarDataWrapper rawData, double[] periodClose, int[] periodIndex) throws Exception {

		int count = 0;
		DateTime currentDate = rawData.getDateTime()[0];

		for (int i = 1; i < rawData.size(); i++) {

			if (isInSamePeriod(rawData.getDateTime()[i], currentDate))
				continue;

			periodClose[count] = rawData.getClose()[i - 1];
			periodIndex[count] = i - 1;

			currentDate = rawData.getDateTime()[i];
			count++;
		}
		
		periodClose[count] = rawData.getClose()[rawData.size() - 1];
		periodIndex[count] = rawData.size() - 1;
		count++;

		return count;
	}

	/**
	 * Check the two given dates are in same time period.
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	private boolean isInSamePeriod(DateTime date1, DateTime date2) {
		LocalDate ldate1 = date1.toLocalDate();
		LocalDate ldate2 = date2.toLocalDate();

		if (consecutivePeriod.getType() == PeriodType.WEEK) {
			return (ldate1.withDayOfWeek(1).equals(ldate2.withDayOfWeek(1)));

		} else if (consecutivePeriod.getType() == PeriodType.MONTH) {
			return ldate1.withDayOfMonth(1).equals(ldate2.withDayOfMonth(1));
		}

		// Period type is day in this case.
		return false;
	}

	@Override
	public FilterResult buildFilterResults(BarIndexSeries indexSeries) throws Exception {
		if(indexSeries.size() ==0) 
			return new FilterResult(indexSeries.getDateList(), "ConsecutiveClose");
		DateList dateList = buildFilterDateList(indexSeries);
		return new FilterResult(dateList, "ConsecutiveClose");
	}
}
