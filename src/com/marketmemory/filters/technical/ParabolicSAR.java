package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
 * The ParabolicSAR filter builds and returns a date list of all the 
 * dates with closed price higher/lower than SAR
 * 
 * @author Johnny Jiang
 *
 */
public class ParabolicSAR extends TechnicalFilterIntersect {
	private TAlib ta;
	private double baseAF;
	private double maxAF;
	private Position position;
	private int start, end = Integer.MAX_VALUE;
	private static final double MAX_VALUE = 3e+037;

	private double[] parabolicSAR;
	
	/**
	 * 
	 * @param ta
	 * @param baseAF
	 * 		-- Base Acceleration Factor; A.K.A "Step"; the unit increment set by user
	 * @param maxAF
	 * 		-- Maximum Acceleration Factor; the roof value that AF can reach set by user
	 * @param position
	 * @param start
	 * @throws Exception
	 */
	public ParabolicSAR(TAlib ta, double baseAF, double maxAF, Position position, 
			int start) throws Exception {
		this.ta = ta;
		
		if(maxAF < baseAF){
			throw new IllegalArgumentException("Maximum AF must be greater than AF");
		}
		
		if(baseAF <= 0 || baseAF > MAX_VALUE){
			throw new IllegalArgumentException("Invalid value of Acceleration Factor");
		}
		this.baseAF = baseAF;
		
		if(maxAF <= 0 || maxAF > MAX_VALUE){
			throw new IllegalArgumentException("Invalid value of maximum Acceleration Factor");
		}
		this.maxAF = maxAF;
		
		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;
		
		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
	}
	
	public ParabolicSAR(TAlib ta, double baseAF, double maxAF, Position position, 
			int start, int end) throws Exception {
		this(ta, baseAF, maxAF, position, start);
		
		if (end < start)
			throw new IllegalArgumentException("End date cannot be smaller than start date!");
		this.end = end;
	}
	
	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception{
		DateList result = new DateList();
		
		Map<String, Object> inputs = new HashMap<String, Object>();
		inputs.put("baseAF", this.baseAF);
		inputs.put("maxAF", this.maxAF);
		
		Map<String, Object> outputs = ta.compute(inputs);
		
		if(outputs.size() > 0){
			this.parabolicSAR = (double[]) outputs.get("parabolicSAR");
		} else{
			this.parabolicSAR = new double[] {};
		}
		
		result = buildResult(rawData, this.parabolicSAR);
		
		return result;
	}
	
	/**
	 * 
	 * @param rawData
	 * @param parabolicSAR
	 * @param isSignalLine
	 * @return
	 */
	private DateList buildResult(BarDataWrapper rawData, double[] parabolicSAR) {
		// TODO Auto-generated method stub
		DateList result = new DateList();
		int consecutiveDay = 0;

			for (int i = 0; i < parabolicSAR.length; i++) {
				int index = i + ta.getIndexBegin();

				if ((position.equals(Position.above) && rawData.getClose()[index] > parabolicSAR[i])
						|| (position.equals(Position.below) && rawData.getClose()[index] < parabolicSAR[i] )) {
					consecutiveDay++;

					if (consecutiveDay >= this.start && consecutiveDay <= this.end)
						result.add(rawData.getDateTime()[index]);
				} else
					consecutiveDay = 0;
			}
		
		return result;
	}
	
	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("parabolicSAR", this.parabolicSAR);

		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "ParabolicSAR");
	}
}