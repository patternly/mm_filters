package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
 * AroonOscillator builds results based on user's setting. The filter returns a
 * date list of AroonOscillator either [above] or [below] user defined signal
 * line value.
 * 
 * 
 * @author Johnny Jiang
 * 
 */
public class AroonOscillator extends TechnicalFilterIntersect {
	private TAlib ta;
	private int timePeriod;
	private double signalLine;
	private Position position;
	private int start, end = Integer.MAX_VALUE;

	private double[] aroonOscillator;
	
/**
* Aroon Oscillator Constructor.
* 
* @param ta
* @param timePeriod
*            The period to calculate aroon up and aroon down.
* @param signalLine
*            A constant value line set by user to compare with aroon
*            oscillator line.
* @param position
*            Specify to select arron oscillator line is above or bellow
*            signalLine
* @param isSignalLine
*            If not SignalLine, the default signalLine value is 0.
* @param start
* @throws Exception
 */
	public AroonOscillator(TAlib ta, int timePeriod, double signalLine, Position position, int start) throws Exception {

		this.ta = ta;

		if (timePeriod < 2 || timePeriod > 100000) {
			throw new IllegalArgumentException("Time period must be greater than 1");
		}
		this.timePeriod = timePeriod;

		if (signalLine > 100 || signalLine < -100) {
			throw new IllegalArgumentException("Signal Line must be between -100 and 100");
		}
		this.signalLine = signalLine;

		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;


	}

	public AroonOscillator(TAlib ta, int timePeriod, double signalLine, Position position,
			int start, int end) throws Exception {
		this(ta, timePeriod, signalLine, position, start);

		if (end < start)
			throw new IllegalArgumentException("End date cannot be smaller than start date!");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();

		Map<String, Object> period = new HashMap<String, Object>();
		period.put("timePeriod", this.timePeriod);

		Map<String, Object> outputs = ta.compute(period);

		if (outputs.size() > 0) {
			this.aroonOscillator = (double[]) outputs.get("aroonOscillator");
		} else {
			this.aroonOscillator = new double[] {};
		}

		result = buildResult(rawData, this.aroonOscillator, this.signalLine);

		return result;
	}

	/**
	 * Build date list & choose weather to filter dates according value of
	 * isSignalLine
	 * 
	 * @param rawData
	 * @param aroonOscillator
	 * @param signalLine
	 * @param isSignalLine
	 * @return
	 * @throws Exception
	 */
	private DateList buildResult(BarDataWrapper rawData, double[] aroonOscillator, double signalLine) throws Exception {
		DateList result = new DateList();
		int consecutiveDay = 0;
		for (int i = 0; i < aroonOscillator.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && aroonOscillator[i] > signalLine)
					|| (position.equals(Position.below) && aroonOscillator[i] < signalLine)) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			} else
				consecutiveDay = 0;
		}
		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("aroonOscillator", this.aroonOscillator);

		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "AroonOscillator");
	}
}