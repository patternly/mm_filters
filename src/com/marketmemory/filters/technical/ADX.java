package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
+	 * Author: Yang Meng
+	 * ADX Constructor.
+	 * 
+	 * @param ta
+	 * @param period
+	 *            The period to calculate ADX , plus_DI, or minus_DI.
+	 * @param isCrossOver
+	 *            True means plusDI AND minus DI crossover mode
+	 *            False means ADX value mode 
+	 * @param position
+	 *            if isCrossOver is True, position.above selects where minus_DI is above plus_DI,
+	 *            position.below selects where minus_DI is under plus_DI.
+	 *            if isCrossOver is False, position.above will select where ADX is higher than upperLine
+	 *            position.below select where ADX is below lowerLine
+    *@param lowerLine
+	 *            if ADX lower than this value, there is no trend. recommended value is 20
+    *@param upperLine
+	 *            if ADX higher than this value, there is strong trend. recommended value is 25
+	 *            
+	 * @param start
+	 * @throws Exception
+	 */
public class ADX extends TechnicalFilterIntersect {
	private TAlib ta;
	
	private int period;
	private boolean isCrossOver; 	
	private Position position;  
	
	private int lowerLine;
	private int upperLine; 
	
	private int start, end = Integer.MAX_VALUE;
	
	private double[] ADXResult;
	private double[] minus_DI;
	private double[] plus_DI;

	public ADX(TAlib ta, int period, boolean isCrossOver, Position position, int lowerLine,int upperLine,
			 int start) throws Exception {
		this.ta = ta;

		if (period <= 1) {
			throw new IllegalArgumentException("Periods must be larger than 1");
		}
		this.period = period;
		
		if (upperLine >= 100 || upperLine <=0 || lowerLine >= 100 || lowerLine <=0){
			throw new IllegalArgumentException("upper and lower line must between 0 to 100");
		}
		
		if (upperLine <= lowerLine ){
			throw new IllegalArgumentException("upper line must larger than lower line");
		}
		
		this.upperLine = upperLine;
		this.lowerLine = lowerLine;
		
		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;

		this.isCrossOver = isCrossOver;
	}

	public ADX(TAlib ta, int period,
			boolean isCrossOver, Position position, int lowerLine,int upperLine,  int start, int end) throws Exception {
		this(ta, period, isCrossOver, position,  lowerLine,upperLine, start);

		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}
	
	
	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		
		Map<String, Object> inputs = new HashMap<String, Object>();
		inputs.put("period", this.period);
		inputs.put("isCrossOver", this.isCrossOver);
		
		Map<String, Object> outputs = ta.compute(inputs);
		
		

		if (outputs.size() > 0) {
			this.ADXResult = (double[]) outputs.get("ADXResult");
			this.minus_DI = (double[]) outputs.get("minus_DI");
			this.plus_DI = (double[]) outputs.get("plus_DI");
		} else {
			this.ADXResult = new double[] {};
			this.minus_DI = new double[] {};
			this.plus_DI = new double[] {};
		}

		if (isCrossOver) {
			result = buildMethod1Result(rawData, this.minus_DI, this.plus_DI);
		} else {
			result = buildMethod0Result(rawData, this.ADXResult);
		}

		return result;
	}
	
	private DateList buildMethod1Result(BarDataWrapper rawData, double[] minus_DI, double[] plus_DI)
		throws Exception{
		
		DateList method1Result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < minus_DI.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && minus_DI[i] > plus_DI[i]) ||
					(position.equals(Position.below) && minus_DI[i] < plus_DI[i])) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					method1Result.add(rawData.getDateTime()[index]);
			}
			else
				consecutiveDay = 0;
		}

		return method1Result;
		
	}
	
	private DateList buildMethod0Result(BarDataWrapper rawData, double[] ADXResult)
			throws Exception{
			
			DateList method0Result = new DateList();
			int consecutiveDay = 0;

			for (int i = 0; i < ADXResult.length; i++) {
				int index = i + ta.getIndexBegin();

				if ( (position.equals(Position.above) && ADXResult[i] > upperLine ) ||
						(position.equals(Position.below) && ADXResult[i] < lowerLine)	) {
					consecutiveDay++;

					if (consecutiveDay >= this.start && consecutiveDay <= this.end)
						method0Result.add(rawData.getDateTime()[index]);
				}
				else
					consecutiveDay = 0;
			}

			return method0Result;
			
			
		}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		
		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("ADXResult", this.ADXResult);
		results.put("minus_DI", this.minus_DI);
		results.put("plus_DI", this.plus_DI);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "ADX");
	}


}
