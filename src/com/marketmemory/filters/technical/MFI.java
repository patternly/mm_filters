package com.marketmemory.filters.technical;

import com.marketmemory.filters.TechnicalFilterIntersect;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
 * 
 * Filter of Money Flow Index(MFI)
 * 
 * The Money Flow Index (MFI) is an oscillator that uses both price and volume
 * to measure buying and selling pressure. MFI is also known as volume-weighted
 * RSI. MFI starts with the typical price for each period. Money flow is
 * positive when the typical price rises (buying pressure) and negative when the
 * typical price declines (selling pressure). A ratio of positive and negative
 * money flow is then plugged into an RSI formula to create an oscillator that
 * moves between zero and one hundred. As a momentum oscillator tied to volume,
 * the Money Flow Index (MFI) is best suited to identify reversals and price
 * extremes with a variety of signals.
 * 
 * @author Yangqiao Meng
 *
 */
public class MFI extends TechnicalFilterIntersect {

	private TAlib ta;
	private int period;
	private int overboughtLine;
	private int oversoldLine;
	private Position position;

	private int start, end = Integer.MAX_VALUE;

	private double[] MFIResult;

	/**
	 * 
	 * @param ta
	 * @param period
	 *            : look-back period
	 * @param overboughtLine
	 *            : the threshold value for over-bought, default:80
	 * @param oversoldLine
	 *            : the threshold value for over-sold, default:20
	 * @param position
	 *            : position=above will select over-bought region,
	 *            position=below will select over-sold region
	 * @param start
	 * @throws Exception
	 */
	public MFI(TAlib ta, int period, int overboughtLine, int oversoldLine, Position position, int start)
			throws Exception {
		this.ta = ta;

		if (period <= 1) {
			throw new IllegalArgumentException("Period must be larger than 1");
		}
		this.period = period;

		if (oversoldLine < 0)
			throw new IllegalArgumentException("Over-sold line must be larger than 0");
		else if (overboughtLine > 100)
			throw new IllegalArgumentException("Over-bought line must be less than 100");
		else if (overboughtLine < oversoldLine)
			throw new IllegalArgumentException("Over-bought line must be larger than over-sold line");

		this.overboughtLine = overboughtLine;
		this.oversoldLine = oversoldLine;

		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;

	}

	public MFI(TAlib ta, int period, int overboughtLine, int oversoldLine, Position position, int start, int end)
			throws Exception {

		this(ta, period, overboughtLine, oversoldLine, position, start);

		if (start >= end)
			throw new IllegalArgumentException("end should not be smaller than start");
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {

		DateList result = new DateList();
		this.MFIResult = ta.compute(period);
		int consecutiveDay = 0;

		for (int i = 0; i < this.MFIResult.length; i++) {

			int index = i + ta.getIndexBegin();
			if ((MFIResult[i] > overboughtLine && position.equals(Position.above))
					|| (MFIResult[i] < oversoldLine && position.equals(Position.below))) {

				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);

			} else
				consecutiveDay = 0;

		}
		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("MFIResult", MFIResult);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "MFI");
	}
}
