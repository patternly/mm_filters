package com.marketmemory.filters.technical;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;

/**
 * The moving average crossovers filter calculates sma for two different
 * periods, and build a date list with dates that
 * 
 * @author Michael
 * 
 */
public class MACrossovers extends TechnicalFilterIntersect {
	private TAlib ta;
	
	private boolean isSMA;
	private int fastPeriod;
	private int slowPeriod;

	private boolean isCrossesOver;
	private int start, end = Integer.MAX_VALUE;

	double[] fastResult;
	double[] slowResult;

	int indexBegin;
	int resultLength;

	public MACrossovers(TAlib ta, boolean isSMA, int fastPeriod, int slowPeriod, boolean isCrossesOver,
			int start) {

		this.ta = ta;
		
		this.isSMA = isSMA;
		if (fastPeriod <= 1 || slowPeriod <= 1) {
			throw new IllegalArgumentException("Period must be larger than 1");
		}

		if (fastPeriod >= slowPeriod) {
			throw new IllegalArgumentException("Fast period must be smaller than slow period.");
		}
		this.fastPeriod = fastPeriod;
		this.slowPeriod = slowPeriod;

		this.isCrossesOver = isCrossesOver;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
	}

	public MACrossovers(TAlib ta, boolean isSMA, int fastPeriod, int slowPeriod, boolean isCrossesOver,
			int start, int end) {
		this(ta, isSMA, fastPeriod, slowPeriod, isCrossesOver, start);
		this.ta = ta;

		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();

		// Get sma/ema result for fast period.
		this.fastResult = ta.compute(this.fastPeriod);
		int fastResultLength = ta.getResultLength();

		// Get sma/ema result for slow period.
		this.slowResult = ta.compute(this.slowPeriod);
		this.indexBegin = ta.getIndexBegin();
		this.resultLength = ta.getResultLength();

		//Chop the fast period result to have the same length as slow period result.
		this.fastResult = Arrays.copyOfRange(fastResult, (fastResultLength - this.resultLength), fastResult.length);

		int consecutiveDay = 0;

		for (int i = 0; i < this.resultLength; i++) {
			int index = i + this.indexBegin;

			if ((isCrossesOver && this.fastResult[i] > this.slowResult[i])
					|| (!isCrossesOver && this.fastResult[i] < this.slowResult[i])) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end) {
					result.add(rawData.getDateTime()[index]);
				}
			} else {
				consecutiveDay = 0;
			}
		}

		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("fastResult", this.fastResult);
		results.put("slowResult", this.slowResult);
		return new FilterResult(dateList, results, this.indexBegin, this.resultLength, "MACrossovers");
	}

}
