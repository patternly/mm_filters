package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.Filter;
import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
 * Retrieve the computing results from DEMACompute, and builds a 
 * date list based on user setting. The date list is consisted of
 * either dates with close prices above DEMA, or close prices below DEMA.
 *  
 * 
 * @author Johnny Jiang
 *
 */
public class DEMA extends TechnicalFilterIntersect {
	private TAlib ta;
	private int timePeriod;
	private Position position;
	private int start, end = Filter.maxInteger;
	private double[] dema;
	private static final int MAX_VALUE = 100000;
	
	/**
	 * 
	 * @param ta
	 * @param timePeriod
	 * 		-- look back period
	 * @param position
	 * 		-- whether close price is above/below the computed DEMA
	 * @param start
	 * @throws Exception
	 */
	public DEMA(TAlib ta, int timePeriod, Position position, int start) throws Exception {
		this.ta = ta;

		if (timePeriod < 2 || timePeriod > MAX_VALUE) {
			throw new IllegalArgumentException("time period must be at least 2");
		}
		this.timePeriod = timePeriod;

		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
	}

	public DEMA(TAlib ta, int timePeriod, Position position, int start, int end) throws Exception {
		this(ta, timePeriod, position, start);
		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		this.dema = ta.compute(timePeriod);

		int consecutiveDay = 0;
		for (int i = 0; i < this.dema.length; i++) {
			int index = i + ta.getIndexBegin();
			if ((position.equals(Position.above) && rawData.getClose()[index] > this.dema[i])
					|| (position.equals(Position.below) && rawData.getClose()[index] < this.dema[i])) {
				consecutiveDay++;
				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			} else
				consecutiveDay = 0;
		}

		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("DEMA", this.dema);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "DEMA");
	}

}
