package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
+	 * Author: Yang Meng
+	 * Chande Momentum Oscillator (CMO) Constructor.
+	 * 
+	 * @param ta
+    * @param period: look-back period
+	 * @param limit
+	 *            Only useful if isCentral Line= False, it is the threshold value to determinate over-bought/sold.
+	 *			  This value must be between 0 to 100. if the user select over-sold mode, the program will automatically 
+	 *			  add the negative sign to convert it to the over-sold limit value.
+    *			  The default value is 50.

+	 * @param isCentralLine
+	 *            TRUE: the mode that select where CMO is higher/lower than 0.
+	 *            False: over-bought/sold mode, which selects where CMO is higher than limit (or lower than negative limit)
+	 *  


+	 * @param position
+	 *            if centralLine = true, position represents above/below 0
+	 *            if centralLine = false, position represents over-bought/over-sold
+	 *            
+    * @param CMOResult: result of CMO value	           
+	 * @param start
+	 * @throws Exception
+	 */

public class CMO extends TechnicalFilterIntersect{
	
	private TAlib ta;
	private int period;
	private int limit;
	private boolean isCentralLine;
	private Position position;
	private int start, end = Integer.MAX_VALUE;
	
	private double[] CMOResult;
	
	
	/**
	 * 
	 * @param ta
	 * @param period
	 * @param limit
	 * @param isCentralLine
	 * @param position
	 * @param start
	 * @throws Exception
	 */
	public CMO(TAlib ta, int period, int limit,
			boolean isCentralLine, Position position, int start) throws Exception {
		this.ta = ta;

		if (period <= 1 ) {
			throw new IllegalArgumentException("Period must be larger than 1");
		}
		this.period = period;
		
		if (limit <=0 || limit >= 100 ) {
			throw new IllegalArgumentException("Limit must between 0 to 100");
		}
		this.limit = limit;
		this.isCentralLine = isCentralLine;
		

		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
	}
	
	/**
	 * 
	 * @param ta
	 * @param period
	 * @param limit
	 * @param isCentralLine
	 * @param position
	 * @param start
	 * @param end
	 * @throws Exception
	 */
	public CMO(TAlib ta, int period, int limit,
			boolean isCentralLine, Position position, int start, int end) throws Exception {
		
		this(ta, period, limit, isCentralLine, position, start);
		if (end < start)
			throw new IllegalArgumentException("End should not be smaller than start");
		this.end = end;
	}
	
	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		
		Map<String, Object> periods = new HashMap<String, Object>();
		periods.put("period", this.period);
		
		Map<String, Object> outputs = ta.compute(periods);

		if (outputs.size() > 0) {
			this.CMOResult = (double[]) outputs.get("cmo");
			
		} else {
			this.CMOResult = new double[] {};
			
		}

		if (isCentralLine) {
			// zero line crossover
			result = buildCentralLineResult(rawData, this.CMOResult);
		} else {
			// over-bought/over-sold
			result = buildLimitResult(rawData, this.CMOResult);
		}

		return result;
	}
	
	private DateList buildCentralLineResult(BarDataWrapper rawData, double[] CMOResult)
			throws Exception {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < CMOResult.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && CMOResult[i] > 0) ||
					(position.equals(Position.below) && CMOResult[i] < 0)) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			}
			else
				consecutiveDay = 0;
		}

		return result;
	}
	
	private DateList buildLimitResult(BarDataWrapper rawData, double[] CMOResult)
			throws Exception {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < CMOResult.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && CMOResult[i] > limit) ||
					(position.equals(Position.below) && CMOResult[i] < (-limit))) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			}
			else
				consecutiveDay = 0;
		}

		return result;
	}
	
	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		
		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("CMOResult", this.CMOResult);
		
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "CMO");
	}
	
}
