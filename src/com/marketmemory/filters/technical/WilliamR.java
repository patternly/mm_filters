package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;


/**
 * Filtered the retrieved William%R according to user settings of 
 * low limit and high limit. Date list is constructed with dates 
 * of which williamR values are in between the two limits.
 * 
 * @author Johnny Jiang
 *
 */

public class WilliamR extends TechnicalFilterIntersect {
	private TAlib ta;
	private int timePeriod;
	private double lowLimit, highLimit;
	private int start, end = Integer.MAX_VALUE;
	
	private double[] williamResult;
	
	/**
	 * 
	 * @param ta
	 * @param timePeriod
	 * 		-- look back period
	 * @param lowLimit
	 * 		-- date with williamR value lower than this will be filtered out
	 * @param highLimit
	 * 		-- date with williamR value higher than this will be filtered out
	 * @param start
	 * @throws Exception
	 */
	public WilliamR(TAlib ta, int timePeriod, double lowLimit, 
			double highLimit, int start) throws Exception {
		this.ta = ta;
		
		if ( timePeriod < 2 || timePeriod > 100000){
			throw new IllegalArgumentException("Invalid time period");
		}
		this.timePeriod = timePeriod;
		
		if (lowLimit > 0 || lowLimit < -100){
			throw new IllegalArgumentException("Low limit must be between -100 and 0");
		} else if ( highLimit > 0 || highLimit < -100){
			throw new IllegalArgumentException("High limit must be between -100 and 0");
		} else if ( lowLimit > highLimit){
			throw new IllegalArgumentException("Low limit must be smaller than high limit");
		}
		this.lowLimit = lowLimit;
		this.highLimit = highLimit;
				
		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
		
	}
	
	/**
	 *	WilliamR constructor with an optional 'end' date parameter 
	 *
	 */
	public WilliamR(TAlib ta, int timePeriod, double lowLimit, double highLimit,  
			int start, int end) throws Exception {
		this(ta, timePeriod, lowLimit, highLimit, start);
		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}
	
	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception{
		DateList result = new DateList();
		
		Map<String, Object> period = new HashMap<String, Object>();
		period.put("timePeriod", this.timePeriod);
		
		Map<String, Object> output = ta.compute(period);
		
		if(output.size()>0){
			this.williamResult = (double[]) output.get("williamR");
		} else {
			this.williamResult = new double[]{};
		}
		
		result = buildDateList(rawData, this.williamResult);

		return result;
	}
	
	/**
	 * Filters the data in between the low limit and high limit
	 * i.e. above -50 -> low limit = -50, high limit = 0
	 * 
	 * @param rawData
	 * @param williamResult
	 * @return
	 */
	private DateList buildDateList(BarDataWrapper rawData, double[] williamResult) {
		DateList result = new DateList();
		int consecutiveDay = 0;
		
			for (int i = 0; i < williamResult.length; i++) {
				int index = i + ta.getIndexBegin();
	
				if ((williamResult[i] >= this.lowLimit) && ( williamResult[i] <= this.highLimit)) {
					consecutiveDay++;
	
					if (consecutiveDay >= this.start && consecutiveDay <= this.end)
						result.add(rawData.getDateTime()[index]);
					
				} else {
					consecutiveDay = 0;
				}
			}
		

		return result;
	}
	
	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		
		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("WilliamRResult", this.williamResult);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "WilliamR");
	}
}