package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;


/**
 * Commodity Channel Index (CCI) filter.
 * TODO: Describe this filter overall.
 * 
 * author: Yang Meng
 */
public class CCI extends TechnicalFilterIntersect {

	private TAlib ta;
	private int lookBackPeriod;
	private int limit;
	boolean isOverBought;

	private double[] cciResult;
	private int start, end = Integer.MAX_VALUE;

	/**
	 * Commodity Channel Index (CCI)Constructor.
	 * 
	 * @param ta
	 * @param lookBackPeriod
	 *            The period to calculate CCI.
	 * @param limit
	 *            A threshold value to define the line of over-bought or
	 *            over-sold. this input value must be larger than 0 (default
	 *            value:100) here. if you select over-sold mode, the program
	 *            will automatically put the negative sign(in fact, the
	 *            over-sold line will be -100).
	 * @param isOverBought
	 *            True means over-bought, and false means over-sold.
	 * @param start
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public CCI(TAlib ta, int lookBackPeriod, int limit, boolean isOverBought, int start)
			throws IllegalArgumentException, Exception {
		this.ta = ta;
		this.isOverBought = isOverBought;
		if (lookBackPeriod <= 1)
			throw new IllegalArgumentException("look-back period must be larger than 1");
		this.lookBackPeriod = lookBackPeriod;

		if (limit < 0)
			throw new IllegalArgumentException("limit must be larger than 0");
		this.limit = limit;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;

	}

	/**
	 * 
	 * @param ta
	 * @param lookBackPeriod
	 * @param limit
	 * @param isOverBought
	 * @param start
	 * @param end
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public CCI(TAlib ta, int lookBackPeriod, int limit, boolean isOverBought, int start, int end)
			throws IllegalArgumentException, Exception {
		this(ta, lookBackPeriod, limit, isOverBought, start);

		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		this.cciResult = ta.compute(lookBackPeriod);
		int consecutiveDay = 0;

		for (int i = 0; i < this.cciResult.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((this.cciResult[i] > limit && this.isOverBought) || (this.cciResult[i] < -limit && !this.isOverBought)) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end) {
					result.add(rawData.getDateTime()[index]);
				}

			} else {
				consecutiveDay = 0;
			}
		}
		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("cciResult", cciResult);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "CCI");
	}

}
