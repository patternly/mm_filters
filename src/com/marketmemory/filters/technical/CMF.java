package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;

/**
 * Builds a date list with CMF values in between the low limit
 * and high limit given by the user. Some TA-lib functions
 * are used to calculate the CMF values, and the detailed calculation
 * can be found in filters.TACompute.CMFCompute.
 * 
 * @author Johnny Jiang
 * @lastUpdate 2015-06-07
 *
 */
public class CMF extends TechnicalFilterIntersect {
	private TAlib ta;
	private int period;
	private int start, end = Integer.MAX_VALUE;
	private double lowLimit, highLimit;
	private static final int MAX_VALUE = 100000;

	private double[] cmf;

	/**
	 * 
	 * @param ta
	 * @param period
	 * 		-- time period specified by user;
	 * @param lowLimit
	 * 		-- filters out date with CMF value lower than this;
	 * @param highLimit
	 * 		-- filters out date with CMF value higher than this;
	 * @param start
	 * @throws Exception
	 */
	public CMF(TAlib ta, int period, double lowLimit, double highLimit, int start) throws Exception {
		this.ta = ta;

		if (period < 2 || period > MAX_VALUE)
			throw new IllegalArgumentException("Invalid time period!");
		this.period = period;

		if (lowLimit > 1 || lowLimit < -1)
			throw new IllegalArgumentException("Signal value must be beteween -1 and 1!");
		if (highLimit > 1 || highLimit < -1 )
			throw new IllegalArgumentException("Signal value must be beteween -1 and 1!");
		else if(lowLimit > highLimit)
			throw new IllegalArgumentException("Low limit must be greater than high limit!");
			
		this.lowLimit = lowLimit;
		this.highLimit = highLimit;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
	}

	public CMF(TAlib ta, int period, double lowLimit, double highLimit, int start, int end) throws Exception {
		this(ta, period, lowLimit, highLimit, start);
		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		Map<String, Object> periods = new HashMap<String, Object>();
		periods.put("period", this.period);
		Map<String, Object> outputs = ta.compute(periods);

		if (outputs.size() > 0) {
			this.cmf = (double[]) outputs.get("CMF");
		} else {
			this.cmf = new double[] {};
		}

		result = buildCMF(rawData, this.cmf);

		return result;
	}
	
	private DateList buildCMF(BarDataWrapper rawData, double[] cmf) {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < cmf.length; i++) {
			int index = i + ta.getIndexBegin();

			if (cmf[i] >= this.lowLimit && cmf[i] <= this.highLimit) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			} else
				consecutiveDay = 0;
		}

		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("CMFResult", this.cmf);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "CMF");
	}

}