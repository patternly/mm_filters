package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;

/**
 * The donchian channels filter gets the min and max for each period, and build
 * a date list with dates that is the period's new high/low.
 * 
 * @author Michael
 * 
 */
public class DonchianChannels extends TechnicalFilterIntersect {
	private TAlib ta;
	private int period;
	private boolean isNewHigh;
	private int start, end = Integer.MAX_VALUE;

	private double[] minValue;
	private double[] maxValue;

	public DonchianChannels(TAlib ta, int period, boolean isNewHigh, int start) {
		this.ta = ta;

		if (period <= 1) {
			throw new IllegalArgumentException("Period must be larger than 1");
		}
		this.period = period;

		this.isNewHigh = isNewHigh;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
	}

	public DonchianChannels(TAlib ta, int period, boolean isNewHigh, int start, int end) {
		this(ta, period, isNewHigh, start);
		this.ta = ta;

		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
//		BarDataWrapper rawData = indexSeries.getBarSeries();

		Map<String, Object> taInputs = new HashMap<String, Object>();
		taInputs.put("period", this.period);

		Map<String, Object> outputs = ta.compute(taInputs);

		int[] minIndex;
		int[] maxIndex;

		if (outputs.size() > 0) {
			minIndex = (int[]) outputs.get("minIndex");
			maxIndex = (int[]) outputs.get("maxIndex");
			this.minValue = (double[]) outputs.get("minValue");
			this.maxValue = (double[]) outputs.get("maxValue");
		} else {
			minIndex = new int[] {};
			maxIndex = new int[] {};
			this.minValue = new double[] {};
			this.maxValue = new double[] {};
		}

		if (ta.getResultLength() > 0 && start == 1) {
			if (isNewHigh) {
				result.add(rawData.getDateTime()[maxIndex[0]]);
			} else {
				result.add(rawData.getDateTime()[minIndex[0]]);
			}
		}
		int consecutiveDay = 0;

		for (int i = 1; i < ta.getResultLength(); i++) {
			if (isNewHigh && rawData.getHigh()[maxIndex[i]] > rawData.getHigh()[maxIndex[i - 1]]) {
				consecutiveDay++;
				
				if (consecutiveDay >= this.start && consecutiveDay <= this.end) {
					result.add(rawData.getDateTime()[maxIndex[i]]);
				}
			} else if (!isNewHigh && rawData.getLow()[minIndex[i]] < rawData.getLow()[minIndex[i - 1]]) {
				consecutiveDay++;
				
				if (consecutiveDay >= this.start && consecutiveDay <= this.end) {
					result.add(rawData.getDateTime()[minIndex[i]]);
				}
			} else {
				consecutiveDay = 0;
			}
		}

		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("minValue", this.minValue);
		results.put("maxValue", this.maxValue);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "DonchianChannels");
	}

}
