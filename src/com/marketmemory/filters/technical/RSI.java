package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;

/**
 * The RSI Filter calculates the RSI results base on user settings, and build a
 * date list with dates that the corresponding RSI value is between low limit
 * and high limit from start to end.
 * 
 * @author Michael
 * 
 */
public class RSI extends TechnicalFilterIntersect {
	private int lookBackPeriod;
	private int highLimit, lowLimit;
	private TAlib ta;
	private double[] rsiResult;
	private int start, end = Integer.MAX_VALUE;

	public RSI(TAlib ta,
			int lookBackPeriod, int highLimit, int lowLimit, int start) throws IllegalArgumentException, Exception {
		this.ta = ta;

		if (lookBackPeriod <= 1)
			throw new IllegalArgumentException("look-back period must be larger than 1");
		this.lookBackPeriod = lookBackPeriod;

		if (lowLimit < 0)
			throw new IllegalArgumentException("low limit must be larger than 0");
		else if (highLimit > 100)
			throw new IllegalArgumentException("high limit must be less than 100");
		else if (highLimit < lowLimit)
			throw new IllegalArgumentException("high limit must be larger than low limit");

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;

		this.highLimit = highLimit;
		this.lowLimit = lowLimit;
	}

	public RSI(TAlib ta,
			int lookBackPeriod, int highLimit, int lowLimit, int start, int end) throws IllegalArgumentException,
			Exception {
		this(ta, lookBackPeriod, highLimit, lowLimit, start);

		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		this.rsiResult = ta.compute(lookBackPeriod);
		int consecutiveDay = 0;

		// Select the RSI results that are between lowLimit and highLimit for
		// start to end days, and add the corresponding date to result DateList.
		for (int i = 0; i < this.rsiResult.length; i++) {
			int index = i + ta.getIndexBegin();

			if (this.rsiResult[i] > lowLimit && this.rsiResult[i] < highLimit) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end) {
					result.add(rawData.getDateTime()[index]);
				}

			} else {
				consecutiveDay = 0;
			}
		}
		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("rsiResult", rsiResult);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "RSI");
	}

}
