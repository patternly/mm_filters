package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
 * The MACD filter calculates the MACD results based on user settings, and build
 * a date list with dates that the corresponding MACD values are above/below
 * MACD signal values, or MACD values are grater than zero.
 * 
 * @author Michael
 * 
 */
public class MACD extends TechnicalFilterIntersect {
	private TAlib ta;
	private int fastPeriod;
	private int slowPeriod;
	private int signalPeriod;
	private boolean isSignalLine;
	private Position position;
	private int start, end = Integer.MAX_VALUE;
	
	private double[] macdResult;
	private double[] macdSignal;
	private double[] macdHist;

	public MACD(TAlib ta, int fastPeriod, int slowPeriod, int signalPeriod,
			boolean isSignalLine, Position position, int start) throws Exception {
		this.ta = ta;

		if (fastPeriod <= 1 || slowPeriod <= 1 || signalPeriod <= 1) {
			throw new IllegalArgumentException("All periods must be larger than 1");
		}
		this.fastPeriod = fastPeriod;
		this.slowPeriod = slowPeriod;
		this.signalPeriod = signalPeriod;

		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;

		this.isSignalLine = isSignalLine;
	}

	public MACD(TAlib ta, int fastPeriod, int slowPeriod, int signalPeriod,
			boolean isSignalLine, Position position, int start, int end) throws Exception {
		this(ta, fastPeriod, slowPeriod, signalPeriod, isSignalLine, position, start);

		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		
		Map<String, Object> periods = new HashMap<String, Object>();
		periods.put("fastPeriod", this.fastPeriod);
		periods.put("slowPeriod", this.slowPeriod);
		periods.put("signalPeriod", this.signalPeriod);
		
		Map<String, Object> outputs = ta.compute(periods);

		if (outputs.size() > 0) {
			this.macdResult = (double[]) outputs.get("macd");
			this.macdSignal = (double[]) outputs.get("macdSignal");
			this.macdHist = (double[]) outputs.get("macdHist");
		} else {
			this.macdResult = new double[] {};
			this.macdSignal = new double[] {};
			this.macdHist = new double[] {};
		}

		if (isSignalLine) {
			result = buildSignalLineResult(rawData, this.macdResult, this.macdSignal);
		} else {
			result = buildCenterLineResult(rawData, this.macdResult);
		}

		return result;
	}

	/**
	 * Build the date list when user select gignal line crossovers.
	 * 
	 * @param rawData
	 * @param macdResult
	 * @param macdSignal
	 * @return
	 * @throws Exception
	 */
	private DateList buildSignalLineResult(BarDataWrapper rawData, double[] macdResult, double[] macdSignal)
			throws Exception {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < macdResult.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && macdResult[i] > macdSignal[i]) ||
					(position.equals(Position.below) && macdResult[i] < macdSignal[i])) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			}
			else
				consecutiveDay = 0;
		}

		return result;
	}

	/**
	 * Build the date list when user select center line crossovers.
	 * 
	 * @param rawData
	 * @param macdResult
	 * @return
	 * @throws Exception
	 */
	private DateList buildCenterLineResult(BarDataWrapper rawData, double[] macdResult) throws Exception {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < macdResult.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && macdResult[i] > 0) ||
					(position.equals(Position.below) && macdResult[i] < 0)) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			}
			else
				consecutiveDay = 0;
		}

		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		
		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("macdResult", this.macdResult);
		results.put("macdSignal", this.macdSignal);
		results.put("macdHist", this.macdHist);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "MACD");
	}

}
