package com.marketmemory.filters.technical;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.marketmemory.filters.Filter;
import com.marketmemory.filters.TechnicalFilterIntersect;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

public class AbsPrice extends TechnicalFilterIntersect {
	private double price;
	private int start, end = Filter.maxInteger;
	private Position position;
	private double recoveryValue = 0.0;
	
	private double[] absPrice;
	
	// 200 trading days are approximate 1 year.
	final int MAX_LOOK_BACK_PERIOD = 200;
	
	public AbsPrice(double price, Position position, int start) throws IllegalArgumentException, Exception {
		if(price <0)
			throw new IllegalArgumentException("absolute price must be grater than or equal to 0");
		this.price = price;
		if(!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;
		if(start<=0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
		
	}
	
	public AbsPrice(double price, Position position, int start, double recoveryValue) throws IllegalArgumentException, Exception {
		this(price, position, start);
		
		if((this.position.equals(Position.above) && recoveryValue>0) || (this.position.equals(Position.below) && recoveryValue<0)) {
			throw new IllegalArgumentException("The recovery value should be negative when select price above and positive when select price below.");
		}
			
		this.recoveryValue = recoveryValue;
	}
	
	public AbsPrice(double price,  Position position, int start, int end) throws IllegalArgumentException, Exception {
		this(price, position, start);
		if(end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}
	
	public AbsPrice(double price,  Position position, int start, int end, double recoveryValue) throws IllegalArgumentException, Exception {
		this(price, position, start, recoveryValue);
		if(end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		this.absPrice = new double[rawData.getClose().length];
		int consecutiveDay = 0;
		
		List<Integer> resultIndex = new ArrayList<Integer>();
		
		for(int i=0; i<rawData.getClose().length; i++) {
			this.absPrice[i] = this.price;
			if((position.equals(Position.above) && rawData.getClose()[i]>this.price) ||
					(position.equals(Position.below) && rawData.getClose()[i]<this.price)) {
				consecutiveDay++;
				if(consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[i]);
					resultIndex.add(i);
			}
			else consecutiveDay = 0;
		}
		
		if (this.recoveryValue != 0.0 && result.size()>0) {
			result = applyRecoveryPercentage(result, rawData, resultIndex);
		}

		return result;
	}

	private DateList applyRecoveryPercentage(DateList result, BarDataWrapper rawData, List<Integer> resultIndex) throws Exception {
		
		double[] rawClose = rawData.getClose();
		
//		// If two period overlaps, remove the first period if it starts lower than second period in negative performance
//		// or highter than second period in positive performance.
//		for(int i = 1; i < result.size(); i++) {
//			
//			if (resultIndexStart.get(i) < resultIndexEnd.get(i-1)) {
//				
//				if ((percentage >= 0 && rawClose[resultIndexStart.get(i)] < rawClose[resultIndexStart.get(i-1)]) ||
//						(percentage < 0 && rawClose[resultIndexStart.get(i)] > rawClose[resultIndexStart.get(i-1)])) {
//					
//					result.remove(i-1);
//					resultIndexStart.remove(i-1);
//					resultIndexEnd.remove(i-1);
//					i--;
//				}
//			}
//		}
		
		for(int i = 1; i < result.size(); i++) {
			if(resultIndex.get(i) - resultIndex.get(i-1) < MAX_LOOK_BACK_PERIOD) {
				if(!meetsRecoveryPercentage(rawClose, resultIndex.get(i), resultIndex.get(i-1))) {
					result.remove(i);
					resultIndex.remove(i);
					i--;
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Check if their is a price difference greater then recovery value in corresponding position from firstStart to secondStart.
	 * @param rawClose
	 * @param secondStart
	 * @param firstStart
	 * @return
	 */
	private boolean meetsRecoveryPercentage(double[] rawClose, int secondStart, int firstStart) {
		for(int i = secondStart-firstStart; i>=1; i--) {
			
			int periodCount = secondStart-firstStart - i + 1;
			
			for (int j=firstStart; j<firstStart+periodCount; j++) {
				
				double change = rawClose[j+i] - rawClose[j];
				
				if ((position.equals(Position.above) && change <= this.recoveryValue) ||
						(position.equals(Position.below) && change >= this.recoveryValue)) {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		
		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("absPrice", this.absPrice);
		
		return new FilterResult(dateList, results, 0, dateList.size(), "AbsPrice");
	}

}
