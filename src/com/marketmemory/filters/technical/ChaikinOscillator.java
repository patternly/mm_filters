package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
 * 
 * The Chaikin Oscillator is the difference between the 3-day EMA of the
 * Accumulation Distribution Line and the 10-day EMA of the Accumulation
 * Distribution Line. Like other momentum indicators, this indicator is designed
 * to anticipate directional changes in the Accumulation Distribution Line by
 * measuring the momentum behind the movements. A momentum change is the first
 * step to a trend change. Anticipating trend changes in the Accumulation
 * Distribution Line can help chartists anticipate trend changes in the
 * underlying security. The Chaikin Oscillator generates signals with crosses
 * above/below the zero line or with bullish/bearish divergences.
 * 
 * @author Yangqiao
 *
 */
public class ChaikinOscillator extends TechnicalFilterIntersect {

	private TAlib ta;
	private int signalLine;
	private int fastPeriod;
	private int slowPeriod;
	private Position position;
	private int start, end = Integer.MAX_VALUE;

	private double[] ChaikinOscResult;

	/**
	 * 
	 * @param ta
	 * @param signalLine
	 *            : default:0
	 * @param fastPeriod
	 *            : default:3
	 * @param slowPeriod
	 *            : default:10
	 * @param position
	 *            : position=above will select where result is positive
	 *            position=below will select where result is negative
	 * @param start
	 * @throws Exception
	 */
	public ChaikinOscillator(TAlib ta, int signalLine, int fastPeriod, int slowPeriod, Position position, int start) throws Exception {

		this.ta = ta;
		

		if (fastPeriod <= 1 || slowPeriod <= 1) {
			throw new IllegalArgumentException("All periods must be larger than 1");
		}

		if (fastPeriod > slowPeriod) {
			throw new IllegalArgumentException("slow periods must be larger than fast period");
		}
		this.signalLine = signalLine;
		this.fastPeriod = fastPeriod;
		this.slowPeriod = slowPeriod;

		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;

	}

	public ChaikinOscillator(TAlib ta, int signalLine,int fastPeriod, int slowPeriod, Position position, int start, int end)
			throws Exception {
		this(ta, signalLine,fastPeriod, slowPeriod, position, start);

		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();

		Map<String, Object> periods = new HashMap<String, Object>();
		periods.put("fastPeriod", this.fastPeriod);
		periods.put("slowPeriod", this.slowPeriod);

		Map<String, Object> outputs = ta.compute(periods);

		if (outputs.size() > 0) {
			this.ChaikinOscResult = (double[]) outputs.get("ChaikinOscResult");
		} else {
			this.ChaikinOscResult = new double[] {};

		}

		int consecutiveDay = 0;

		for (int i = 0; i < ChaikinOscResult.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && ChaikinOscResult[i] > signalLine)
					|| (position.equals(Position.below) && ChaikinOscResult[i] < signalLine)) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			} else
				consecutiveDay = 0;
		}

		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("ChaikinOscResult", ChaikinOscResult);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "ChaikinOsc");
	}
}
