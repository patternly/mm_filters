package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;


/**
 * Filters and builds a date list based on user setting that whether the slope of the close
 * prices of a data bar is [above]/[below] the user inputed [signal line].
 * 
 * @author Johnny Jiang
 *
 */
public class Slope extends TechnicalFilterIntersect{
	private TAlib ta;
	private int timePeriod;
	private Position position;
	private double signalLine;
	private int start, end = Integer.MAX_VALUE;
	private static final int MAX_VALUE = 100000;
	
	
	private double[] slope;
	
	/**
	 * 
	 * @param ta
	 * @param timePeriod
	 * 		-- the look back period
	 * @param position
	 * 		-- whether the slope is above/below signalLine
	 * @param signalLine
	 * 		-- user input of signal line
	 * @param start
	 * @throws Exception
	 */
	public Slope(TAlib ta, int timePeriod, Position position, double signalLine, int start) throws Exception{
		this.ta = ta;
		
		if ( timePeriod < 2 || timePeriod > MAX_VALUE){
			throw new IllegalArgumentException("Invalid time period");
		}
		this.timePeriod = timePeriod;
		
		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;
		
		this.signalLine = signalLine;
		
		if ( start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
	}
	
	public Slope(TAlib ta, int timePeriod, Position position, double signalLine, int start, int end) throws Exception{
		this(ta, timePeriod, position, signalLine, start);
		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}
	
	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception{
		DateList result = new DateList();
		
		Map<String, Object> period = new HashMap<String, Object>();
		period.put("timePeriod", this.timePeriod);
		
		Map<String, Object> output = ta.compute(period);
		
		if(output.size()>0){
			this.slope = (double[]) output.get("slope");
		} else {
			this.slope = new double[]{};
		}
		
		result = buildDateList(rawData, this.slope);

		return result;
	}
	
	/**
	 * Builds the filtered date list
	 * @param rawData
	 * @param slope
	 * @return
	 */
	private DateList buildDateList(BarDataWrapper rawData, double[] slope) {
		DateList result = new DateList();
		int consecutiveDay = 0;
		
		for (int i = 0; i < slope.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && slope[i] > this.signalLine) ||
					(position.equals(Position.below) && slope[i] < this.signalLine)) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
				
			} else {
				consecutiveDay = 0;
			}
		}
	return result;		
	}
	
	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		
		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("slopeResult", this.slope);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "Slope");
	}	
}