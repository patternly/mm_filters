package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
/**
 * The ATR Filter calculates the ATR results base on user settings, and build a
 * date list with dates that the corresponding ATR value is above atrUp, below
 * atrDown, or Between artUp and atrDown.
 * 
 * @author Michael
 * 
 */
public class ATR extends TechnicalFilterIntersect {
	private double atrUp;
	private double atrDown;
	private int period;
	private TAlib ta;
	private double[] atr;
	boolean isRelative;

	public ATR(TAlib ta, int period, double atrUp,
			double atrDown, boolean isRelative) throws Exception {
		this.ta = ta;
		this.period = period;
		this.atrUp = atrUp;
		this.atrDown = atrDown;
		this.isRelative = isRelative;
		if (atrDown == emptyValue && atrUp == emptyValue)
			throw new Exception("Error, up and down should not be both empty");
	}

	private DateList buildFilterDateList(BarDataWrapper rawData)
			throws Exception {
		double[] tr = ta.compute(1);
		this.atr = ta.compute(period);

		if (isRelative) {
			return filterRelativePriceATR(tr, this.atr, rawData);
		} else {
			return filterAbsolutePriceATR(this.atr, rawData);
		}
	}

	/**
	 * Filter the result that compares true range with average true range based
	 * on specified settings.
	 * 
	 * @param tr
	 * @param atr
	 * @param rawData
	 * @return
	 */
	private DateList filterRelativePriceATR(double[] tr, double[] atr,
			BarDataWrapper rawData) {
		DateList result = new DateList();
		for (int i = 0; i < atr.length; i++) {
			int index = i + ta.getIndexBegin();
			int trIndex = i + ta.getIndexBegin() - 1;
			if ((atrDown == emptyValue && atrUp != emptyValue && tr[trIndex] >= atrUp
					* atr[i])
					|| (atrDown != emptyValue && atrUp == emptyValue && tr[trIndex] <= atrDown
							* atr[i])
					|| ((atrDown != emptyValue && atrUp != emptyValue) && (tr[trIndex] <= atrUp
							* atr[i] && tr[trIndex] >= atrDown * atr[i]))) {

				result.add(rawData.getDateTime()[index]);
			}
		}

		return result;
	}

	/**
	 * Filter the result compares average true range with absolute value that
	 * specified.
	 * 
	 * @param atr
	 * @param rawData
	 * @return
	 */
	private DateList filterAbsolutePriceATR(double[] atr, BarDataWrapper rawData) {
		DateList result = new DateList();
		for (int i = 0; i < atr.length; i++) {
			int index = i + ta.getIndexBegin();
			if ((atrDown == emptyValue && atrUp != emptyValue && atr[i] >= atrUp)
					|| (atrDown != emptyValue && atrUp == emptyValue && atr[i] <= atrDown)
					|| ((atrDown != emptyValue && atrUp != emptyValue) && (atr[i] <= atrUp && atr[i] >= atrDown))) {

				result.add(rawData.getDateTime()[index]);
			}
		}

		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData)
			throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("atrResult", this.atr);
		return new FilterResult(dateList, results, ta.getIndexBegin(),
				ta.getResultLength(), "ATR");
	}
}
