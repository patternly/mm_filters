package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
 * Builds two kinds of date list based on the signal line chosen by user.
 * Filters the dates with ADLine values either above or below the signal line.
 * The signal line for this method is either SMA or OBV.
 * 
 * @author Johnny Jiang
 *
 */

public class ADLine extends TechnicalFilterIntersect{
	private TAlib ta;
	private boolean  isSMALine;
	private int signalPeriod = 30;
	private Position position;
	private int start, end = Integer.MAX_VALUE;
	private static final int MAX_VALUE = 100000;
	
	private double[] adLine;
	private double[] smaSignal;
	private double[] obvSignal;
	
	/**
	 * 
	 * @param ta
	 * @param isSMALine
	 * 		-- is true when SMA is chosen to be the signal line;
	 * @param signalPeriod
	 * 		-- the signal period for SMA signal line only
	 * @param position
	 * 		-- either above or below the chosen signal line 
	 * @param start
	 * @throws Exception
	 */
	public ADLine(TAlib ta, boolean isSMALine, int signalPeriod, Position position, 
			int start) throws Exception{
		this.ta = ta;
		
		if(signalPeriod < 2 || signalPeriod > MAX_VALUE)
			throw new IllegalArgumentException("SMA period cannot be smaller than 2!");
		this.signalPeriod = signalPeriod;
			
		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;
		
		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
		
		this.isSMALine = isSMALine;	
	}
	
	public ADLine(TAlib ta, boolean isSMALine, int signalPeriod, Position position, 
			int start, int end) throws Exception{
		this(ta, isSMALine, signalPeriod, position, start);
		
		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}
	
	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception{
		DateList result = new DateList();
		
		Map<String, Object> periods = new HashMap<String, Object>();
		periods.put("signalPeriod", this.signalPeriod);
		Map<String, Object> outputs = ta.compute(periods);
		
		if (outputs.size() > 0) {
			this.adLine = (double[]) outputs.get("ADLine");
			this.smaSignal = (double[]) outputs.get("SMASignal");
			this.obvSignal = (double[]) outputs.get("OBVSignal");
		} else {
			this.adLine = new double[] {};
			this.smaSignal = new double[] {};
			this.obvSignal = new double[] {};
		}
		
		if (isSMALine){
			result = buildSMASignalResult(rawData, this.adLine, this.smaSignal);
		} else {
			result = buildOBVSignalResult(rawData, this.adLine, this.obvSignal);
		}
		return result;
	}
	
	/**
	 * Filters and builds date list based on OBV value
	 * @param rawData
	 * @param adLine
	 * @param obvSignal
	 * @return
	 */
	private DateList buildOBVSignalResult(BarDataWrapper rawData, double[] adLine, double[] obvSignal) {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < adLine.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && adLine[i] > obvSignal[i]) ||
					(position.equals(Position.below) && adLine[i] < obvSignal[i])) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			}
			else
				consecutiveDay = 0;
		}

		return result;
	}
	
	/**
	 * 
	 * Filters and builds date list based on SMA value
	 * @param rawData
	 * @param adLine
	 * @param smaSignal
	 * @return
	 */
	private DateList buildSMASignalResult(BarDataWrapper rawData, double[] adLine, double[] smaSignal) {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < adLine.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && adLine[i] > smaSignal[i]) ||
					(position.equals(Position.below) && adLine[i] < smaSignal[i])) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			}
			else
				consecutiveDay = 0;
		}

		return result;
	}
	
	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		
		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("adLineResult", this.adLine);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "ADLine");
	}
}