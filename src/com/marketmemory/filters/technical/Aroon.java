package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;

/**
 * Aroon filter computes Aroon-up and Aroon-down values based on a given period
 * of days; builds a date list that either selects dates with Aroon-up >
 * Aroon-down or dates with Aroon-up < Aroon-down
 * 
 * @author Johnny Jiang
 *
 */
public class Aroon extends TechnicalFilterIntersect {
	private TAlib ta;
	private int period;
	// private Position position;
	private boolean isAroonUp;
	private int start, end = Integer.MAX_VALUE;
	private double[] aroonUp;
	private double[] aroonDown;

	public Aroon(TAlib ta, int period, boolean isAroonUp, int start) throws Exception {
		this.ta = ta;
		if (period <= 1) {
			throw new IllegalArgumentException("Period must be larger than 1");
		}
		this.period = period;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;

		this.isAroonUp = isAroonUp;
	}

	public Aroon(TAlib ta, int period, boolean isAroonUp, int start, int end) throws Exception {
		this(ta, period, isAroonUp, start);

		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();

		Map<String, Object> periods = new HashMap<String, Object>();
		periods.put("period", this.period);
		Map<String, Object> outputs = ta.compute(periods);

		if (outputs.size() > 0) {
			this.aroonUp = (double[]) outputs.get("aroonUp");
			this.aroonDown = (double[]) outputs.get("aroonDown");
		} else {
			this.aroonUp = new double[] {};
			this.aroonDown = new double[] {};
		}

		result = buildAroonUp(rawData, isAroonUp, this.aroonUp, this.aroonDown);

		return result;
	}

	/**
	 * Build datelist when [Aroon up is above Aroon down] is selected
	 * 
	 * @param rawData
	 * @param aroonUp
	 * @return
	 * @throws Exception
	 */
	private DateList buildAroonUp(BarDataWrapper rawData, boolean isAroonUp, double[] aroonUp, double[] aroonDown)
			throws Exception {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < aroonUp.length; i++) {
			int index = i + ta.getIndexBegin();
			if (isAroonUp && aroonUp[i] > aroonDown[i] || (!isAroonUp) && aroonUp[i] < aroonDown[i]) {
				consecutiveDay++;
				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			} else
				consecutiveDay = 0;
		}
		// System.out.println(result);
		return result;
	}


	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("aroonUp", this.aroonUp);
		results.put("aroonDown", this.aroonDown);

		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "Aroon");
	}

}