package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
 * TRIX fileter 
 * The TRIX filter computes the TRIX results based on User inputs,
 * and builds date lists according to user settings. The filtered
 * results is either above/below the signalLine/centreLine.
 * 
 * @author Johnny Jiang
 * 
 *
 */


public class TRIX extends TechnicalFilterIntersect {
	private TAlib ta;
	private int timePeriod;
	private boolean isSignalLine;
	private int signalPeriod;
	private Position position;
	private int start, end = Integer.MAX_VALUE;
	
	private double[] trixResult;
	private double[] trixSignal;
	
	public TRIX(TAlib ta, int timePeriod, boolean isSignalLine, int signalPeriod, Position position,
			int start) throws Exception{
		this.ta = ta;
		
		if(timePeriod <= 1 || timePeriod > 100000){
			throw new IllegalArgumentException("Invalid time period!");
		}
		this.timePeriod = timePeriod;
		
		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;
		
		if(signalPeriod <= 1){
			throw new IllegalArgumentException("Invalid signal period!");
		}
		this.signalPeriod = signalPeriod;
		
		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;

		this.isSignalLine = isSignalLine;
	}
	
	public TRIX(TAlib ta, int timePeriod, boolean isSignalLine, int signalPeriod, Position position,
			int start, int end) throws Exception{
		this(ta, timePeriod, isSignalLine, signalPeriod, position, start);
		
		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}
	
	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		
		Map<String, Object> periods = new HashMap<String, Object>();
		periods.put("timePeriod", this.timePeriod);
		periods.put("signalPeriod", this.signalPeriod);
		Map<String, Object> outputs = ta.compute(periods);
		
		if (outputs.size() > 0) {
			this.trixResult = (double[]) outputs.get("trix");
			this.trixSignal = (double[]) outputs.get("trixSignal");
		} else {
			this.trixResult = new double[] {};
			this.trixSignal = new double[] {};
		}

		if (isSignalLine) {
			result = buildSignalLineResult(rawData, this.trixResult, this.trixSignal);
		} else {
			result = buildCenterLineResult(rawData, this.trixResult);
		}

		return result;
	}
	
	/**
	 * 
	 * Build Datelist when user chooses Signal line
	 * @param rawData
	 * @param trixResult
	 * @param trixSignal
	 * @return
	 * @throws Exception
	 */
	private DateList buildSignalLineResult(BarDataWrapper rawData, double[] trixResult, double[] trixSignal)
			throws Exception {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < trixResult.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && trixResult[i] > trixSignal[i]) ||
					(position.equals(Position.below) && trixResult[i] < trixSignal[i])) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			}
			else
				consecutiveDay = 0;
		}

		return result;
	}
	
	/**
	 * Build Datelist when user chooses CenterLine
	 * 
	 * @param rawData
	 * @param trixResult
	 * @return
	 * @throws Exception
	 */
	private DateList buildCenterLineResult(BarDataWrapper rawData, double[] trixResult) throws Exception {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < trixResult.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && trixResult[i] > 0) ||
					(position.equals(Position.below) && trixResult[i] < 0)) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			}
			else
				consecutiveDay = 0;
		}

		return result;
	}
	
	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		
		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("trixResult", this.trixResult);
		results.put("trixSignal", this.trixSignal);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "TRIX");
	}
}