package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.Filter;
import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
 * Retrieve the computing results from TEMACompute, and builds a 
 * date list based on user setting. The date list is consisted of
 * either dates with close prices above TEMA or below TEMA.
 *  
 * 
 * @author Johnny Jiang
 * @lastUpdate Mar 6, 2015
 *
 */
public class TEMA extends TechnicalFilterIntersect {
	private TAlib ta;
	private int timePeriod;
	private Position position;
	private int start, end = Filter.maxInteger;
	private double[] tema;
	private static final int MAX_VALUE = 100000;
	
	/**
	 * 
	 * @param ta
	 * @param timePeriod
	 * 		-- look back period
	 * @param position
	 * 		-- whether close price is above/below the computed TEMA
	 * @param start
	 * @throws Exception
	 */
	public TEMA(TAlib ta, int timePeriod, Position position, int start) throws Exception {
		this.ta = ta;

		if (timePeriod < 2 || timePeriod > MAX_VALUE) {
			throw new IllegalArgumentException("time period must be at least 2");
		}
		this.timePeriod = timePeriod;

		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
	}

	public TEMA(TAlib ta, int timePeriod, Position position, int start, int end) throws Exception {
		this(ta, timePeriod, position, start);
		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}
	
	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		this.tema = ta.compute(timePeriod);

		int consecutiveDay = 0;
		for (int i = 0; i < this.tema.length; i++) {
			int index = i + ta.getIndexBegin();
			if ((position.equals(Position.above) && rawData.getClose()[index] > this.tema[i])
					|| (position.equals(Position.below) && rawData.getClose()[index] < this.tema[i])) {
				consecutiveDay++;
				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			} else
				consecutiveDay = 0;
		}

		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("TEMA", this.tema);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "TEMA");
	}

}