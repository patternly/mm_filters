package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
 * Builds two different date lists based on user settings. The filter returns
 * a date list with PPO values above/below signalLine/centerLine.
 * This class also retrieves & stores PPO Histogram values in @param ppoHist.
 * 
 *  
 * 
 * @author Johnny Jiang
 *
 */

public class PPO extends TechnicalFilterIntersect {
	private TAlib ta;
	private int fastPeriod;
	private int slowPeriod;
	private int signalPeriod;
	private boolean isSignalLine;
	private Position position;
	private int start, end = Integer.MAX_VALUE;
	
	private double[] ppoResult;
	private double[] ppoSignal;
	private double[] ppoHist;
	/**
	 * 
	 * @param ta
	 * @param fastPeriod
	 * @param slowPeriod
	 * @param signalPeriod
	 * @param isSignalLine
	 * 		-- true if filter is based on signal line; false if based on center line
	 * @param position
	 * @param start
	 * @throws Exception
	 */
	public PPO(TAlib ta, int fastPeriod, int slowPeriod, int signalPeriod,
			boolean isSignalLine, Position position, int start) throws Exception {
		this.ta = ta;
		
		if (fastPeriod < 2 || slowPeriod < 2 || signalPeriod < 2) {
			throw new IllegalArgumentException("All periods must be larger than 1");
		} else if (fastPeriod > 100000 || slowPeriod > 100000 || signalPeriod > 100000) {
			throw new IllegalArgumentException("All periods must be smaller than 100000");
		} else if (fastPeriod >= slowPeriod){
			throw new IllegalArgumentException("Fast period must be smaller than slow period");
		}
		this.fastPeriod = fastPeriod;
		this.slowPeriod = slowPeriod;
		this.signalPeriod = signalPeriod;
		
		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;
		
		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
		
		this.isSignalLine = isSignalLine;
	}
	
	/**
	 *  Constructor with 'end' parameter
	 */
	public PPO(TAlib ta, int fastPeriod, int slowPeriod, int signalPeriod,
			boolean isSignalLine, Position position, int start, int end) throws Exception {
		this(ta, fastPeriod, slowPeriod, signalPeriod, isSignalLine, position, start);
		
		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}
		
	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		
		Map<String, Object> periods = new HashMap<String, Object>();
		periods.put("fastPeriod", this.fastPeriod);
		periods.put("slowPeriod", this.slowPeriod);
		periods.put("signalPeriod", this.signalPeriod);
		
		Map<String, Object> outputs = ta.compute(periods);

		if (outputs.size() > 0) {
			this.ppoResult = (double[]) outputs.get("ppoResult");
			this.ppoSignal = (double[]) outputs.get("ppoSignal");
			this.ppoHist = (double[]) outputs.get("ppoHist");
		} else {
			this.ppoResult = new double[] {};
			this.ppoSignal = new double[] {};
			this.ppoHist = new double[] {};
		}

		if (isSignalLine) {
			result = buildSignalLineResult(rawData, this.ppoResult, this.ppoSignal);
		} else {
			result = buildCenterLineResult(rawData, this.ppoResult);
		}

		return result;
	}
	
	/**
	 * Build date list with ppo values above/below center line
	 * 
	 * @param rawData
	 * @param ppoResult
	 * @return
	 */
	private DateList buildCenterLineResult(BarDataWrapper rawData, double[] ppoResult) {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < ppoResult.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && ppoResult[i] > 0) ||
					(position.equals(Position.below) && ppoResult[i] < 0)) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			}
			else
				consecutiveDay = 0;
		}

		return result;
	}
	
	/**
	 * Build date list with ppo values above/below signal line
	 * 
	 * @param rawData
	 * @param ppoResult2
	 * @param ppoSignal2
	 * @return
	 */
	private DateList buildSignalLineResult(BarDataWrapper rawData, double[] ppoResult, double[] ppoSignal) {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < ppoResult.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && ppoResult[i] > ppoSignal[i]) ||
					(position.equals(Position.below) && ppoResult[i] < ppoSignal[i])) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			}
			else
				consecutiveDay = 0;
		}

		return result;
	}
	
	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		
		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("ppoResult", this.ppoResult);
		results.put("ppoSignal", this.ppoSignal);
		results.put("ppoHist", this.ppoHist);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "PPO");
	}
}