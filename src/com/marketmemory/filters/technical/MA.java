package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.Filter;
import com.marketmemory.filters.TechnicalFilterIntersect;
import com.patternly.datasource.BarDataWrapper;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

public class MA extends TechnicalFilterIntersect {
	private int period;
	private int start, end = Filter.maxInteger;
	private TAlib ta;
	private Position position;
	private double[] data;
	
	public MA(TAlib ta,
			int period, Position position, int start) throws IllegalArgumentException, Exception {
		this.ta = ta;
		if(period <=1)
			throw new IllegalArgumentException("period must be larger than 1");
		this.period = period;
		if(!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;
		if(start<=0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
	}
	
	public MA(TAlib ta,
			int period, Position position, int start, int end) throws IllegalArgumentException, Exception {
		this(ta, period, position, start);
		if(end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		this.data = ta.compute(period);
		
		int consecutiveDay = 0;
		for(int i=0; i<this.data.length; i++) {
			int index = i + ta.getIndexBegin();
			if((position.equals(Position.above) && rawData.getClose()[index]>this.data[i]) ||
					(position.equals(Position.below) && rawData.getClose()[index]<this.data[i])) {
				consecutiveDay++;
				if(consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			}
			else consecutiveDay = 0;
		}
		
		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		
		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("maResult", this.data);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "MA");
	}

}
