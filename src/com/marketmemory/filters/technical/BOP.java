package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

public class BOP extends TechnicalFilterIntersect{

	private TAlib ta;
	private Position position;
	private double[] BOPResult;
	private int start, end = Integer.MAX_VALUE;
	
	/**
	+	 * Author: Yang Meng
	+	 * BOP Constructor.
	+	 * 
	+	 * @param ta

	+	 * @param position
	+	 *            position.above selects where BOP is above 0,
	+	 *            position.below selects where BOP is below 0.
	+	 *            
	+    * @param BOPResult: result of BOP value.length should always be same as data provider	           
	+	 * @param start
	+	 * @throws Exception
	+	 */
	
	public BOP(TAlib ta,Position position, int start) throws IllegalArgumentException,
	Exception {
		this.ta = ta;
		this.position = position;
		

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
		
	}
	
	/**
	 * 
	 * @param ta
	 * @param position
	 * @param start
	 * @param end
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	public BOP(TAlib ta, Position position, int start, int end) throws IllegalArgumentException,
	Exception {
		
		this(ta, position,  start);

		if (start >= end)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
		
	}
	
	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();

		Map<String, Object> periods = new HashMap<String, Object>();
		periods.put("start", this.start);
		
		Map<String, Object> outputs = ta.compute(periods);
		
		if (outputs.size() > 0) {
			this.BOPResult = (double[]) outputs.get("BOPResult");
			
		} else {
			
			this.BOPResult = new double[] {};
		}
		
		int consecutiveDay = 0;

		for (int i = 0; i < this.BOPResult.length; i++) {
			int index = i + ta.getIndexBegin();

			if ( (this.BOPResult[i] > 0 && position.equals(Position.above)) ||
					(this.BOPResult[i] < 0 && position.equals(Position.below)) ) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end) {
					result.add(rawData.getDateTime()[index]);
				}

			} else {
				consecutiveDay = 0;
			}
		}
		
		return result;
		
	}

	
	

	 
	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("BOPResult", BOPResult);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "BOP");
	}

	
	
	
	
}
