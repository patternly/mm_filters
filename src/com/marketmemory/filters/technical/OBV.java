package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;
import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
 * Builds 2 types of date list based on user settings of the signal line of OBV.
 * Filters out dates with OBV above or below the chosen signal line.
 * The signal line for this method is either SMA or EMA of OBV line
 * 
 * @author Johnny Jiang
 * @since 2015-04-15
 *
 */
public class OBV extends TechnicalFilterIntersect{
	private TAlib ta;
	private boolean  isSMALine;
	private int smaPeriod, emaPeriod;  
	private Position position;
	private int start, end = Integer.MAX_VALUE;
	private static final int MAX_VALUE = 100000;
	
	private double[] obv;
	private double[] smaSignal;
	private double[] emaSignal;
	
	/**
	 * 
	 * @param ta
	 * @param isSMALine
	 * 			-- true when SMA line of OBV is chosen as the signal line
	 * @param smaPeriod
	 * 			-- integer period for SMA signal line; only used when SMA line is signal line
	 * @param emaPeriod
	 * 			-- integer period for EMA signal line; only used when EMA line is signal line
	 * @param position
	 * 			-- either above or below the chosen signal line
	 * @param start
	 * @throws Exception
	 */
	public OBV(TAlib ta, boolean isSMALine, int smaPeriod, int emaPeriod,
			Position position, int start) throws Exception{
		this.ta = ta;
		this.isSMALine = isSMALine;	
		
		if (isSMALine){
			if (smaPeriod < 2 || smaPeriod > MAX_VALUE)
				throw new IllegalArgumentException("SMA period cannot be smaller than 2!");
			this.smaPeriod = smaPeriod;
		} else {
			if (emaPeriod < 2 || emaPeriod > MAX_VALUE)
				throw new IllegalArgumentException("EMA period cannot be smaller than 2!");
			this.emaPeriod = emaPeriod;
		}
		
		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;
		
		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
		
	}
	
	public OBV(TAlib ta, boolean isSMALine, int smaPeriod, int emaPeriod, Position position, 
			int start, int end) throws Exception{
		this(ta, isSMALine, smaPeriod, emaPeriod, position, start);
		
		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}
	
	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception{
		DateList result = new DateList();
		
		Map<String, Object> periods = new HashMap<String, Object>();
		periods.put("isSMALine", this.isSMALine);
		periods.put("smaPeriod", this.smaPeriod);
		periods.put("emaPeriod", this.emaPeriod);
		Map<String, Object> outputs = ta.compute(periods);
		
		if (outputs.size() > 0) {
			this.obv = (double[]) outputs.get("OBV");
			if(this.isSMALine){
				this.smaSignal = (double[]) outputs.get("SMASignal");
			} else {
				this.emaSignal = (double[]) outputs.get("EMASignal");
			}
		} else {
			this.obv = new double[] {};
			this.smaSignal = new double[] {};
			this.emaSignal = new double[] {};
		}
		
		if (this.isSMALine){
			result = buildSMASignalResult(rawData, this.obv, this.smaSignal);
		} else {
			result = buildEMASignalResult(rawData, this.obv, this.emaSignal);
		}
		return result;
	}

	/**
	 * Builds date list based on EMA signal line
	 * 
	 * @param rawData
	 * @param obv
	 * @param emaSignal
	 * @return
	 */
	private DateList buildEMASignalResult(BarDataWrapper rawData, double[] obv, double[] emaSignal) {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < obv.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && obv[i] > emaSignal[i]) ||
					(position.equals(Position.below) && obv[i] < emaSignal[i])) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			}
			else
				consecutiveDay = 0;
		}

		return result;
	}
	
	/**
	 * Builds date list based on SMA signal line
	 * 
	 * @param rawData
	 * @param obv
	 * @param smaSignal
	 * @return
	 */
	private DateList buildSMASignalResult(BarDataWrapper rawData, double[] obv, double[] smaSignal) {
		DateList result = new DateList();
		int consecutiveDay = 0;

		for (int i = 0; i < obv.length; i++) {
			int index = i + ta.getIndexBegin();

			if ((position.equals(Position.above) && obv[i] > smaSignal[i]) ||
					(position.equals(Position.below) && obv[i] < smaSignal[i])) {
				consecutiveDay++;

				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			}
			else
				consecutiveDay = 0;
		}
		return result;
	}
	
	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		
		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("obvResult", this.obv);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "OBV");
	}
}