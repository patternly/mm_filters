package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;

/**
 * Builds out a date list based on user's inputs of three time frames, and filters
 * out the results based on the low limit and high limit, which are input by user
 * from filter method. 
 * 
 * 
 * @author Johnny Jiang
 *
 */

public class UltimateOsc extends TechnicalFilterIntersect{
	private TAlib ta;
	private int fastPeriod, mediumPeriod, slowPeriod;
	private double lowLimit, highLimit;
	private int start, end = Integer.MAX_VALUE;
	private static final int MAX_VALUE = 100000;
	
	private double[] ultimateOsc;
	
	/**
	 * 
	 * @param ta
	 * @param fastPeriod
	 * 		-- shortest time frame 1, default 7
	 * @param mediumPeriod
	 * 		-- time frame 2, default 14
	 * @param slowPeriod
	 * 		-- longest time frame 3, default 28
	 * @param lowLimit
	 * 		-- the lower bound of the filter; 
	 * 			if ultimateOsc is smaller than this value, it will be exclude in 
	 * 			the filtered results.	 * 			
	 * @param highLimit
	 * 		-- the higher bound of the filter; 
	 * 			if ultimateOsc is larger than this value, it will be exclude in 
	 * 			the filtered results.	 
	 * @param start
	 * @throws Exception
	 */
	public UltimateOsc(TAlib ta, int fastPeriod, int mediumPeriod, int slowPeriod, 
			double lowLimit, double highLimit, int start) throws Exception {
		this.ta = ta;
		
		if ( fastPeriod < 1 || mediumPeriod < 1 || slowPeriod < 1){
			throw new IllegalArgumentException("Invalid time periods");
		} else if (fastPeriod > MAX_VALUE || mediumPeriod  > MAX_VALUE || slowPeriod > MAX_VALUE){
			throw new IllegalArgumentException("Invalid time periods");
		} else if (fastPeriod > mediumPeriod || mediumPeriod > slowPeriod){
			throw new IllegalArgumentException("Invalid time periods");
		}
		this.fastPeriod = fastPeriod;
		this.mediumPeriod = mediumPeriod;
		this.slowPeriod = slowPeriod;
		
		if (lowLimit > 100 || lowLimit < 0){
			throw new IllegalArgumentException("Low limit must be between 0 and 100");
		} else if ( highLimit > 100 || highLimit < 0){
			throw new IllegalArgumentException("High limit must be between 0 and 100");
		} else if ( lowLimit > highLimit){
			throw new IllegalArgumentException("Low limit must be smaller than high limit");
		}
		this.lowLimit = lowLimit;
		this.highLimit = highLimit;
				
		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
		
	}
	
	public UltimateOsc(TAlib ta, int fastPeriod, int mediumPeriod, int slowPeriod, 
			double lowLimit, double highLimit, int start, int end) throws Exception {
		this(ta, fastPeriod, mediumPeriod, slowPeriod, lowLimit, highLimit, start);
		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}
	
	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception{
		DateList result = new DateList();
		
		Map<String, Object> periods = new HashMap<String, Object>();
		periods.put("fastPeriod", this.fastPeriod);
		periods.put("mediumPeriod", this.mediumPeriod);
		periods.put("slowPeriod", this.slowPeriod);
		
		Map<String, Object> output = ta.compute(periods);
		
		if(output.size()>0){
			this.ultimateOsc = (double[]) output.get("UltimateOsc");
		} else {
			this.ultimateOsc = new double[]{};
		}
		
		result = buildDateList(rawData, this.ultimateOsc);

		return result;
	}
	
	private DateList buildDateList(BarDataWrapper rawData, double[] ultimateOsc) {
		DateList result = new DateList();
		int consecutiveDay = 0;
		
			for (int i = 0; i < ultimateOsc.length; i++) {
				int index = i + ta.getIndexBegin();
	
				if ((ultimateOsc[i] >= this.lowLimit) && (ultimateOsc[i] <= this.highLimit)) {
					consecutiveDay++;
	
					if (consecutiveDay >= this.start && consecutiveDay <= this.end)
						result.add(rawData.getDateTime()[index]);
					
				} else {
					consecutiveDay = 0;
				}
			}
		

		return result;
	}
	
	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);
		
		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("UltimateOscResult", this.ultimateOsc);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "UltimateOsc");
	}
	
}