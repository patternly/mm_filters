package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;

/**
 * Triangle Moving Average(TMA) filter
 * 
 * @author Yang Meng
 *
 */

public class TMA extends TechnicalFilterIntersect {

	private TAlib ta;
	private int period;
	private Position position;
	private int start, end = Integer.MAX_VALUE;
	private double[] out;

	/**
	 * 
	 * @param ta
	 * @param period
	 *            The look-back period
	 * @param position
	 *            ABOVE will select when the stock price is higher than TMA.
	 *            BELOW will select when the stock price is lower than TMA.
	 * @param start
	 * @throws Exception
	 */
	
	//Constructor if there is no input value for end
	public TMA(TAlib ta, int period, Position position, int start) throws Exception {

		this.ta = ta;
		if (period <= 1)
			throw new IllegalArgumentException("period must be larger than 1");
		this.period = period;
		if (!position.equals(Position.below) && !position.equals(Position.above))
			throw new Exception("This position argument is not supported");
		this.position = position;
		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;

	}

/**
 * 
 * @param ta
 * @param period
 * @param position
 * @param start
 * @param end
 * @throws Exception
 */
	public TMA(TAlib ta, int period, Position position, int start, int end) throws Exception {

		this(ta, period, position, start);
		if (end < start)
			throw new IllegalArgumentException("end must be larger than start");
		this.end = end;

	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		this.out = ta.compute(period);

		int consecutiveDay = 0;
		for (int i = 0; i < this.out.length; i++) {
			int index = i + ta.getIndexBegin();
			if ((position.equals(Position.above) && rawData.getClose()[index] > this.out[i])
					|| (position.equals(Position.below) && rawData.getClose()[index] < this.out[i])) {
				consecutiveDay++;
				if (consecutiveDay >= this.start && consecutiveDay <= this.end)
					result.add(rawData.getDateTime()[index]);
			} else
				consecutiveDay = 0;
		}

		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("TMAResult", this.out);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "TMA");
	}

}
