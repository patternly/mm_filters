package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;

/**
 * The ZScore filter calculates the z-score based on user specifications, and
 * build a date list with dates that the z-score above or below user specified
 * limit.
 * 
 * @author Michael
 * 
 */
public class ZScore extends TechnicalFilterIntersect {
	private TAlib ta;
	private int period;
	private double limit;
	private boolean isAbove;
	private int start, end = Integer.MAX_VALUE;
	
	private double[] zScore;

	public ZScore(TAlib ta, int period, double limit, boolean isAbove, int start) {
		this.ta = ta;

		if (period <= 1) {
			throw new IllegalArgumentException("Period must be larger than 1");
		}
		this.period = period;
		
		this.limit = limit;
		this.isAbove = isAbove;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;
	}

	public ZScore(TAlib ta, int period, double limit, boolean isAbove, int start, int end) {
		this(ta, period, limit, isAbove, start);
		this.ta = ta;

		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		
		Map<String, Object> taInputs = new HashMap<String, Object>();
		taInputs.put("period", this.period);
		
		Map<String, Object> outputs = ta.compute(taInputs);
		
		double[] stDev;
		double[] sma;
		
		if (outputs.size() > 0) {
			stDev = (double[]) outputs.get("stDev");
			sma = (double[]) outputs.get("sma");
		} else {
			stDev = new double[] {};
			sma = new double[] {};
		}

		int consecutiveDay = 0;
		
		this.zScore = new double[ta.getResultLength()];
		
		for (int i = 0; i < ta.getResultLength(); i++) {
			int index = i + ta.getIndexBegin();
			
			// Calculate z-score.
			this.zScore[i] = (rawData.getClose()[index] - sma[i])/stDev[i];

			if ((isAbove && this.zScore[i] > limit) || (!isAbove && this.zScore[i] < limit)) {
				consecutiveDay++;
				
				if (consecutiveDay >= this.start && consecutiveDay <= this.end) {
				result.add(rawData.getDateTime()[index]);
				}
			} else {
				consecutiveDay = 0;
			}
		}
		
		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("zScore", this.zScore);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "ZScore");
	}

}
