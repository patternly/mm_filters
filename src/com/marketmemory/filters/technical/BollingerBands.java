package com.marketmemory.filters.technical;

import java.util.HashMap;
import java.util.Map;

import com.marketmemory.filters.TechnicalFilterIntersect;
import com.marketmemory.filters.TACompute.TAlib;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;

/**
 * The bollinger bands filter calculates the bollinger bands results based on
 * user settings, and build a date list with dates that the correspoinding
 * closing value is above/below the bollinger bands results.
 * 
 * @author Michael
 * 
 */
public class BollingerBands extends TechnicalFilterIntersect {
	private TAlib ta;
	private int period;
	private double stdDev;
	private boolean isUpperBand;
	private int start, end = Integer.MAX_VALUE;

	private double[] upperBand;
	private double[] middleBand;
	private double[] lowerBand;

	public BollingerBands(TAlib ta, int period, double stdDev, boolean isUpperBand, int start) {
		this.ta = ta;

		if (period <= 1) {
			throw new IllegalArgumentException("Period must be larger than 1");
		}
		this.period = period;

		if (stdDev < 1) {
			throw new IllegalArgumentException("Standard deviation must be larger than or equal to 1");
		}
		this.stdDev = stdDev;

		this.isUpperBand = isUpperBand;

		if (start <= 0)
			throw new IllegalArgumentException("start must be larger than 0");
		this.start = start;

	}

	public BollingerBands(TAlib ta, int period, double stdDev, boolean isUpperBand, int start,
			int end) {
		this(ta, period, stdDev, isUpperBand, start);
		this.ta = ta;

		if (end < start)
			throw new IllegalArgumentException("end should not be smaller than start");
		this.end = end;
	}

	private DateList buildFilterDateList(BarDataWrapper rawData) throws Exception {
		DateList result = new DateList();
		Map<String, Object> taInputs = new HashMap<String, Object>();
		taInputs.put("period", this.period);
		taInputs.put("stdDev", this.stdDev);

		Map<String, Object> outputs = ta.compute(taInputs);

		if (outputs.size() > 0) {
			this.upperBand = (double[]) outputs.get("upperBand");
			this.middleBand = (double[]) outputs.get("middleBand");
			this.lowerBand = (double[]) outputs.get("lowerBand");
		} else {
			this.upperBand = new double[] {};
			this.lowerBand = new double[] {};
			this.middleBand = new double[] {};
		}

		int consecutiveDay = 0;
		
		for (int i = 0; i < ta.getResultLength(); i++) {
			int index = i + ta.getIndexBegin();

			if ((isUpperBand && this.upperBand[i] < rawData.getClose()[index]) ||
					(!isUpperBand && this.lowerBand[i] > rawData.getClose()[index])) {
				consecutiveDay++;
				
				if (consecutiveDay >= this.start && consecutiveDay <= this.end) {
				result.add(rawData.getDateTime()[index]);
				}
			} else {
				consecutiveDay = 0;
			}
		}

		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarDataWrapper rawData) throws Exception {
		DateList dateList = buildFilterDateList(rawData);

		Map<String, double[]> results = new HashMap<String, double[]>();
		results.put("upperBand", this.upperBand);
		results.put("middleBand", this.middleBand);
		results.put("lowerBand", this.lowerBand);
		return new FilterResult(dateList, results, ta.getIndexBegin(), ta.getResultLength(), "BollingerBands");
	}

}
