package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * Retrieve slope(linear regression) from TA-Lib.
 * 
 * @author Johnny Jiang
 *
 */
public class SlopeCompute extends TAlib{
	
	public SlopeCompute(BarDataWrapper barSeries){
		super(barSeries);
	}
	
	@Override
	protected RetCode runTALib() throws Exception{
		double[] slope = new double[barSeries.size()];
		
		RetCode result = c.linearRegSlope(0, barSeries.size()-1, barSeries.getClose(), 
				(Integer)inputs.get("timePeriod"), indexBegin, resultLength, slope);
 		
		if (result == RetCode.Success){
			outputs.put("slope", Arrays.copyOf(slope, resultLength.value));
		}
		
		return result;
	}
}