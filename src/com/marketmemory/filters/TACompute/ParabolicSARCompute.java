package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * Retrieve Parabolic SAR based on user inputs of
 * base acceleration factor & maximum acceleration factor
 * 
 * @author Johnny Jiang
 *
 */

public class ParabolicSARCompute extends TAlib{
	
	public ParabolicSARCompute(BarDataWrapper barSeries){
		super(barSeries);
	}
	
	@Override
	protected RetCode runTALib() throws Exception{
		double[] ParabolicSAR = new double[barSeries.size()];
		
		RetCode result = c.sar(0, barSeries.size() - 1, barSeries.getHigh(), 
				barSeries.getLow(), (Double)inputs.get("baseAF"), 
				(Double)inputs.get("maxAF"),indexBegin, resultLength, ParabolicSAR);
		
		if(result == RetCode.Success){
			outputs.put("parabolicSAR", Arrays.copyOf(ParabolicSAR, resultLength.value));
		}
		
		return result;
	}
}