package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

// author: Yang Meng
public class ADXCompute extends TAlib{
	
	public ADXCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	protected RetCode runTALib() throws Exception {

		double[] ADXResult = new double[barSeries.size()];
		double[] minus_DI = new double[barSeries.size()];
		double[] plus_DI = new double[barSeries.size()];

		if( (boolean)inputs.get("isCrossOver")== false){
			RetCode result1 = c.adx(0, barSeries.size() - 1, barSeries.getHigh(),barSeries.getLow(),barSeries.getClose(), (Integer) inputs.get("period"),
					 indexBegin, resultLength,
					ADXResult);
			if (result1 == RetCode.Success)  {
				outputs.put("ADXResult", Arrays.copyOf(ADXResult, resultLength.value));
				} 
			return result1;
		}

		else
			
			{
			RetCode result2 = c.minusDI(0, barSeries.size() - 1, barSeries.getHigh(),barSeries.getLow(),barSeries.getClose(), (Integer) inputs.get("period"),
					 indexBegin, resultLength,
					 minus_DI);
			if (result2 == RetCode.Success ) {
				outputs.put("minus_DI", Arrays.copyOf(minus_DI, resultLength.value));
				
			}
			
			RetCode result3 = c.plusDI(0, barSeries.size() - 1, barSeries.getHigh(),barSeries.getLow(),barSeries.getClose(), (Integer) inputs.get("period"),
					 indexBegin, resultLength,
					 plus_DI);
			if (result3 == RetCode.Success){
				outputs.put("plus_DI", Arrays.copyOf(plus_DI, resultLength.value));
				
			}
			
			if (result3 != RetCode.Success) {
				System.err.println("Error while computing TA");
				throw new Exception(result3.toString());
			}
			
			
			
			return result2;
		}

		
		
	}
	
	
	
	

}
