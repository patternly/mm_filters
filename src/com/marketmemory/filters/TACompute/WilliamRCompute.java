package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * Obtain williamR from TA-lib;
 * 
 * @author Johnny Jiang
 *
 */


public class WilliamRCompute extends TAlib{
	
	public WilliamRCompute(BarDataWrapper barSeries){
		super(barSeries);
	}
	
	@Override
	protected RetCode runTALib()throws Exception{
		double[] williamR = new double[barSeries.size()];
		
		RetCode result = c.willR(0, barSeries.size()-1, barSeries.getHigh(), 
				barSeries.getLow(), barSeries.getClose(), (Integer)inputs.get("timePeriod"),
				indexBegin, resultLength, williamR);
		
		if (result == RetCode.Success) {
			outputs.put("williamR", Arrays.copyOf(williamR, resultLength.value));
		}
		
		return result;
	}
}