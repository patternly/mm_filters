package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;


/**
 * 
 * @author Yangqiao Meng
 *
 */
public class CCICompute extends TAlib{
	
	public CCICompute(BarDataWrapper barSeries) {
		super(barSeries);
	}
 
	@Override
	public RetCode runTALib() throws Exception {
		double[] out = new double[barSeries.size()];
		RetCode result = c.cci(0, out.length - 1, barSeries.getHigh(),barSeries.getLow(),barSeries.getClose(), (Integer) inputs.get("period"), indexBegin,
				resultLength, out);
		
		if (result == RetCode.Success) {
			outputs.put("out", Arrays.copyOf(out, resultLength.value));
		}
		return result;
	}

}
