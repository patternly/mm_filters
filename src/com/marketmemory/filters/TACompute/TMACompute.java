package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * TACompute file for (Triangle Moving Average)TMA 
 * @author Yang Meng
 *
 */
public class TMACompute extends TAlib{
	
	public TMACompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	public RetCode runTALib() throws Exception {
		double[] out = new double[barSeries.size()];
		RetCode result =  c.trima(0, out.length - 1, barSeries.getClose(), (Integer) inputs.get("period"), indexBegin,
				resultLength, out);
		
		if (result == RetCode.Success) {
			outputs.put("out", Arrays.copyOf(out, resultLength.value));
		}
		return result;
	}

}
