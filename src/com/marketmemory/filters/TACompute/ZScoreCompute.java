package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * Get TA library results stDev and sma for ZScore filter.
 * 
 * @author Michael
 *
 */
public class ZScoreCompute extends TAlib {

	public ZScoreCompute(BarDataWrapper barSeries2) {
		super(barSeries2);
	}

	@Override
	protected RetCode runTALib() throws Exception {
		double[] stDev = new double[barSeries.size()];
		double[] sma = new double[barSeries.size()];

		RetCode stDevResult = c.stdDev(0, stDev.length - 1, barSeries.getClose(), (Integer) inputs.get("period"), 1,
				indexBegin, resultLength, stDev);

		RetCode smaResult = c.sma(0, sma.length - 1, barSeries.getClose(), (Integer) inputs.get("period"), indexBegin,
				resultLength, sma);

		if (stDevResult == RetCode.Success && smaResult == RetCode.Success) {
			outputs.put("stDev", Arrays.copyOf(stDev, resultLength.value));
			outputs.put("sma", Arrays.copyOf(sma, resultLength.value));
		}

		return stDevResult;
	}

}
