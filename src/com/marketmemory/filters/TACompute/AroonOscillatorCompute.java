package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * Retrieve aroonOsillator from TA-lib
 * 
 * @author Johnny Jiang
 *
 */
public class AroonOscillatorCompute extends TAlib {
	
	public AroonOscillatorCompute(BarDataWrapper barSeries){
		super(barSeries);
	}
	
	@Override
	protected RetCode runTALib() throws Exception{
		double[] aroonOsillator = new double[barSeries.size()];
		
		RetCode result = c.aroonOsc(0, barSeries.size() - 1, 
				barSeries.getHigh(), barSeries.getLow(), 
				(Integer)inputs.get("timePeriod"),
				indexBegin, resultLength, aroonOsillator);
		
		if(result == RetCode.Success){
			outputs.put("aroonOscillator", Arrays.copyOf(aroonOsillator, resultLength.value));
		}
		
		return result;
	}
}