package com.marketmemory.filters.TACompute;

import java.lang.reflect.Method;
import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.patternly.util.CandlestickPatternsType;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

/**
 * Get results from TA library for Candlestick Patterns filter.
 * 
 * @author Michael
 * 
 */
public class CandlestickPatternsCompute extends TAlib {

	public CandlestickPatternsCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	protected RetCode runTALib() throws Exception {
		int[] output = new int[barSeries.size()];
		
		Object obj = c.getClass().newInstance();
		CandlestickPatternsType patternType = (CandlestickPatternsType) inputs.get("patterntype");
		
		Class<?>[] argTypes = new Class[] { int.class, int.class, double[].class, double[].class, double[].class, double[].class,
				 MInteger.class, MInteger.class, int[].class};
		
		RetCode result;
		
		if (patternType.additionalArg != null) {
			argTypes = new Class[] { int.class, int.class, double[].class, double[].class, double[].class, double[].class,
					 double.class, MInteger.class, MInteger.class, int[].class};
			Method method = c.getClass().getDeclaredMethod(patternType.methodName, argTypes);
			
			result = (RetCode) method.invoke(obj, 0, barSeries.size() - 1, barSeries.getOpen(), barSeries.getHigh(), barSeries.getLow(),
					barSeries.getClose(), patternType.additionalArg, indexBegin, resultLength, output);
			
		} else {

			Method method = c.getClass().getDeclaredMethod(patternType.methodName, argTypes);
			result = (RetCode) method.invoke(obj, 0, barSeries.size() - 1, barSeries.getOpen(), barSeries.getHigh(), barSeries.getLow(),
					barSeries.getClose(), indexBegin, resultLength, output);
		}
			
		if (result == RetCode.Success) {
			outputs.put(inputs.get("patterntype").toString(), Arrays.copyOf(output, resultLength.value));
		}
		
		return result;
	}

}
