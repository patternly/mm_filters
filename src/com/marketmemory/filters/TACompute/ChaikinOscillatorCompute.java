package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;
/**
 * 
 * This is the file to compute Chaikin Oscillator result
 * 
 * @author Yang Meng
 *
 */
public class ChaikinOscillatorCompute extends TAlib{
	
	public ChaikinOscillatorCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}
	
	@Override
	protected RetCode runTALib() throws Exception {

		double[] ChaikinOscResult = new double[barSeries.size()];

		RetCode result = c.adOsc(0, barSeries.size() - 1, barSeries.getHigh(),barSeries.getLow(),barSeries.getClose(),
				barSeries.getVolume(),(Integer) inputs.get("fastPeriod"),
				(Integer) inputs.get("slowPeriod"), indexBegin, resultLength,
				ChaikinOscResult);

		if (result == RetCode.Success) {
			outputs.put("ChaikinOscResult", Arrays.copyOf(ChaikinOscResult, resultLength.value));
			
		}

		return result;
	}
	
}
