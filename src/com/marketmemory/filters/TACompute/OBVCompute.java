package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;


/**
 * Retrieve OBV Line from TA-lib and compute SMA & EMA of the result as 
 * signal lines.
 * 
 * @author Johnny Jiang
 *
 */
public class OBVCompute extends TAlib{
	
	public OBVCompute(BarDataWrapper barSeries){
		super(barSeries);
	}
	
	@Override
	protected RetCode runTALib() throws Exception{
		MInteger signalIndexSMA = new MInteger();
		MInteger signalIndexEMA = new MInteger();
		MInteger signalLengthSMA = new MInteger();
		MInteger signalLengthEMA = new MInteger();
		MInteger resultIndex = new MInteger();
		MInteger resultLen = new MInteger();
		RetCode signalSMA = RetCode.BadParam;
		RetCode signalEMA = RetCode.BadParam;
		double[] obv = new double[barSeries.size()];
		double[] smaSignal = new double[barSeries.size()];
		double[] emaSignal = new double[barSeries.size()];
		
		RetCode result = c.obv(0, barSeries.size()-1, barSeries.getClose(), barSeries.getVolume(), 
				resultIndex, resultLen, obv);
		
		if((Boolean) inputs.get("isSMALine")){
			signalSMA = c.sma(0, resultLen.value - 1, obv, 
					(Integer)inputs.get("smaPeriod"), signalIndexSMA, 
					signalLengthSMA, smaSignal);
			indexBegin.value = resultIndex.value + signalIndexSMA.value;
			resultLength.value = signalLengthSMA.value;
		} else {
			signalEMA = c.ema(0, resultLen.value-1, obv, 
					(Integer)inputs.get("emaPeriod"), signalIndexEMA, 
					signalLengthEMA, emaSignal);
			indexBegin.value = resultIndex.value + signalIndexEMA.value;
			resultLength.value = signalLengthEMA.value;
		}
				
		if ((result == RetCode.Success && signalSMA == RetCode.Success)){
			outputs.put("OBV", Arrays.copyOfRange(obv, signalIndexSMA.value,
					signalIndexSMA.value + signalLengthSMA.value));
			outputs.put("SMASignal", Arrays.copyOf(smaSignal, signalLengthSMA.value));
		} else if(result == RetCode.Success && signalEMA == RetCode.Success){
			outputs.put("OBV", Arrays.copyOfRange(obv, signalIndexEMA.value,
					signalIndexEMA.value + signalLengthEMA.value));
			outputs.put("EMASignal", Arrays.copyOf(emaSignal, signalLengthEMA.value));
		}
		
		return result;
	}
}