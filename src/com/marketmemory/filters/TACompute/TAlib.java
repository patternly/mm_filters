package com.marketmemory.filters.TACompute;

import java.util.HashMap;
import java.util.Map;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

/**
 * TAlib class is an abstract implementation of calling TA library.
 *
 */
public abstract class TAlib {
	protected MInteger indexBegin;
	protected MInteger resultLength;
	protected Core c;
	protected BarDataWrapper barSeries;
	Map<String, Object> inputs;
	Map<String, Object> outputs;

	public TAlib(BarDataWrapper barSeries2) {
		c = new Core();
		indexBegin = new MInteger();
		resultLength = new MInteger();
		this.barSeries = barSeries2;
	}

	public int getIndexBegin() {
		return indexBegin.value;
	}

	public int getResultLength() {
		return resultLength.value;
	}
	
	protected abstract RetCode runTALib() throws Exception;
	
	public Map<String, Object> compute(Map<String, Object> inputs) throws Exception {
		
		this.inputs = inputs;
		outputs = new HashMap<String, Object>();
		// This checking is not only optimization of performance, but also prevent the error of negative value of last index
		// namely, (out.length -1) in runTaLib
		if(barSeries.size() == 0) {
			return new HashMap<String, Object>();
		}
		
		RetCode retCode = runTALib();
		
		if (retCode != RetCode.Success) {
			System.err.println("Error while computing TA");
			throw new Exception(retCode.toString());
		}
		
		return outputs;
	}
	
	public double[] compute(int period) throws Exception {
		Map<String, Object> periods = new HashMap<String, Object>();
		periods.put("period", period);
		compute(periods);
		if(outputs.size() >0) {
			return (double[]) outputs.get("out");
		} else {
			return new double[] {};
		}
	}
	

	
}