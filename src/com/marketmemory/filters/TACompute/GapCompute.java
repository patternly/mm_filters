package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

public class GapCompute extends TAlib {

	public GapCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	public RetCode runTALib() throws Exception {
		double[] out = new double[barSeries.size()];
		
		//INFO: skipping 0 because we can't compute gap value for first record
		for(int i=1; i<out.length; i++) {
			out[i-1] = (barSeries.getOpen()[i] - barSeries.getClose()[i-1])/barSeries.getClose()[i-1];
		}
		indexBegin.value = 1;
		resultLength.value = out.length - 1;
		outputs.put("out", Arrays.copyOf(out, resultLength.value));
		
		return RetCode.Success;
	}
}
