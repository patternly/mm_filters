package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;
/**
 * 
 * @author Yang Meng
 * 
 */
public class BOPCompute extends TAlib{
	
	public BOPCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	public RetCode runTALib() throws Exception {

		double[] BOPResult = new double[barSeries.size()];
		
			RetCode result = c.bop(0, barSeries.size() - 1, barSeries.getOpen(),barSeries.getHigh(),barSeries.getLow(),barSeries.getClose(), 
					 indexBegin, resultLength,
					BOPResult);
			if (result == RetCode.Success)  {
				outputs.put("BOPResult", Arrays.copyOf(BOPResult, resultLength.value));
				} 
			return result;
		}



}
