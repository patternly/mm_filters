package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * Retrieve TEMA��from TA-lib;
 * Retrieve DEMA value from TA-lib;
 * The length of DEMA returned by TA-lib is determined by the following formula:
 * N - 3*(n-1)
 * Where N is the # of input bar series, and n is the timePeriod
 * 
 * @author Johnny Jiang
 * @lastUpdated March 6, 2015
 * 
 */
public class TEMACompute extends TAlib{
	public TEMACompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	public RetCode runTALib() throws Exception {
		double[] tema = new double[barSeries.size()];
		
		RetCode result =  c.tema(0, barSeries.size()-1, barSeries.getClose(), 
				(Integer)inputs.get("period"), indexBegin, resultLength, tema);
		
		if (result == RetCode.Success) {
			outputs.put("out", Arrays.copyOf(tema, resultLength.value));
		}
		return result;
	}
}