package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * Get results from TA library for MACD filter.
 * 
 * @author Michael
 * 
 */
public class MACDCompute extends TAlib {

	public MACDCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	protected RetCode runTALib() throws Exception {

		double[] macd = new double[barSeries.size()];
		double[] macdSignal = new double[barSeries.size()];
		double[] macdHist = new double[barSeries.size()];

		RetCode result = c.macd(0, barSeries.size() - 1, barSeries.getClose(), (Integer) inputs.get("fastPeriod"),
				(Integer) inputs.get("slowPeriod"), (Integer) inputs.get("signalPeriod"), indexBegin, resultLength,
				macd, macdSignal, macdHist);

		if (result == RetCode.Success) {
			outputs.put("macd", Arrays.copyOf(macd, resultLength.value));
			outputs.put("macdSignal", Arrays.copyOf(macdSignal, resultLength.value));
			outputs.put("macdHist", Arrays.copyOf(macdHist, resultLength.value));
		}

		return result;
	}

}
