package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * Retrieve DEMA value from TA-lib;
 * The length of DEMA returned is determined by the following formula:
 * N - 2*(n-1)
 * Where N is the # of input bar series, and n is the timePeriod
 * 
 * @author Johnny Jiang
 *
 */
public class DEMACompute extends TAlib{
	public DEMACompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	public RetCode runTALib() throws Exception {
		double[] dema = new double[barSeries.size()];
		
		RetCode result =  c.dema(0, barSeries.size()-1, barSeries.getClose(), 
				(Integer)inputs.get("period"), indexBegin, resultLength, dema);
		
		if (result == RetCode.Success) {
			outputs.put("out", Arrays.copyOf(dema, resultLength.value));
		}
		return result;
	}
}