package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;


/**
 * Retrieve A/D Line from TA-lib and compute SMA of the result & OBV as 
 * signal line.
 * 
 * @author Johnny Jiang
 *
 */
public class ADLineCompute extends TAlib{
	
	public ADLineCompute(BarDataWrapper barSeries){
		super(barSeries);
	}
	
	@Override
	protected RetCode runTALib() throws Exception{
		MInteger signalIndex = new MInteger();
		MInteger resultIndex = new MInteger();
		MInteger signalLength = new MInteger();
		MInteger resultLen = new MInteger();
		MInteger obvLength = new MInteger();
		MInteger obvIndex = new MInteger();
		double[] adLine = new double[barSeries.size()];
		double[] smaSignal = new double[barSeries.size()];
		double[] obvSignal = new double[barSeries.size()];
		
		RetCode result = c.ad(0, barSeries.size()-1, barSeries.getHigh(),
				barSeries.getLow(), barSeries.getClose(), barSeries.getVolume(), 
				resultIndex, resultLen, adLine);
		
		RetCode signalSMA = c.sma(0, resultLen.value-1, adLine, 
				(Integer)inputs.get("signalPeriod"), signalIndex, 
				signalLength, smaSignal);
		
		RetCode signalOBV = c.obv(0, barSeries.size()-1, barSeries.getClose(),
				barSeries.getVolume(), obvIndex, obvLength, obvSignal);
		
		indexBegin.value = resultIndex.value + signalIndex.value;
		resultLength.value = signalLength.value;
		
		if (result == RetCode.Success && signalSMA == RetCode.Success && signalOBV == RetCode.Success){
			outputs.put("ADLine", Arrays.copyOfRange(adLine, signalIndex.value,
					signalIndex.value+signalLength.value));
			outputs.put("SMASignal", Arrays.copyOf(smaSignal, signalLength.value));
			outputs.put("OBVSignal", Arrays.copyOfRange(obvSignal, signalIndex.value,
					signalIndex.value+signalLength.value));
		}
		
		return result;
		
	}
}