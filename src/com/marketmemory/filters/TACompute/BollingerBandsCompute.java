package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.MAType;
import com.tictactec.ta.lib.RetCode;

/**
 * Get results from TA library for Bollinger Bands filter.
 * 
 * @author Michael
 * 
 */
public class BollingerBandsCompute extends TAlib {

	public BollingerBandsCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	protected RetCode runTALib() throws Exception {

		double[] upperBand = new double[barSeries.size()];
		double[] middleBand = new double[barSeries.size()];
		double[] lowerBand = new double[barSeries.size()];

		RetCode result = c.bbands(0, barSeries.size() - 1, barSeries.getClose(), (Integer) inputs.get("period"),
				(Double) inputs.get("stdDev"),
				(Double) inputs.get("stdDev"), MAType.Sma, indexBegin, resultLength, upperBand, middleBand, lowerBand);

		if (result == RetCode.Success) {
			outputs.put("upperBand", Arrays.copyOf(upperBand, resultLength.value));
			outputs.put("middleBand", Arrays.copyOf(middleBand, resultLength.value));
			outputs.put("lowerBand", Arrays.copyOf(lowerBand, resultLength.value));
		}

		return result;
	}

}
