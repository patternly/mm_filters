package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

/**
 * Caculate CMF using TA-lib ADLine, Summation & Division functions;
 * CMF = ADLine/Volume;
 * 
 * 
 * @author Johnny Jiang
 *
 */

public class CMFCompute extends TAlib {

	public CMFCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	protected RetCode runTALib() throws Exception {
		int period = (Integer) inputs.get("period");
		MInteger sumIndex = new MInteger();
		MInteger sumLen = new MInteger();
		MInteger adIndex = new MInteger();
		MInteger adLen = new MInteger();
		MInteger divIndex = new MInteger();
		MInteger divLen = new MInteger();
		double[] adLineSub = new double[period]; 
		double[] adLine = new double[barSeries.size()];
		double[] volSum = new double[barSeries.size()];
		double[] cmf = new double[barSeries.size()];
		
		indexBegin.value = period - 1;
		//Since ad functions doesn't allow specificaiton of a time period, 
		//this loop extracts last element of adLineSub to form the adLine array we need;
		for(int i = 0; i < barSeries.size() - indexBegin.value; i++){
			RetCode ad = c.ad(i, i + period-1, barSeries.getHigh(), barSeries.getLow(), barSeries.getClose(),
					barSeries.getVolume(), adIndex, adLen, adLineSub);
			adLine[i] = adLineSub[adLen.value - 1];
		}

		RetCode sumVol = c.sum(0, barSeries.size() - 1, barSeries.getVolume(), (Integer) inputs.get("period"),
				sumIndex, sumLen, volSum);
	
		RetCode result = c.div(0, sumLen.value - 1, adLine, 
				volSum, divIndex, divLen, cmf);
		
		resultLength.value = divLen.value;
	
		if (result == RetCode.Success) {
			outputs.put("CMF", Arrays.copyOf(cmf, resultLength.value));
		}

		return result;
	}
}