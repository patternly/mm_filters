package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * Get results from TA library for Donchian Channels filter.
 * 
 * @author Michael
 *
 */
public class DonchianChannelsCompute extends TAlib {

	public DonchianChannelsCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	protected RetCode runTALib() throws Exception {

		int[] minIndex = new int[barSeries.size()];
		int[] maxIndex = new int[barSeries.size()];
		double[] minValue = new double[barSeries.size()];
		double[] maxValue = new double[barSeries.size()];
		
		RetCode result = null;

		RetCode minIndexResult = c.minIndex(0, barSeries.size() - 1, barSeries.getLow(), (Integer) inputs.get("period"),
				indexBegin, resultLength, minIndex);
		
		RetCode maxIndexResult = c.maxIndex(0, barSeries.size() - 1, barSeries.getHigh(), (Integer) inputs.get("period"),
				indexBegin, resultLength, maxIndex);
		
		RetCode minValueResult = c.min(0, barSeries.size() - 1, barSeries.getLow(), (Integer) inputs.get("period"),
				indexBegin, resultLength, minValue);
		
		RetCode maxValueResult = c.max(0, barSeries.size() - 1, barSeries.getHigh(), (Integer) inputs.get("period"),
				indexBegin, resultLength, maxValue);

		if (minIndexResult == RetCode.Success) {
			outputs.put("minIndex", Arrays.copyOf(minIndex, resultLength.value));
		} else {
			result = minIndexResult;
		}
		
		if (maxIndexResult == RetCode.Success) {
			outputs.put("maxIndex", Arrays.copyOf(maxIndex, resultLength.value));
		} else {
			result = minIndexResult;
		}
		
		if (minValueResult == RetCode.Success) {
			outputs.put("minValue", Arrays.copyOf(minValue, resultLength.value));
		} else {
			result = minIndexResult;
		}
		
		if (maxValueResult == RetCode.Success) {
			outputs.put("maxValue", Arrays.copyOf(maxValue, resultLength.value));
		} else {
			result = minIndexResult;
		}
		
		if(result == null) {
			return RetCode.Success;
		} else {
			return result;
		}
	}

}
