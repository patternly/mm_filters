package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * Get results from TA library for RSI filter.
 * 
 * @author Michael
 * 
 */
public class RSICompute extends TAlib {

	public RSICompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	public RetCode runTALib() throws Exception {
		double[] out = new double[barSeries.size()];
		RetCode result = c.rsi(0, out.length - 1, barSeries.getClose(), (Integer) inputs.get("period"), indexBegin,
				resultLength, out);
		
		if (result == RetCode.Success) {
			outputs.put("out", Arrays.copyOf(out, resultLength.value));
		}
		return result;
	}

}
