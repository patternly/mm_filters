package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

/**
 * Uses c.ema to calculate the signal line for TRIX.
 * Then get the results for TRIX from TA library. Put both signal
 * line and TRIX into the result arrays. 
 * indexBegin = 3(n-1)+1-(m-1); n = timePeriod, m = signalPeriod
 * resultLength = signalLength
 * 
 * @author Johnny Jiang
 *
 */

public class TRIXCompute extends TAlib {

	public TRIXCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	protected RetCode runTALib() throws Exception {
		MInteger signalIndex = new MInteger();
		MInteger resultIndex = new MInteger();
		MInteger signalLength = new MInteger();
		MInteger resultLen = new MInteger();
		double[] trix = new double[barSeries.size()];
		double[] trixSignal = new double[barSeries.size()];

		RetCode result = c.trix(0, barSeries.size() - 1, barSeries.getClose(), (Integer) inputs.get("timePeriod"),
				resultIndex, resultLen, trix);
		
		RetCode singalLine = c.ema(0, resultLen.value-1,trix,
				(Integer) inputs.get("signalPeriod"), signalIndex, signalLength, trixSignal);

		indexBegin.value = resultIndex.value + signalIndex.value;
		resultLength.value = signalLength.value;
		
		if (singalLine != RetCode.Success) {
			System.err.println("Error while computing TRIX signal");
			throw new Exception(singalLine.toString());
		}
		if (result != RetCode.Success) {
			System.err.println("Error while computing TRIX");
			throw new Exception(result.toString());
		}
		outputs.put("trixSignal",Arrays.copyOf(trixSignal,signalLength.value));
		outputs.put("trix",	Arrays.copyOfRange(trix,signalIndex.value,
				signalIndex.value+resultLength.value));

		return result;
	}

}