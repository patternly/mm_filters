package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

public class ATRCompute extends TAlib {

	public ATRCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	public RetCode runTALib() throws Exception {
		double[] out = new double[barSeries.size()];
		RetCode result =  c.atr(0, out.length -1, barSeries.getHigh(), barSeries.getLow(), barSeries.getClose(),
				(Integer) inputs.get("period"), indexBegin, resultLength, out);
		
		if (result == RetCode.Success) {
			outputs.put("out", Arrays.copyOf(out, resultLength.value));
		}
		return result;
	}
}
