package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.MAType;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

/**
 * Obtain results from TA-lib for Percetange Price Oscillator;
 * Compute the ema of ppo as signal line.
 * Calculate and put PPO histogram by ppo - signalLine.
 * 
 * @author Johnny Jiang
 *
 */


public class PPOCompute extends TAlib {

	public PPOCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	protected RetCode runTALib() throws Exception {
		MInteger signalIndex = new MInteger();
		MInteger signalLength = new MInteger();
		MInteger resultIndex = new MInteger();
		MInteger resultLen = new MInteger();
		double[] ppo = new double[barSeries.size()];
		double[] ppoSignal = new double[barSeries.size()];
		double[] ppoHist = new double[barSeries.size()];
		MAType EMA = MAType.Ema;

		RetCode result = c.ppo(0, barSeries.size()-1, barSeries.getClose(), 
				(Integer) inputs.get("fastPeriod"), (Integer) inputs.get("slowPeriod"), 
				EMA, resultIndex, resultLen, ppo);
		
		RetCode signal = c.ema(0, resultLen.value-1, ppo, 
				(Integer) inputs.get("signalPeriod"), signalIndex, signalLength, ppoSignal);
		
		indexBegin.value = resultIndex.value + signalIndex.value;
		resultLength.value = signalLength.value;
		
		//ppoHist = PPO - Signal Line
		for(int i = 0; i < signalLength.value; i++){
			ppoHist[i] = ppo[i+signalIndex.value] - ppoSignal[i];
		}
		
		if (result == RetCode.Success && signal == RetCode.Success) {
			outputs.put("ppoSignal", Arrays.copyOf(ppoSignal, signalLength.value));
			outputs.put("ppoResult", Arrays.copyOfRange(ppo, signalIndex.value, 
					signalIndex.value+resultLength.value));
			outputs.put("ppoHist", Arrays.copyOf(ppoHist, signalLength.value));
		}

		return result;
	}

}