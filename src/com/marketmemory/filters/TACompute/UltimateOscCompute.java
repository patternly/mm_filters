package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * 
 * Obtain Ultimate Oscillator results from TA-Lib.
 * 
 * @author Johnny Jiang
 *
 */
public class UltimateOscCompute extends TAlib {

	public UltimateOscCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}
	
	@Override
	protected RetCode runTALib() throws Exception {
		double[] ultimateOsc = new double[barSeries.size()];
		
		RetCode result = c.ultOsc(0, barSeries.size()-1, barSeries.getHigh(), 
				barSeries.getLow(), barSeries.getClose(), 
				(Integer)inputs.get("fastPeriod"), (Integer)inputs.get("mediumPeriod"), 
				(Integer)inputs.get("slowPeriod"), indexBegin, resultLength, ultimateOsc);
		
		if(result == RetCode.Success){
			outputs.put("UltimateOsc", Arrays.copyOf(ultimateOsc, resultLength.value));
		}
		
		return result;
	}
}