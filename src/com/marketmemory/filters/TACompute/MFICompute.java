package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;
/**
 * 
 * @author Yang Meng
 * This is the file to compute MFI index
 *
 */
public class MFICompute extends TAlib{
	
	public MFICompute(BarDataWrapper barSeries) {
		super(barSeries);
	}

	@Override
	public RetCode runTALib() throws Exception {
		double[] out = new double[barSeries.size()];
		RetCode result = c.mfi(0, out.length - 1, barSeries.getHigh(),barSeries.getLow(),barSeries.getClose(),barSeries.getVolume(), (Integer) inputs.get("period"), indexBegin,
				resultLength, out);
		
		if (result == RetCode.Success) {
			outputs.put("out", Arrays.copyOf(out, resultLength.value));
		}
		return result;
	}

}
