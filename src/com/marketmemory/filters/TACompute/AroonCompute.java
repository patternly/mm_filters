package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * Computes Aroon using TA-lib.
 * 
 * @author Johnny Jiang
 *
 */
public class AroonCompute extends TAlib{
	public AroonCompute(BarDataWrapper barSeries){
		super(barSeries);
	}
	

	@Override
	protected RetCode runTALib() throws Exception {
		double[] aroonUp = new double[barSeries.size()];
		double[] aroonDown = new double[barSeries.size()];
		
		RetCode result = c.aroon(0, barSeries.size() - 1, barSeries.getHigh(), barSeries.getLow(),
				(Integer) inputs.get("period"),indexBegin, resultLength,
				aroonDown, aroonUp);
		
		if(result == RetCode.Success){
			outputs.put("aroonUp", Arrays.copyOf(aroonUp, resultLength.value));
			outputs.put("aroonDown", Arrays.copyOf(aroonDown, resultLength.value));
		}
		
		return result;
	}
}