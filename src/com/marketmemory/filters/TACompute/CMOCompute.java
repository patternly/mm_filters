package com.marketmemory.filters.TACompute;

import java.util.Arrays;

import com.patternly.datasource.BarDataWrapper;
import com.tictactec.ta.lib.RetCode;

/**
 * 
 * @author Yangqiao Meng
 *
 */
public class CMOCompute extends TAlib{
	
	public CMOCompute(BarDataWrapper barSeries) {
		super(barSeries);
	}
	
	@Override
	protected RetCode runTALib() throws Exception {

		double[] cmo = new double[barSeries.size()];
		
		RetCode result = c.cmo(0, barSeries.size() - 1, barSeries.getClose(), (Integer) inputs.get("period"),
				  indexBegin, resultLength,cmo);

		if (result == RetCode.Success) {
			outputs.put("cmo", Arrays.copyOf(cmo, resultLength.value));
		}

		return result;
	}
}
