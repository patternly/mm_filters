package com.marketmemory.filters.calendar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;

import com.marketmemory.filters.FilterIntersect;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.SimplePeriod;

public class CalendarCycle extends FilterIntersect {
	private DateTime date;
	private SimplePeriod singlePeriod;
	
	public CalendarCycle(DateTime date, SimplePeriod singlePeriod) {
		this.date = date;
		this.singlePeriod = singlePeriod;
	}
	
	private DateList buildFilterDateList(BarIndexSeries indexSeries) {
		DateTime end = indexSeries.getEnd();
		DateList result = new DateList();
		
		DateTime tmp = date;
		List<DateTime> list = new ArrayList<DateTime>();
		while(indexSeries.getStart().compareTo(tmp) <= 0) {
			if(end.compareTo(tmp) >= 0) {
				list.add(tmp);
			}
			tmp = tmp.minus(singlePeriod.toPeriod());
		}
		Collections.reverse(list);
		for(DateTime dt:list)
			result.add(dt);
		
		tmp = date.plus(singlePeriod.toPeriod());
		while(end.compareTo(tmp) >= 0) {
			result.add(tmp);
			tmp = tmp.plus(singlePeriod.toPeriod());
		}
		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarIndexSeries indexSeries) throws Exception {
		DateList dateList = buildFilterDateList(indexSeries);
		return new FilterResult(dateList, "CalendarCycle");
	}
}
