package com.marketmemory.filters.calendar;

import java.util.List;

import org.joda.time.DateTime;

import com.marketmemory.filters.FilterIntersect;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;

/**
 * The WeekOfMonth filter computes the week of the month for all the dates
 * between start and end, then selects the specified weeks for everymonth. The
 * week of month matchs options expiration calendar, so the third week is always
 * the week with options stop trading friday.
 * 
 * @author Michael
 * 
 */
public class WeekOfMonth extends FilterIntersect {

	private List<Integer> weekIndex;

	public WeekOfMonth(List<Integer> weekIndex) throws IllegalArgumentException {

		for (Integer i : weekIndex) {
			if (i > 5 || i < 1) {
				throw new IllegalArgumentException("Error in week of month: no such week index");
			}
		}
		this.weekIndex = weekIndex;
	}

	private DateList buildFilterDateList(BarIndexSeries indexSeries) throws Exception {
		DateList result = new DateList();
		DateTime currentDate = indexSeries.getStart();

		while (!currentDate.isAfter(indexSeries.getEnd())) {
			for (Integer type : weekIndex)
				if (computeWeekOfMonth(currentDate) == type.intValue()) {
					result.add(currentDate);
				}
			currentDate = currentDate.plusDays(1);
		}
		return result;
	}

	/**
	 * Compute the week of month based on options expiration calendar.
	 * 
	 * @param date
	 * @return
	 */
	private int computeWeekOfMonth(DateTime date) {
		int offset = date.withDayOfMonth(1).getDayOfWeek();

		if (offset == 6) {
			offset = -1;
		} else if (offset == 7) {
			offset = 0;
		}

		int wom = (date.getDayOfMonth() + offset) / 7;
		if ((date.getDayOfMonth() + offset) % 7 != 0) {
			wom++;
		}

		return wom;
	}

	@Override
	public FilterResult buildFilterResults(BarIndexSeries indexSeries) throws Exception {
		DateList dateList = buildFilterDateList(indexSeries);
		return new FilterResult(dateList, "WeekOfMonth");
	}
}
