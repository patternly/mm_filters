package com.marketmemory.filters.calendar;

import java.util.List;

import org.joda.time.DateTime;

import com.marketmemory.filters.FilterIntersect;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;

public class WindowDressing extends FilterIntersect {

	private List<Integer> monthList;
	private int end;
	
	public WindowDressing(List<Integer> monthList, int start, int end) {
		for (Integer i : monthList) {
			if (i > 12 || i < 1) {
				throw new IllegalArgumentException("Error in month of year: no such month");
			}
		}
		this.monthList = monthList;
		
		if(end < start) {
			throw new IllegalArgumentException("Starting day must be before or equal to ending day");
		}
		this.end = end;
	}

	private DateList buildFilterDateList(BarIndexSeries indexSeries) throws Exception {
		DateList result = new DateList();
		DateTime current = indexSeries.getStart();

		while (!current.isAfter(indexSeries.getEnd())) {
			if(monthList.contains(current.getMonthOfYear()) && (current.getDayOfMonth() == 1)) {
					result.add(current.plusDays(end));
			}
		current = current.plusDays(1);
		}
		
		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarIndexSeries indexSeries) throws Exception {
		DateList dateList = buildFilterDateList(indexSeries);
		return new FilterResult(dateList, "WindowDressing");
	}

}
