package com.marketmemory.filters.calendar;

import java.util.List;

import org.joda.time.DateTime;

import com.marketmemory.filters.FilterIntersect;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;

public class DayOfWeek extends FilterIntersect {
	private List<Integer> dayTypes;
	
	public DayOfWeek(List<Integer> dayType) throws IllegalArgumentException {
		for(Integer i: dayType) {
			if(i>7 || i <1) {
				throw new IllegalArgumentException("Error in day of week: no such day type");
			}
		}
		this.dayTypes = dayType;
	}

	private DateList buildFilterDateList(BarIndexSeries indexSeries) {
		DateList result = new DateList();
		DateTime tmp = indexSeries.getStart();
		while(!tmp.isAfter(indexSeries.getEnd())) {
			for(Integer type: dayTypes)
			if(tmp.getDayOfWeek() ==  type.intValue()) {
				result.add(tmp);
			}
			tmp = tmp.plusDays(1);
		}
		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarIndexSeries indexSeries) throws Exception {
		DateList dateList = buildFilterDateList(indexSeries);
		return new FilterResult(dateList, "DayOfWeek");
	}
}
