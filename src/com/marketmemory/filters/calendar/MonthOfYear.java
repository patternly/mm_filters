package com.marketmemory.filters.calendar;

import java.util.List;

import org.joda.time.DateTime;

import com.marketmemory.filters.FilterIntersect;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;

/**
 * The MonthOfYear filter gets the value of month of year for all the dates
 * between start and end, then selects the specified months for every year.
 * 
 * @author Michael
 * 
 */
public class MonthOfYear extends FilterIntersect {
	private List<Integer> monthType;

	public MonthOfYear(List<Integer> monthType) {

		for (Integer i : monthType) {
			if (i > 12 || i < 1) {
				throw new IllegalArgumentException("Error in month of year: no such month type");
			}
		}
		this.monthType = monthType;
	}

	private DateList buildFilterDateList(BarIndexSeries indexSeries) {
		DateList result = new DateList();
		DateTime moy = indexSeries.getStart();

		while (!moy.isAfter(indexSeries.getEnd())) {
			for (Integer type : monthType)
				if (moy.getMonthOfYear() == type.intValue()) {
					result.add(moy);
				}
			moy = moy.plusDays(1);
		}
		return result;
	}

	@Override
	public FilterResult buildFilterResults(BarIndexSeries indexSeries) throws Exception {
		DateList dateList = buildFilterDateList(indexSeries);
		return new FilterResult(dateList, "MonthOfYear");
	}

}
