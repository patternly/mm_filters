package test.marketmemory.filters.pattern;

import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.GapCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.pattern.Gaps;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.StockRecord;

public class TestGaps {
	@DataProvider
	public Object[][] testGapsDp() {
		StockRecord sr_D1 = TestConfig.stock_D("2011-10-10", 4, 2, 11.2, 5);
		StockRecord sr_D2 = TestConfig.stock_D("2011-10-11", 5.03, 2, 11.2, 5);
		StockRecord sr_D3 = TestConfig.stock_D("2011-10-21", 4.97, 2, 11.2, 5);
		StockRecord sr_D4 = TestConfig.stock_D("2011-10-22", 4.99, 2, 11.2, 5);
		return new Object[][] {
				new Object[] { new BarDataWrapper(sr_D1,sr_D2,sr_D3,sr_D4),
						new String[] { sr_D2.getDate() },
						new DateTime(2011,10,11,0,0), new DateTime(2012,10,11,0,0), 0.005, Gaps.emptyValue, 1, 3},
				new Object[] { new BarDataWrapper(sr_D1,sr_D2,sr_D3,sr_D4),
						new String[] { sr_D3.getDate() },
						new DateTime(2011,10,11,0,0), new DateTime(2012,10,11,0,0), Gaps.emptyValue, -0.005, 1, 3},
				new Object[] { new BarDataWrapper(sr_D1,sr_D2,sr_D3,sr_D4),
						new String[] { sr_D4.getDate() },
						new DateTime(2011,10,11,0,0), new DateTime(2012,10,11,0,0), 0.005, -0.005, 1, 3},						
		};
	}
	@Test(dataProvider = "testGapsDp")
	public void testGaps(BarDataWrapper barSeries, String[] expectedResult, DateTime start, DateTime end,
			double gapUpPerc, double gapDownPerc, int indexBegin, int resultLength) throws Exception {
		DateList expected = new DateList(expectedResult);
		
		TAlib ta = new GapCompute(barSeries);
		Gaps g = new Gaps(ta, gapUpPerc, gapDownPerc);
		
		FilterResult result2 = g.buildFilterResults(barSeries);
		
		Assert.assertEquals(result2.getIndexBegin(), indexBegin);
		Assert.assertEquals(result2.getResultLength(), resultLength);
		Assert.assertEquals(result2.getDateList(), expected);
		Assert.assertEquals(result2.getResults().get("gapResult").length, resultLength);
	}
}
