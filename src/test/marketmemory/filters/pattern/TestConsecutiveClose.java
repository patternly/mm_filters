package test.marketmemory.filters.pattern;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.pattern.ConsecutiveClose;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.MultiTimeFrame;
import com.patternly.util.PeriodType;
import com.patternly.util.SimplePeriod;
import com.patternly.util.StockRecord;
import com.patternly.util.TimeCoverage;

public class TestConsecutiveClose {
	@DataProvider
	public Object[][] testConsecutiveClosedp() {
		StockRecord sr0 = TestConfig.stock_D("2012-7-31", 12, 13, 11, 11.0);
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.5);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 14, 15.5, 10, 12.4);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 13.6, 15.5, 10, 12.5);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 12.6);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11.5, 15.5, 10, 13.3);
		StockRecord sr9 = TestConfig.stock_D("2012-9-3", 11, 15.5, 10, 14.4);
		StockRecord sr10 = TestConfig.stock_D("2012-9-4", 11, 15.5, 10, 13.5);
		StockRecord sr11 = TestConfig.stock_D("2012-9-5", 11, 15.5, 10, 12.6);
		StockRecord sr12 = TestConfig.stock_D("2012-9-6", 11, 15.5, 10, 11.7);
		StockRecord sr13 = TestConfig.stock_D("2012-9-11", 11, 15.5, 10, 10.7);
		StockRecord sr14 = TestConfig.stock_D("2012-9-12", 9.5, 15.5, 10, 9.7);
		StockRecord sr15 = TestConfig.stock_D("2012-10-4", 12.5, 15.5, 10, 11.7);
		StockRecord sr16 = TestConfig.stock_D("2012-10-5", 9.5, 15.5, 10, 8.7);
		StockRecord sr17 = TestConfig.stock_D("2012-10-8", 9.5, 15.5, 10, 8.3);
		
		return new Object[][] {
			new Object[] { // Test 0 day
				new BarDataWrapper(), "", new SimplePeriod(1, PeriodType.DAY), true, 1, ""},
			new Object[] { // Test select consecutive up for 1 day that over the percentage. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5), "2012-8-8#2012-8-14", new SimplePeriod(1, PeriodType.DAY), true,
				0, "2012-8-9 2012-8-10"},
			new Object[] { // Test select consecutive down for 1 day that over the percentage. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5), "2012-8-8#2012-8-15", new SimplePeriod(1, PeriodType.DAY), false,
				0.01, "2012-8-13 2012-8-14"},
			new Object[] { // Test select consecutive up for 2 day that over the percentage. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-8#2012-8-17",
				new SimplePeriod(2, PeriodType.DAY), true, 2, "2012-8-10 2012-8-17"},
			new Object[] { // Test select consecutive down for 2 day that over the percentage. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-8#2012-8-17",
				new SimplePeriod(2, PeriodType.DAY), false, 2, ""},
			new Object[] { // Test select consecutive up for 4 day that over the percentage.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14),
				"2012-8-8#2012-9-12", new SimplePeriod(4, PeriodType.DAY), true, 1, "2012-9-3"},
			new Object[] { // Test select consecutive down for 4 day that over the percentage.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14),
				"2012-8-8#2012-9-12", new SimplePeriod(4, PeriodType.DAY), false, 1, "2012-9-11 2012-9-12"},
			new Object[] { // Test select consecutive up for 1 week that over the percentage.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16),
				"2012-8-8#2012-10-5", new SimplePeriod(1, PeriodType.WEEK), true, 1, "2012-8-17"},
			new Object[] { // Test select consecutive down for 1 week that over the percentage. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16),
				"2012-8-8#2012-10-5", new SimplePeriod(1, PeriodType.WEEK), false, 1,
				"2012-9-6 2012-9-12 2012-10-05"},
			new Object[] { // Test select consecutive up for 2 week that over the percentage.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16,sr17),
				"2012-8-8#2012-10-8", new SimplePeriod(2, PeriodType.WEEK), true, 1, ""},
			new Object[] { // Test select consecutive down for 2 week that over the percentage.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16,sr17),
				"2012-8-8#2012-10-8", new SimplePeriod(2, PeriodType.WEEK), false, 1,
				"2012-9-12 2012-10-5 2012-10-8"},
			new Object[] { // Test select consecutive up for 1 month that over the percentage.
				new BarDataWrapper(sr0,sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16),
				"2012-7-31#2012-10-5", new SimplePeriod(1, PeriodType.MONTH), true, 1, "2012-8-17"},
			new Object[] { // Test select consecutive down for 1 month that over the percentage.
				new BarDataWrapper(sr0,sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16,sr17),
				"2012-7-31#2012-10-9", new SimplePeriod(1, PeriodType.MONTH), false, 1, "2012-9-12 2012-10-8"},
		};
	}
	@Test(dataProvider = "testConsecutiveClosedp")
	public void testConsecutiveClose(BarDataWrapper barSeries, String dates, SimplePeriod period, boolean direction,
			double percentage, String expectedDates) throws Exception {
		
		DateList expected = new DateList(expectedDates);
		
			BarIndexSeries dl = new BarIndexSeries(new DateList(dates), barSeries,
					new TimeCoverage(MultiTimeFrame.day, 1));
			ConsecutiveClose ConsecutiveClose = null;
			ConsecutiveClose = new ConsecutiveClose(period, direction, percentage);
			
			FilterResult result = ConsecutiveClose.buildFilterResults(dl);
			Assert.assertEquals(result.getDateList(), expected);
	}
	
	@Test
	public void testInvalidConsecutiveClose() throws Exception {
		ConsecutiveClose pfc;
		
		// Test time period less than 1.
		try {
			pfc = new ConsecutiveClose(new SimplePeriod(-1, PeriodType.DAY), true, 1);
			Assert.fail("The time period must be greater than or equal to 1");
		} catch(IllegalArgumentException e){}
		
		// Test time period less than 1.
		try {
			pfc = new ConsecutiveClose(new SimplePeriod(1, PeriodType.DAY), true, -1);
			Assert.fail("The percentage must be greater than or equal to 0");
		} catch(IllegalArgumentException e){}
	}
}
