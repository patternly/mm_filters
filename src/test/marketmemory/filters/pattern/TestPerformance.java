package test.marketmemory.filters.pattern;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.pattern.Performance;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.MultiTimeFrame;
import com.patternly.util.PeriodType;
import com.patternly.util.SimplePeriod;
import com.patternly.util.StockRecord;
import com.patternly.util.TimeCoverage;


public class TestPerformance {

	@DataProvider
	public Object[][] testPerformancedp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 10.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 10.9);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 11.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.8);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 14, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 13.6, 15.5, 10, 12.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 11.2);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11.5, 15.5, 10, 11.9);
		StockRecord sr9 = TestConfig.stock_D("2012-9-3", 11, 15.5, 10, 11.4);
		StockRecord sr10 = TestConfig.stock_D("2012-9-4", 11, 15.5, 10, 11.5);
		StockRecord sr11 = TestConfig.stock_D("2012-9-5", 11, 15.5, 10, 10.6);
		StockRecord sr12 = TestConfig.stock_D("2012-9-6", 11, 15.5, 10, 9.7);
		StockRecord sr13 = TestConfig.stock_D("2012-9-11", 11, 15.5, 10, 9.7);
		StockRecord sr14 = TestConfig.stock_D("2012-9-12", 9.5, 15.5, 10, 12.7);
		StockRecord sr15 = TestConfig.stock_D("2012-10-4", 12.5, 15.5, 10, 11.7);
		StockRecord sr16 = TestConfig.stock_D("2012-10-5", 9.5, 15.5, 10, 10.7);
		
		
		return new Object[][] {
			new Object[] { // Test 0 day
				new BarDataWrapper(), "", new SimplePeriod(1, PeriodType.DAY), 1, ""},
			new Object[] { // Test select positive changes that over the percentage. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5), "2012-8-8#2012-8-14", new SimplePeriod(1, PeriodType.DAY),
				0, "2012-8-9 2012-8-10 2012-8-13"},
			new Object[] { // Test select negative changes that bellow the percentage.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5), "2012-8-8#2012-8-15", new SimplePeriod(1, PeriodType.DAY),
				-0.000001, "2012-8-14"},
			new Object[] { // Test select positive changes that over the percentage with more dates.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-8#2012-8-17",
				new SimplePeriod(1, PeriodType.DAY), 2, "2012-8-9 2012-8-10 2012-8-13 2012-8-17"},
			new Object[] { // Test select negative changes that bellow the percentage with more dates.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-8#2012-8-17",
				new SimplePeriod(1, PeriodType.DAY), -2, "2012-8-14 2012-8-16"},
			new Object[] { // Test select positive changes that over the percentage with period of 2.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-8#2012-8-17",
				new SimplePeriod(2, PeriodType.DAY), 1, "2012-8-10 2012-8-13 2012-8-14"},
			new Object[] { // Test select negative changes that bellow the percentage with period of 2.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-8#2012-8-17",
				new SimplePeriod(2, PeriodType.DAY), -1, "2012-8-15 2012-8-16 2012-8-17"},
			new Object[] { // Test select positive changes that over the percentage for weekly performance.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-8#2012-9-6",
				new SimplePeriod(1, PeriodType.WEEK), 1, "2012-8-17"},
			new Object[] { // Test select negative changes that bellow the percentage for weekly performance.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-8#2012-9-6",
				new SimplePeriod(1, PeriodType.WEEK), -1, "2012-9-6"},
			new Object[] { // Test select positive changes that over the percentage with period of 2 weeks.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14), "2012-8-8#2012-9-12",
				new SimplePeriod(2, PeriodType.WEEK), 1, "2012-9-12"},
			new Object[] { // Test select negative changes that bellow the percentage with period of 2 weeks.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14), "2012-8-8#2012-9-12",
				new SimplePeriod(2, PeriodType.WEEK), -1, "2012-9-6"},
			new Object[] { // Test select positive changes that over the percentage for monthly performance.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16),
				"2012-8-8#2012-10-5", new SimplePeriod(1, PeriodType.MONTH), 1, "2012-9-12"},
			new Object[] { // Test select negative changes that bellow the percentage for monthly performance.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16),
				"2012-8-8#2012-10-5", new SimplePeriod(1, PeriodType.MONTH), -1, "2012-10-5"},
			new Object[] { // Test select positive changes that over the percentage with period of two months.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16),
				"2012-8-8#2012-10-5", new SimplePeriod(2, PeriodType.MONTH), 1, ""},
			new Object[] { // Test select negative changes that bellow the percentage with period of two months.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16),
				"2012-8-8#2012-10-5", new SimplePeriod(2, PeriodType.MONTH), -1, "2012-10-5"},
		};
	}
	@Test(dataProvider = "testPerformancedp")
	public void testPerformance(BarDataWrapper barSeries, String dates, SimplePeriod period, double percentage,
			String expectedDates) throws Exception {
		
		DateList expected = new DateList(expectedDates);
		
		BarIndexSeries dl = new BarIndexSeries(new DateList(dates), barSeries,
				new TimeCoverage(MultiTimeFrame.day, 1));
		Performance performance = null;
		performance = new Performance(period, percentage);
		
		FilterResult result = performance.buildFilterResults(dl);
		Assert.assertEquals(result.getDateList(), expected);
	}
	
	@DataProvider
	public Object[][] testPerformancewithRecoveragedp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 10.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 10.9);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 11.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.8);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 14, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 13.6, 15.5, 10, 12.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 11.2);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11.5, 15.5, 10, 11.9);
		StockRecord sr9 = TestConfig.stock_D("2012-9-3", 11, 15.5, 10, 11.4);
		StockRecord sr10 = TestConfig.stock_D("2012-9-4", 11, 15.5, 10, 11.5);
		StockRecord sr11 = TestConfig.stock_D("2012-9-5", 11, 15.5, 10, 10.6);
		StockRecord sr12 = TestConfig.stock_D("2012-9-6", 11, 15.5, 10, 9.7);
		
		
		return new Object[][] {
			new Object[] { // Test 0 day
				new BarDataWrapper(), "", new SimplePeriod(1, PeriodType.DAY), 1, -0.5, ""},
			new Object[] { // Test select positive changes that over the percentage. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5), "2012-8-8#2012-8-14", new SimplePeriod(1, PeriodType.DAY),
				0, 0.0, "2012-8-9 2012-8-10 2012-8-13"},
			new Object[] { // Test select positive changes that over the percentage with more dates.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-8#2012-8-17",
				new SimplePeriod(1, PeriodType.DAY), 2, -1, "2012-8-9 2012-8-17"},
			new Object[] { // Test select negative changes that bellow the percentage with more dates.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-8#2012-8-17",
				new SimplePeriod(1, PeriodType.DAY), -2, 1.5, "2012-8-14"},
			new Object[] { // Test select positive changes that over the percentage with period of 2.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-8#2012-8-17",
				new SimplePeriod(2, PeriodType.DAY), 1, -0.5, "2012-8-10"},
			new Object[] { // Test select negative changes that bellow the percentage with period of 2.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-8#2012-8-17",
				new SimplePeriod(2, PeriodType.DAY), -1, 0.5, "2012-8-15 2012-8-16"},
			new Object[] { // Test select positive changes that over the percentage for weekly performance.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-8#2012-9-6",
				new SimplePeriod(1, PeriodType.WEEK), 1, -0.5, "2012-8-17"},
		};
	}
	@Test(dataProvider = "testPerformancewithRecoveragedp")
	public void testPerformancewithRecoverage(BarDataWrapper barSeries, String dates, SimplePeriod period, double percentage,
			double recoveryPercentage, String expectedDates) throws Exception {
		
		DateList expected = new DateList(expectedDates);
		
		BarIndexSeries dl = new BarIndexSeries(new DateList(dates), barSeries,
				new TimeCoverage(MultiTimeFrame.day, 1));
		Performance performance = null;
		performance = new Performance(period, percentage, recoveryPercentage);
		
		FilterResult result = performance.buildFilterResults(dl);
		Assert.assertEquals(result.getDateList(), expected);
	}
	
	@Test
	public void testInvalidPerformance() throws Exception {
		Performance pfc;
		
		// Test time period less than 1.
		try {
			pfc = new Performance(new SimplePeriod(-1, PeriodType.DAY), 1);
			Assert.fail("The time period must be greater than or equal to 1");
		} catch(IllegalArgumentException e){}
	}
}
