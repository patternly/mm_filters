package test.marketmemory.filters.pattern;


import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.CandlestickPatternsCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.pattern.CandlestickPatterns;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.CandlestickPatternsType;
import com.patternly.util.StockRecord;

public class TestCandlestickPatternsFilter {
	
	@DataProvider
	public Object[][] testPatternsDojiAndMarubozudp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 13.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 9, 11);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 11);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 13.8);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 13.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 5, 5.0);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 5, 8.5);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 13.5, 11, 13.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 15.5, 5, 11.1);
		StockRecord sr13 = TestConfig.stock_D("2012-8-22", 12, 13, 11, 13.3);
		StockRecord sr14 = TestConfig.stock_D("2012-8-23", 11, 13.5, 9, 11.1);
		StockRecord sr15 = TestConfig.stock_D("2012-8-24", 11, 15.5, 7, 10.8);
		StockRecord sr16 = TestConfig.stock_D("2012-8-25", 16, 15.9, 11.1, 11);
		
		return new Object[][] {
			new Object[] { // Test 0 day
				new BarDataWrapper(), "", CandlestickPatternsType.doji, "both", "" },
			new Object[] { // Test not enough data to get result
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), 
					"2012-8-8#2012-8-21", CandlestickPatternsType.doji, "both", "" },	
			new Object[] { // Test with 1 Doji. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-8#2012-8-21", CandlestickPatternsType.doji, "both", "2012-8-21" },
			new Object[] { // Test with multiple Doji bearish. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16), 
				"2012-8-8#2012-8-29", CandlestickPatternsType.doji, "both", "2012-8-21 2012-8-23 2012-8-24" },
			new Object[] { // Test with 1 Marubozu. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-8#2012-8-21", CandlestickPatternsType.marubozu, "bullish", "2012-8-20" },
			new Object[] { // Test with multiple Marubozu.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16), 
				"2012-8-8#2012-8-29", CandlestickPatternsType.marubozu, "bullish", "2012-8-20" },
			new Object[] { // Test with multiple Marubozu.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16), 
				"2012-8-8#2012-8-29", CandlestickPatternsType.marubozu, "bearish", "2012-8-25" },
		};
	}
	@Test(dataProvider = "testPatternsDojiAndMarubozudp")
	public void testPatternsDojiAndMarubozu(BarDataWrapper barSeries, String dates, CandlestickPatternsType patternType,
			String patternfilterType, String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new CandlestickPatternsCompute(barSeries);
		CandlestickPatterns cdpattern = null;

		cdpattern = new CandlestickPatterns(ta, patternType, patternfilterType);
		
		FilterResult result = cdpattern.buildFilterResults(barSeries);
		Assert.assertEquals(result.getDateList(), expected);
	}
	
	@DataProvider
	public Object[][] testPatternsEngulfingdp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 13.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 9, 11);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 11);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 13.8);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 18, 19.5, 11, 13.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 17, 18.5, 10, 15.0);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 16, 17.5, 9, 14.5);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 14, 14.5, 8, 12.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 13, 13, 4, 12);
		StockRecord sr13 = TestConfig.stock_D("2012-8-22", 12, 14, 11, 13.5);
		StockRecord sr14 = TestConfig.stock_D("2012-8-23", 14, 14.5, 10.2, 11);
		StockRecord sr15 = TestConfig.stock_D("2012-8-24", 11, 12.5, 8.5, 9.3);
		StockRecord sr16 = TestConfig.stock_D("2012-8-25", 8, 13.9, 7, 12.8);
		
		return new Object[][] {
			new Object[] { // Test not enough data to get result
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), 
					"2012-8-8#2012-8-21", CandlestickPatternsType.engulfing, "both", "" },	
			new Object[] { // Test with 1 Engulfing. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14), 
				"2012-8-8#2012-8-25", CandlestickPatternsType.engulfing, "bearish", "2012-8-23" },
			new Object[] { // Test with multiple Engulfing. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16), 
				"2012-8-8#2012-8-25", CandlestickPatternsType.engulfing, "both", "2012-8-23 2012-8-25" },
			new Object[] { // Test with multiple Engulfing bearish. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16), 
				"2012-8-8#2012-8-25", CandlestickPatternsType.engulfing, "bullish", "2012-8-25" },
			new Object[] { // Test with multiple Engulfing bullish. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16), 
				"2012-8-8#2012-8-25", CandlestickPatternsType.engulfing, "bearish", "2012-8-23" },
		};
	}
	@Test(dataProvider = "testPatternsEngulfingdp")
	public void testPatternsEngulfing(BarDataWrapper barSeries, String dates, CandlestickPatternsType patternType,
			String patternfilterType, String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new CandlestickPatternsCompute(barSeries);
		CandlestickPatterns cdpattern = null;

		cdpattern = new CandlestickPatterns(ta, patternType, patternfilterType);
		
		FilterResult result = cdpattern.buildFilterResults(barSeries);
		Assert.assertEquals(result.getDateList(), expected);
	}
	
	@DataProvider
	public Object[][] testPatternsDarkCloudCoverdp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 13.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 9, 11);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 11);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 10, 11.5, 8, 11.8);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 12.5, 10, 13.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 12, 14.5, 11, 14.0);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 13, 14.5, 12, 15);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 14, 18.5, 13.5, 18);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 19, 19.5, 14.5, 15);
		StockRecord sr13 = TestConfig.stock_D("2012-8-22", 12, 14, 11, 13.5);
		StockRecord sr14 = TestConfig.stock_D("2012-8-23", 14, 14.5, 10.2, 11);
		StockRecord sr15 = TestConfig.stock_D("2012-8-24", 11, 15.5, 10.5, 15);
		StockRecord sr16 = TestConfig.stock_D("2012-8-25", 16.5, 16.9, 12, 12.2);
		
		return new Object[][] {
			new Object[] { // Test not enough data to get result
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), 
					"2012-8-8#2012-8-21", CandlestickPatternsType.darkcloudcover, "bearish", "" },	
			new Object[] { // Test with 1 darkcloudcover. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14), 
				"2012-8-8#2012-8-25", CandlestickPatternsType.darkcloudcover, "bearish", "2012-8-21" },
			new Object[] { // Test with multiple darkcloudcover. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12,sr13,sr14,sr15,sr16), 
				"2012-8-8#2012-8-25", CandlestickPatternsType.darkcloudcover, "bearish", "2012-8-21 2012-8-25" },
		};
	}
	@Test(dataProvider = "testPatternsDarkCloudCoverdp")
	public void testPatternsDarkCloudCover(BarDataWrapper barSeries, String dates, CandlestickPatternsType patternType,
			String patternfilterType, String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new CandlestickPatternsCompute(barSeries);
		CandlestickPatterns cdpattern = null;

		cdpattern = new CandlestickPatterns(ta, patternType, patternfilterType);
		
		FilterResult result = cdpattern.buildFilterResults(barSeries);
		Assert.assertEquals(result.getDateList(), expected);
	}
	
	@DataProvider
	public Object[][]testPatternsHaramidp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 81.71, 81.97, 81.58, 81.84);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 81.71, 81.97, 81.58, 81.84);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 81.71, 81.97, 81.58, 81.84);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 81.71, 81.97, 81.58, 81.84);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 81.81, 82.18, 81.4, 82.65);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 81.67, 82.41, 81.61, 82.35);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 82.34, 82.49, 81.82, 82.93);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 81.94, 82.08, 81.75, 81.85);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 81.85, 81.95, 81.51, 81.62);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 81.57, 81.81, 81.22, 81.26);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 81.27, 81.35, 80.86, 81.03);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 81.05, 81.17, 80.93, 81.12);
		StockRecord sr13 = TestConfig.stock_D("2012-8-22", 81.27, 81.35, 79.86, 80.03);
		StockRecord sr14 = TestConfig.stock_D("2012-8-23", 81.05, 81.17, 80.93, 81.12);

		
		return new Object[][] {
			new Object[] { // Test not enough data to get result
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), 
					"2012-8-8#2012-8-21", CandlestickPatternsType.harami, "bullish", "" },	
			new Object[] { // Test with 1 darkcloudcover. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-8#2012-8-25", CandlestickPatternsType.harami, "bullish", "" },
			new Object[] { // Test with 1 darkcloudcover. 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr13,sr14), 
				"2012-8-8#2012-8-25", CandlestickPatternsType.harami, "bullish", "2012-8-23" },
		};
	}
	@Test(dataProvider = "testPatternsHaramidp")
	public void testPatternsHarami(BarDataWrapper barSeries, String dates, CandlestickPatternsType patternType,
			String patternfilterType, String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
			TAlib ta = new CandlestickPatternsCompute(barSeries);
			CandlestickPatterns cdpattern = null;

			cdpattern = new CandlestickPatterns(ta, patternType, patternfilterType);
			
			FilterResult result = cdpattern.buildFilterResults(barSeries);
			Assert.assertEquals(result.getDateList(), expected);
	}
	
	
	@Test
	public void testInvalidCandlestickPatterns() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new CandlestickPatternsCompute(barSeries);
		CandlestickPatterns cdsp;
		
		// Test invalid pattern filter type.
		try {
			cdsp = new CandlestickPatterns(ta, CandlestickPatternsType.doji, "invalid");
			Assert.fail("No such candlestick pattern filter type: invalid");
		} catch(IllegalArgumentException e){}
		
	}
}
