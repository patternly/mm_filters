package test.marketmemory.filters.calendar;

import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.marketmemory.filters.calendar.MonthOfYear;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.MultiTimeFrame;
import com.patternly.util.TimeCoverage;

public class TestMonthOfYear {
	@DataProvider
	public Object[][] testMonthOfYearDp() {
		return new Object[][] {
				new Object[] { "2012-1-20", "2012-8-5", Arrays.asList(new Integer[] {8}), new String[] 
						{ "2012-8-1", "2012-8-2", "2012-8-3", "2012-8-4", "2012-8-5"} },
				new Object[] { "2012-1-20", "2012-8-5", Arrays.asList(new Integer[] {4, 5}), new String[] 
						{"2012-4-1#2012-5-31"} },
				new Object[] { "2012-1-20", "2012-8-5", Arrays.asList(new Integer[] {1, 2, 8}), new String[] 
						{"2012-1-20#2012-2-29", "2012-8-1#2012-8-5"} },		
		};
	}
	@Test(dataProvider = "testMonthOfYearDp")
	public void testMonthOfYear(String startDate, String endDate, List<Integer> monthOfYear, String[] expectedResult)
			throws Exception {
		DateTime start = DateList.convertToDate(startDate);
		DateTime end = DateList.convertToDate(endDate);
		MonthOfYear moy = new MonthOfYear(monthOfYear);
		BarIndexSeries dl = new BarIndexSeries(start, end, null, new TimeCoverage(MultiTimeFrame.day, 1));
		DateList expected = new DateList(expectedResult);
		
		FilterResult result = moy.buildFilterResults(dl);
		Assert.assertEquals(result.getDateList(), expected);
	}
}
