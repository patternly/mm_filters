package test.marketmemory.filters.calendar;

import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.marketmemory.filters.calendar.CalendarCycle;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.MultiTimeFrame;
import com.patternly.util.SimplePeriod;
import com.patternly.util.TimeCoverage;

public class TestCalendarCycle {
	@DataProvider
	public Object[][] testCalendarCycleDp() {
		return new Object[][] {
				new Object[] { "2012-1-9", "2012-5-15", "2012-1-16", SimplePeriod.days(10), new String[] 
						{ "2012-1-16", "2012-1-26","2012-2-5", "2012-2-15", "2012-2-25", "2012-3-6", "2012-3-16",
						"2012-3-26", "2012-4-5", "2012-4-15", "2012-4-25", "2012-5-5", "2012-5-15"} },
				
				new Object[] { "2012-1-9", "2012-5-15", "2012-1-9", SimplePeriod.weeks(3), new String[]
						{ "2012-1-9", "2012-1-30", "2012-2-20", "2012-3-12", 
						"2012-4-2", "2012-4-23", "2012-5-14" } },
						
				new Object[] { "2012-1-9", "2012-5-15", "2012-1-9", SimplePeriod.months(2), new String[]
						{ "2012-1-9", "2012-3-9", "2012-5-9"} },
						
				new Object[] { "2012-4-9", "2012-5-15", "2012-5-22", SimplePeriod.weeks(1), new String[]
						{"2012-4-10", "2012-4-17", "2012-4-24", "2012-5-1", "2012-5-8", "2012-5-15" } },
		};
	}
	@Test(dataProvider = "testCalendarCycleDp")
	public void testCalendarCycle(String startDate, String endDate, String selectedDate, SimplePeriod period,
			String[] expectedResult) throws Exception {

		DateTime start = DateList.convertToDate(startDate);
		DateTime end = DateList.convertToDate(endDate);
		DateTime date = DateList.convertToDate(selectedDate);
		BarIndexSeries actual = new BarIndexSeries(start, end, null, new TimeCoverage(MultiTimeFrame.day, 1));
		CalendarCycle filter = new CalendarCycle(date, period);
		DateList expected = new DateList(expectedResult);
		
		Assert.assertEquals(date, DateList.convertToDate(selectedDate));
		
		FilterResult result = filter.buildFilterResults(actual);
		Assert.assertEquals(result.getDateList(), expected);
	}
}
