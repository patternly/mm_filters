package test.marketmemory.filters.calendar;

import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.marketmemory.filters.calendar.WeekOfMonth;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.MultiTimeFrame;
import com.patternly.util.TimeCoverage;

public class TestWeekOfMonth {
	@DataProvider
	public Object[][] testWeekOfMonthDp() {
		return new Object[][] {
				// Test no input in the weekIndex.
				new Object[] { "2013-6-1", "2013-6-30", Arrays.asList(new Integer[] {}), new String[]{} },
				// Test normal case, select one week with first day of the month is Saturday.
				new Object[] { "2013-6-1", "2013-6-30", Arrays.asList(new Integer[] {2}), new String[] 
						{ "2013-6-9", "2013-6-10", "2013-6-11", "2013-6-12", "2013-6-13", "2013-6-14", "2013-6-15"} },
				// Select two weeks with first day of the month is Saturday.		
				new Object[] { "2013-6-1", "2013-6-30", Arrays.asList(new Integer[] {1, 2}), new String[] 
						{"2013-6-2", "2013-6-3", "2013-6-4", "2013-6-5", "2013-6-6", "2013-6-7", "2013-6-8",
					"2013-6-9", "2013-6-10", "2013-6-11", "2013-6-12", "2013-6-13", "2013-6-14", "2013-6-15"} },
				// Select first week for two months.	
				new Object[] { "2013-5-1", "2013-6-30", Arrays.asList(new Integer[] {1}), new String[] 
						{"2013-5-1", "2013-5-2", "2013-5-3", "2013-5-4", "2013-6-2", "2013-6-3", "2013-6-4",
					"2013-6-5", "2013-6-6", "2013-6-7", "2013-6-8",} },
				// Select first and last week.
				new Object[] { "2013-4-16", "2013-5-27", Arrays.asList(new Integer[] {1, 5}), new String[] 
							{"2013-4-28",  "2013-4-29",  "2013-4-30",  "2013-5-1", "2013-5-2", "2013-5-3", "2013-5-4",
						"2013-5-26", "2013-5-27",} },	
		};
	}
	@Test(dataProvider = "testWeekOfMonthDp")
	public void testWeekOfMonth(String startDate, String endDate, List<Integer> dayOfWeek, String[] expectedResult) throws Exception {
		DateTime start = DateList.convertToDate(startDate);
		DateTime end = DateList.convertToDate(endDate);
		
		WeekOfMonth wom = new WeekOfMonth(dayOfWeek);
		BarIndexSeries dl = new BarIndexSeries(start, end, null, new TimeCoverage(MultiTimeFrame.day, 1));
		DateList expected = new DateList(expectedResult);
		
		dl = new BarIndexSeries(start, end, null, new TimeCoverage(MultiTimeFrame.day, 1));
		FilterResult result = wom.buildFilterResults(dl);
		Assert.assertEquals(result.getDateList(), expected);
	}
}
