package test.marketmemory.filters.calendar;

import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.marketmemory.filters.calendar.WindowDressing;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.MultiTimeFrame;
import com.patternly.util.TimeCoverage;

public class TestWindowDressing {
	@DataProvider
	public Object[][] testWindowDressingDp() {
		return new Object[][] {
				new Object[] { "2012-1-20", "2012-8-5", Arrays.asList(new Integer[] {}), 0, 0, new String[]{} },
				new Object[] { "2012-1-20", "2012-8-5", Arrays.asList(new Integer[] {8}), 0, 0, new String[] 
						{ "2012-8-1"} },
				new Object[] { "2012-1-20", "2012-8-5", Arrays.asList(new Integer[] {4, 5}), 0, 0, new String[] 
						{"2012-4-1", "2012-5-1"} },
				new Object[] { "2012-1-20", "2012-8-5", Arrays.asList(new Integer[] {4, 5}), 0, 3, new String[] 
						{"2012-4-4", "2012-5-4"} },
				new Object[] { "2012-1-20", "2012-8-5", Arrays.asList(new Integer[] {4, 5}), -5, -3, new String[] 
						{"2012-3-29", "2012-4-28"} },
				new Object[] { "2011-4-20", "2012-8-5", Arrays.asList(new Integer[] {1, 5}), -5, 2, new String[] 
						{"2011-5-3", "2012-1-3", "2012-5-3"} },
				new Object[] { "2011-4-20", "2012-8-5", Arrays.asList(new Integer[] {1, 5}), -5, -2, new String[] 
						{"2011-4-29", "2011-12-30", "2012-4-29"} },	
		};
	}
	@Test(dataProvider = "testWindowDressingDp")
	public void testWindowDressing(String startDate, String endDate, List<Integer> monthList, int start, int end,
			String[] expectedResult) throws Exception {
		DateTime startDay = DateList.convertToDate(startDate);
		DateTime endDay = DateList.convertToDate(endDate);
		
		WindowDressing wdressing = new WindowDressing(monthList, start, end);
		BarIndexSeries dl = new BarIndexSeries(startDay, endDay, null, new TimeCoverage(MultiTimeFrame.day, 1));
		DateList expected = new DateList(expectedResult);
		
		dl = new BarIndexSeries(startDay, endDay, null, new TimeCoverage(MultiTimeFrame.day, 1));
		FilterResult result = wdressing.buildFilterResults(dl);
		Assert.assertEquals(result.getDateList(), expected);
	}
}
