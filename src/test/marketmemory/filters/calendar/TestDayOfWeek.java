package test.marketmemory.filters.calendar;

import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.marketmemory.filters.calendar.DayOfWeek;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.MultiTimeFrame;
import com.patternly.util.TimeCoverage;

public class TestDayOfWeek {
	@DataProvider
	public Object[][] testDayOfWeekDp() {
		return new Object[][] {
				new Object[] { "2012-7-21", "2012-7-21", Arrays.asList(new Integer[] {5}), new String[] 
						{} },
				new Object[] { "2012-7-20", "2012-8-5", Arrays.asList(new Integer[] {5}), new String[] 
						{ "2012-7-20", "2012-7-27", "2012-8-3"} },
				new Object[] { "2012-7-20", "2012-8-5", Arrays.asList(new Integer[] {5, 3}), new String[] 
						{ "2012-7-20", "2012-07-25", "2012-7-27", "2012-08-01", "2012-8-3"} },
						
		};
	}
	@Test(dataProvider = "testDayOfWeekDp")
	public void testDayOfWeek(String startDate, String endDate, List<Integer> dayOfWeek, String[] expectedResult) throws Exception {
		DateTime start = DateList.convertToDate(startDate);
		DateTime end = DateList.convertToDate(endDate);
		DayOfWeek dow = new DayOfWeek(dayOfWeek);
		BarIndexSeries dl = new BarIndexSeries(start, end, null, new TimeCoverage(MultiTimeFrame.day, 1));
		DateList expected = new DateList(expectedResult);
		
		dl = new BarIndexSeries(start, end, null, new TimeCoverage(MultiTimeFrame.day, 1));
		FilterResult result = dow.buildFilterResults(dl);
		Assert.assertEquals(result.getDateList(), expected);
	}
}
