package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.TACompute.ZScoreCompute;
import com.marketmemory.filters.technical.ZScore;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.StockRecord;

public class TestZScore {
	@DataProvider
	public Object[][] testZScoredp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 17.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 17.8);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 5.0);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 3.5);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 15.5, 10, 33.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 15.5, 10, 34.7);
		return new Object[][] {
			new Object[] { //Test 0 day
				new BarDataWrapper(), "", 20, 2.0, true, 0, 0, 1, 0, "" },
			new Object[] { //Test given dates are not enough to generate result.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 20, 2.0, true, 0, 0, 1, 0, "" },
			new Object[] { //Test above z-score.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-9#2012-8-21", 4, 1, true,
				3, 5, 1, 0, "2012-8-15 2012-8-16" },
			new Object[] { //Test below z-score.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-9#2012-8-21", 4, -1, false,
				3, 5, 1, 0, "2012-8-17" },
			new Object[] { //Test above limit with more data.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-9#2012-8-21",
				4, 1, true, 3, 9, 1, 0, "2012-8-15 2012-8-16 2012-8-20 2012-8-21" },
			new Object[] { //Test above limit with more data with different start and end.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-9#2012-8-21",
				4, 1, true, 3, 9, 1, 1, "2012-8-15 2012-8-20" },
			new Object[] { //Test below limit with more data.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-9#2012-8-21",
				4, -1, false, 3, 9, 1, 0, "2012-8-17 2012-8-18 2012-8-19" },
			new Object[] { //Test below limit with more data with different start and end.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-9#2012-8-21",
				4, -1, false, 3, 9, 2, 3, "2012-8-18 2012-8-19" },
			new Object[] { //Test above limit with more data with larger limit.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-9#2012-8-21",
				4, 1.5, true, 3, 9, 1, 0, "2012-8-15 2012-8-20" },
			new Object[] { //Test below limit with more data with larger limit..
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-9#2012-8-21",
				4, -1.5, false, 3, 9, 1, 0, "2012-8-18" },
		};
	}
	@Test(dataProvider = "testZScoredp")
	public void testZScore(BarDataWrapper barSeries, String dates, int period, double limit, boolean isAbove,
			int indexBegin, int resultLength, int start, int end, String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new ZScoreCompute(barSeries);
		ZScore zscore = null;

		if (end == 0) {
			zscore = new ZScore(ta, period, limit, isAbove, start);
		} else {
			zscore = new ZScore(ta, period, limit, isAbove, start, end);
		}
		
		FilterResult result = zscore.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("zScore").length, resultLength);
	}
	
	@Test
	public void testInvalidZScore() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new ZScoreCompute(barSeries);
		ZScore zscore;
		
		// Test time period less than 1.
		try {
			zscore = new ZScore(ta, 1, 1.0, true, 1);
			Assert.fail("Period must be larger than 1");
		} catch(IllegalArgumentException e){}
	}
	
}
