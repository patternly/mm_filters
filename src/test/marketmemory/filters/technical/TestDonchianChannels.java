package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.DonchianChannelsCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.DonchianChannels;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.StockRecord;

public class TestDonchianChannels {
	@DataProvider
	public Object[][] testDonchianChannelsdp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 12, 13.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 11.6, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 12.6, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 12.4, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 12.3, 11, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 17.4, 10, 17.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 17.8, 10, 17.8);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 11.3, 7, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 10, 6, 5.0);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 10, 5, 3.5);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 16.6, 5, 16.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 13, 7, 12.7);
		return new Object[][] {
			new Object[] { //Test 0 day
				new BarDataWrapper(), "", 20, true, 0, 0, 1, 0, "" },
			new Object[] { //Test given dates are not enough to generate result.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 20, true, 0, 0,  1, 0, "" },
			new Object[] { //Test new 3 days high.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-8#2012-8-21", 3, true, 2, 6, 
				1, 0, "2012-8-8 2012-8-15 2012-8-16" },
			new Object[] { //Test new 3 days high.
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-8#2012-8-21", 3, true, 2, 6, 
					1, 1, "2012-8-8 2012-8-15" },
			new Object[] { //Test new 3 days low.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), "2012-8-8#2012-8-21", 3, false, 2, 6, 
				1, 0, "2012-8-9 2012-8-17" },
			new Object[] { //Test new 5 days high.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-8#2012-8-21",
				5, true, 4, 8, 1, 0, "2012-8-8 2012-8-15 2012-8-16" },
			new Object[] { //Test new 5 days low.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-8#2012-8-21",
				5, false, 4, 8, 1, 0, "2012-8-9 2012-8-17 2012-8-18 2012-8-19" },
			new Object[] { //Test new 5 days low.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-8#2012-8-21",
				5, false, 4, 8, 2, 2, "2012-8-18" },
			new Object[] { //Test new 5 days low.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-8#2012-8-21",
				5, false, 4, 8, 1, 2, "2012-8-9 2012-8-17 2012-8-18" },

		};
	}
	@Test(dataProvider = "testDonchianChannelsdp")
	public void testDonchianChannels(BarDataWrapper barSeries, String dates, int period, boolean isNewHigh, int indexBegin,
			int resultLength, int start, int end, String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		TAlib ta = new DonchianChannelsCompute(barSeries);
		DonchianChannels dchannels = null;

		if (end == 0) {
			dchannels = new DonchianChannels(ta, period, isNewHigh, start);
		} else {
			dchannels = new DonchianChannels(ta, period, isNewHigh, start, end);
		}
		
		FilterResult result = dchannels.buildFilterResults(barSeries);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getResults().get("minValue").length, resultLength);
		Assert.assertEquals(result.getResults().get("maxValue").length, resultLength);
	}
	
	@Test
	public void testInvalidDonchianChannels() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new DonchianChannelsCompute(barSeries);
		DonchianChannels dchannels;
		
		// Test time period less than 1.
		try {
			dchannels = new DonchianChannels(ta, 1, true, 1);
			Assert.fail("Period must be larger than 1");
		} catch(IllegalArgumentException e){}
	}
}
