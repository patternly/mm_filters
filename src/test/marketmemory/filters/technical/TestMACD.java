package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.MACDCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.MACD;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

public class TestMACD {

	@DataProvider
	public Object[][] testMACDdp() {
		
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 12.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 13.5);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 15.5, 10, 13.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 15.5, 10, 14.7);
		return new Object[][] {
			new Object[] { //Test 0 day
				new BarDataWrapper(), "", 12, 26, 9, false, Position.above, 1, 0, 0, 0, "" },
			new Object[] { //Test given dates are not enough to generate result.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 12, 26, 9, false, Position.above, 1, 0, 0, 0, "" },
				
				// MACD results for sr1 to sr12 for fastPeriod = 2, slowPeriod = 3, signalPeriod = 2.
				// [0.18333333333333357, 0.09166666666666679, 0.22916666666666607, -0.040972222222222854, -0.20567129629629655,
				// -0.21456404320987588, 0.2721418467078198, 0.2792122127914958, 0.3706532028749425]
				
				// MACD signal results for sr1 to sr12 for fastPeriod = 2, slowPeriod = 3, signalPeriod = 2.
				// [0.22499999999999964, 0.13611111111111107, 0.19814814814814774, 0.03873456790123403, -0.12420267489711967,
				// -0.1844435871056238, 0.11994670210333858, 0.2261237092287767, 0.3224767049928873]
				
			new Object[] { //Test center line crossovers, abouve zero, and start at 1st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), 
				"2012-8-9#2012-8-21", 2, 3, 2, false, Position.above, 1, 0, 3, 5, "2012-8-13 2012-8-14 2012-8-15" },
			new Object[] { //Test center line crossovers, below zero, and start at 1st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), 
				"2012-8-9#2012-8-21", 2, 3, 2, false, Position.below, 1, 0, 3, 5, "2012-8-16 2012-8-17" },
			new Object[] { //Test center line crossovers, abouve zero, and start at 2st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 2, 3, 2, false, Position.above, 2, 0, 3, 9, "2012-8-14 2012-8-15 2012-8-20 2012-8-21" },
			new Object[] { //Test center line crossovers, below zero, and start at 2st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 2, 3, 2, false, Position.below, 2, 0, 3, 9, "2012-8-17 2012-8-18" },
			new Object[] { //Test center line crossovers, abouve zero, starts 1, ends 2
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 2, 3, 2, false, Position.above, 2, 2, 3, 9, "2012-8-14 2012-8-20" },
			new Object[] { //Test center line crossovers, below zero, starts 1, ends 2
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 2, 3, 2, false, Position.below, 1, 2, 3, 9, "2012-8-16 2012-8-17" },
			new Object[] { //Test signal line crossovers, above zero, and start at 1st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 2, 3, 2, true, Position.above, 1, 0, 3, 9, "2012-8-15 2012-8-19 2012-8-20 2012-8-21" },
			new Object[] { //Test signal line crossovers, below zero, and start at 1st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), "2012-8-9#2012-8-21",
				2, 3, 2, true, Position.below, 1, 0, 3, 9, "2012-8-13 2012-8-14 2012-8-16 2012-8-17 2012-8-18" },
		};
	}
	@Test(dataProvider = "testMACDdp")
	public void testMACD(BarDataWrapper barSeries, String dates, int fastPeriod, int slowPeriod, int signalPeriod,
			boolean isSignalLine, Position position, int start, int end, int indexBegin, int resultLength,
			String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new MACDCompute(barSeries);
		MACD macd = null;

		if (end == 0) {
			macd = new MACD(ta, fastPeriod, slowPeriod, signalPeriod, isSignalLine, position, start);
		} else {
			macd = new MACD(ta, fastPeriod, slowPeriod, signalPeriod, isSignalLine, position, start, end);
		}
		
		FilterResult result = macd.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("macdResult").length, resultLength);
	}
	
	@Test
	public void testInvalidMACD() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new MACDCompute(barSeries);
		MACD macd;
		
		// Test time period less than 1.
		try {
			macd = new MACD(ta, 1 , 26, 9, true, Position.above, 1);
			Assert.fail("All periods must be larger than 1");
		} catch(IllegalArgumentException e){}
		
		// start must be larger than 0.
		try {
			macd = new MACD(ta, 1 , 26, 9, true, Position.above, 0);
			Assert.fail("start must be larger than 0");
		} catch(Exception e){}
	}
}
