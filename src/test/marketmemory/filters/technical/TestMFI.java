package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.MFICompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.MFI;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

/**
 * 
 * @author Yangqiao Meng
 *
 */
public class TestMFI {

	@DataProvider
	public Object[][] testMFIdp() {
		
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3,1000);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.1,1000);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6,1000);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4,1000);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3,1000);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4,1000);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 12.1,1000);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3,1000);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0,1000);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 13.5,1000);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 15.5, 10, 13.6,1000);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 15.5, 10, 14.7,1000);
		return new Object[][] {
			new Object[] { //Test 0 day
				new BarDataWrapper(), "", 4, 80, 20, Position.above, 1, 0, 0, 0, "" },
			new Object[] { //Test given dates are not enough to generate result.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 
				"2012-8-9#2012-8-21", 15, 80, 20,  Position.above, 1, 0, 0, 0, "" },

			//	for look-back period=4, data from sr1-sr12, the result is: index begin=4,result length=8			
//				 25.67385444743936,  8-14
//				 50.4256712508186,  8-15
//				 25.55847568988174,  8-16
//				 25.744540039708802, 8-17
//				 25.96795727636849, 8-18
//				 26.017344896597734,8-19
//				 51.58520475561428, 8-20
//				 76.42118863049096, 8-21
				
			new Object[] { //Test over-bought,>70, and start at 1st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 4, 70, 30,  Position.above, 1, 0, 4, 8, "2012-8-21" },
			new Object[] { //Test cover-sold,<30, and start at 1st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 4, 70, 30, Position.below, 1, 0, 4, 8, "2012-8-14 2012-8-16 2012-8-17 2012-8-18 2012-8-19" },
			new Object[] { //Test over-bought,>70,, and start at 2nd date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 4, 70, 30, Position.above, 2, 0, 4, 8, "" },
			new Object[] {  //Test cover-sold,<28, and start at 2nd date. end at 4th date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 4, 70, 28, Position.below, 2, 4, 4, 8, "2012-8-17 2012-8-18 2012-8-19" },
			
			//for look-back period=5, data from sr1-sr9, result is: index begin=5, result length =4
//				 41.1105178857448,   8-15
//				 40.46242774566475,  8-16
//				 20.582010582010586, 8-17
//				 20.735607675906184, 8-18
				 	
			new Object[] { //Test over-bought,>35, and start at 2nd date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 
				"2012-8-9#2012-8-21", 5, 35, 20, Position.above, 2, 0, 5, 4, "2012-8-16" },
			new Object[] { //Test cover-sold,<28, and starts 1, ends 2
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 
				"2012-8-9#2012-8-21", 5, 80, 20,  Position.below, 1, 2, 5, 4, "" },
			new Object[] { //Test over-bought,>80, and start at 1st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 
				"2012-8-9#2012-8-21", 5, 80, 20,  Position.above, 1, 0, 5, 4, "" },
			new Object[] { //Test over-sold,<25,  and start at 2nd date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
				"2012-8-9#2012-8-21",5, 75, 25,  Position.below, 2, 0, 5, 4, "2012-8-18" },
		};
	}
	@Test(dataProvider = "testMFIdp")
	public void testMFI(BarDataWrapper barSeries, String dates, int period, int overboughtLine, int oversoldLine,
			 Position position, int start, int end, int indexBegin, int resultLength,
			String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new MFICompute(barSeries);
		MFI mfi = null;

		if (end == 0) {
			mfi = new MFI(ta, period, overboughtLine,oversoldLine, position, start);
		} else {
			mfi = new MFI(ta, period, overboughtLine,oversoldLine, position, start, end);
		}
		
		FilterResult result = mfi.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("MFIResult").length, resultLength);
	}
	
	@Test
	public void testInvalidMFI() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3,1000));
		TAlib ta = new MFICompute(barSeries);
		MFI mfi;
		
		// Test time period less than 1.
		try {
			mfi = new MFI(ta, 1 , 80, 20,Position.above, 1);
			Assert.fail("Period must be larger than 1");
		} catch(IllegalArgumentException e){}
		
		// Test over-bought limit larger than 100
		try {
			mfi = new MFI(ta, 1 , 101, 20,Position.above, 1);
			Assert.fail("Over-bought line must be less than 100");
		} catch(IllegalArgumentException e){}
		
		// test over-sold limit smaller than 0
		try {
			mfi = new MFI(ta, 1 , 80, 0,Position.above, 1);
			Assert.fail("Over-sold line must be larger than 0");
		} catch(IllegalArgumentException e){}
		
		// test over-bought limit higher than over-sold limit
		try {
			mfi = new MFI(ta, 1 , 80, 90,Position.above, 1);
			Assert.fail("Over-bought line must be larger than over-sold line");
		} catch(IllegalArgumentException e){}
		
		// start must be larger than 0.
		try {
			mfi = new MFI(ta, 1 , 80, 20, Position.above, 0);
			Assert.fail("start must be larger than 0");
		} catch(Exception e){}
	}
}
