package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.DEMACompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.DEMA;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

public class TestDEMA {
	@Test
	public void testInvalidDEMA() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D(
				"2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new DEMACompute(barSeries);
		DEMA dema;

		// timePeriod smaller than 2
		try {
			dema = new DEMA(ta, 1, Position.above, 1);
			Assert.fail("period must be greater or  equal to 2");
		} catch (IllegalArgumentException e) {
		}

		// start smaller than 1
		try {
			dema = new DEMA(ta, 2, Position.above, 0);
			Assert.fail("start must be greater than 0");
		} catch (IllegalArgumentException e) {
		}

		// end smaller than start
		try {
			dema = new DEMA(ta, 1, Position.above, 2, 1);
			Assert.fail("end must be greater than start");
		} catch (IllegalArgumentException e) {
		}
	}

	@DataProvider
	public Object[][] testDEMAdp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 17, 9, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 8, 11.6); // new low
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 7, 13.4); // new low
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 18, 10, 12.1); // new high
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0);


		return new Object[][] { 
				
				//timePeriod = 2;
				//DEMA = [12.600000000000001, 12.507407407407408, 12.360493827160495, 13.295061728395062, 
				//		12.289986282578875, 11.3827617741198, 10.978509373571102]
				//Close = [12.6, 12.4, 12.3, 13.4, 12.1, 11.3, 11.0]
				new Object[]{
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9),
						2, Position.above, 1, 0, 2, 7, "2012-8-15 2012-8-18" }, 
				new Object[]{
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9),
						2, Position.below, 1, 0, 2, 7, "2012-8-10 2012-8-13 2012-8-14 2012-8-16 2012-8-17" }, 
				new Object[]{//start = 2;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9),
						2, Position.below, 2, 0, 2, 7, "2012-8-13 2012-8-14 2012-8-17" },
				new Object[]{//end = 2;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9),
						2, Position.below, 1, 2, 2, 7, "2012-8-10 2012-8-13 2012-8-16 2012-8-17" },
				new Object[]{//start = 2; end = 3;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9),
						2, Position.below, 2, 3, 2, 7, "2012-8-13 2012-8-14 2012-8-17" },
				
				//timePeriod = 3;
				//DEMA = [12.363888888888889, 13.179861111111112, 12.46388888888889, 11.593923611111112, 11.077951388888888]
				//Close = [ 12.3, 13.4, 12.1, 11.3, 11.0]
				new Object[]{
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9),
						3, Position.above, 1, 0, 4, 5, "2012-8-15" }, 
				new Object[]{
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9),
						3, Position.below, 1, 0, 4, 5, "2012-8-14 2012-8-16 2012-8-17 2012-8-18" },
				new Object[]{ //start = 2;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9),
						3, Position.below, 2, 0, 4, 5, "2012-8-17 2012-8-18" },
				new Object[]{ //end = 2;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9),
						3, Position.below, 1, 2, 4, 5, "2012-8-14 2012-8-16 2012-8-17" },
				
		};
	}

	@Test(dataProvider = "testDEMAdp")
	public void testDEMA(BarDataWrapper barSeries, int timePeriod, Position position,
			int start, int end, int indexBegin, int resultLength, String expectedDates) throws Exception{
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new DEMACompute(barSeries);
		DEMA dema = null;
		
		if(end == 0){
			dema = new DEMA(ta, timePeriod, position, start);
		} else {
			dema = new DEMA(ta, timePeriod, position, start, end);
		}
		
		FilterResult result = dema.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("DEMA").length, resultLength);
	}
}