package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.TACompute.WilliamRCompute;
import com.marketmemory.filters.technical.WilliamR;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.StockRecord;

public class TestWilliamR {

	@DataProvider
	public Object[][] testWilliamRdp() {

		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 12.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0);

		
		
		return new Object[][] { 
				
				//timePeriod = 2
				//williamR = [-54.28571428571429, -52.727272727272734, -56.36363636363636, -58.181818181818166, 
				//			-38.18181818181817, -61.81818181818183, -76.36363636363635, -81.81818181818181]
				new Object[] { 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						2, -50, 0, 1, 0, 1, 8, "2012-8-15" }, 
				new Object[] { 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						2, -100, -50, 1, 0, 1, 8, "2012-8-9 2012-8-10 2012-8-13 2012-8-14 2012-8-16 2012-8-17 2012-8-18"},
				new Object[] { //boundray value: -100; position = below
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5),
						2, -100, -100, 1, 0, 1, 4, "" },
				new Object[] { //boundray value: -100; position = above
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5),
						2, -100, 0, 1, 0, 1, 4, "2012-8-9 2012-8-10 2012-8-13 2012-8-14" },
				new Object[] { //boundray value: 0; position = above
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5),
						2, 0, 0, 1, 0, 1, 4, "" },
				new Object[] { //boundray value: 0; position = below
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5),
						2, -100, 0, 1, 0, 1, 4, "2012-8-9 2012-8-10 2012-8-13 2012-8-14" },
		
				//timePeriod = 5
				//williamR = [-58.181818181818166, -38.18181818181817, -61.81818181818183, -76.36363636363635, -81.81818181818181]		
				new Object[] { 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						5, -50, 0, 1, 0, 4, 5, "2012-8-15" },
				new Object[] { 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						5, -100, -80, 1, 0, 4, 5, "2012-8-18" },
				new Object[] { //test start = 2
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						5, -100, -50, 2, 0, 4, 5, "2012-8-17 2012-8-18" },
				new Object[] { //test start = 1; end = 2
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						5, -100, -50, 1, 2, 4, 5, "2012-8-14 2012-8-16 2012-8-17" },
		};
	}

	@Test(dataProvider = "testWilliamRdp")
	public void testWilliamR(BarDataWrapper barSeries, int timePeriod, double lowLimit, 
			double highLimit, int start, int end, int indexBegin, int resultLength, String expectedDates)
			throws Exception {
		DateList expected = new DateList(expectedDates);

		TAlib ta = new WilliamRCompute
				
				(barSeries);
		WilliamR williamR = null;

		if (end == 0) {
			williamR = new WilliamR(ta, timePeriod, lowLimit, highLimit, start);
		} else {
			williamR = new WilliamR(ta, timePeriod, lowLimit, highLimit, start, end);
		}

		FilterResult result = williamR.buildFilterResults(barSeries);

		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("WilliamRResult").length, resultLength);
	}

	@SuppressWarnings("unused")
	@Test
	public void testInvalidWilliamR() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new WilliamRCompute(barSeries);
		WilliamR williamR;

		// start smaller than 0.
		try {
			williamR = new WilliamR(ta, 2, -50, 0, 0);
			Assert.fail("start must be greater than 0");
		} catch (IllegalArgumentException e) {
		}

		// end smaller than start.
		try {
			williamR = new WilliamR(ta, 2, -50, 0, 2, 1);
			Assert.fail("start must be smaller than end");
		} catch (IllegalArgumentException e) {
		}

		// timePeriod smaller than 2.
		try {
			williamR = new WilliamR(ta, 1, -50, 0, 1);
			Assert.fail("Time period must be greater than 1");
		} catch (IllegalArgumentException e) {
		}

		// low limit larger than high limit.
		try {
			williamR = new WilliamR(ta, 2, -50, -80, 1);
			Assert.fail("Low limit must be smaller than high limit");
		} catch (IllegalArgumentException e) {
		}
		
		// low limit not in valid range.
		try {
			williamR = new WilliamR(ta, 2, -101, -80, 1);
			Assert.fail("Low limit must be between -100 and 0");
		} catch (IllegalArgumentException e) {
		}
		
		// high limit not in valid range.
		try {
			williamR = new WilliamR(ta, 2, -100, 1, 1);
			Assert.fail("High limit must be between -100 and 0");
		} catch (IllegalArgumentException e) {
		}
	}
}