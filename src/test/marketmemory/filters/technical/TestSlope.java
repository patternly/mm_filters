package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.SlopeCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.Slope;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

public class TestSlope {

	@DataProvider
	public Object[][] testSlopedp(){
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 10.5);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 10.1);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 10);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 11);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 12.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 10.2);

		
		return new Object[][]{
				
				//timePeriod = 2
				//Slope = [-0.3000000000000007, -0.5, -0.3999999999999986, -0.09999999999999787, 1.0, 1.1000000000000014, -0.8000000000000007]
				new Object[]{ //test position = above; 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), 2, Position.above, 0,
						1, 0, 1, 7, "2012-8-15 2012-8-16"},
				new Object[]{ //test position = below; 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), 2, Position.below, 0,
						1, 0, 1, 7, "2012-8-9 2012-8-10 2012-8-13 2012-8-14 2012-8-17"},
				new Object[]{ //test start = 2; 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), 2, Position.below, 0,
						2, 0, 1, 7, "2012-8-10 2012-8-13 2012-8-14"},
				new Object[]{ //test end = 2; 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), 2, Position.below, 0,
						1, 2, 1, 7, "2012-8-9 2012-8-10 2012-8-17"},
				new Object[]{ //test start = 2; end = 3; 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), 2, Position.below, 0,
						2, 3, 1, 7, "2012-8-10 2012-8-13"},
						
				//timePeriod = 3;
				//Slope = [-0.4000000000000033, -0.4499999999999981, -0.25, 0.45000000000000284, 1.0500000000000018,
				//			0.15000000000000094, -0.9500000000000028]
				new Object[]{ //test position = below; 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 3, Position.below, 0,
						1, 0, 2, 7, "2012-8-10 2012-8-13 2012-8-14 2012-8-18"},
				new Object[]{ //test position = above; 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 3, Position.above, 0,
						1, 0, 2, 7, "2012-8-15 2012-8-16 2012-8-17"},						
				new Object[]{ //test start = 2; 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 3, Position.below, 0,
						2, 0, 2, 7, "2012-8-13 2012-8-14"},
				new Object[]{ //test end = 2; 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 3, Position.below, 0,
						1, 2, 2, 7, "2012-8-10 2012-8-13 2012-8-18"},
		};
	}
	
	@Test(dataProvider = "testSlopedp")
		public void testSlope(BarDataWrapper barSeries, int timePeriod, 
				Position position, double signalLine, int start, int end, int indexBegin, int resultLength, String expectedDates)
				throws Exception {
			DateList expected = new DateList(expectedDates);

			TAlib ta = new SlopeCompute(barSeries);
			Slope slope = null;

			if (end == 0) {
				slope = new Slope(ta, timePeriod, position, signalLine, start);
			} else {
				slope = new Slope(ta, timePeriod, position, signalLine, start, end);
			}

			FilterResult result = slope.buildFilterResults(barSeries);

			Assert.assertEquals(result.getIndexBegin(), indexBegin);
			Assert.assertEquals(result.getResultLength(), resultLength);
			Assert.assertEquals(result.getDateList(), expected);
			Assert.assertEquals(result.getResults().get("slopeResult").length, resultLength);
		}
		
	@SuppressWarnings("unused")
	@Test
	public void testInvalidSlope() throws Exception{
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new SlopeCompute(barSeries);
		Slope slope;
		
		//start smaller than 1;
		try{
			slope = new Slope(ta, 2, Position.above, 0, 0);
			Assert.fail("start must be greater or equal to 1");
		}catch(IllegalArgumentException e){
		}
		
		//timePeriod smaller than 2
		try{
			slope = new Slope(ta, 1, Position.above, 0, 1);
			Assert.fail("time period must be greater than 1");
		}catch(IllegalArgumentException e){
		}
		
		//end smaller than start
		try{
			slope = new Slope(ta, 1, Position.above, 0, 3, 1);
			Assert.fail("end must be greater than start");
		}catch(IllegalArgumentException e){
		}
	}		
	
}