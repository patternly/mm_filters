package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.RSICompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.RSI;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.StockRecord;

public class TestRSI {

	@DataProvider
	public Object[][] testRSIp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 12.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.4);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 11.5);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 15.5, 10, 10.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 15.5, 10, 9.7);
		return new Object[][] {
			new Object[] { //Test 0 day
				new BarDataWrapper(), "", 2, 1, 1, 0, 0, 1, 0, "" },
				
				//For the given stock records, the output for RSI with look-back period = 3 should be:
				// 86.6666666666667
				// 78.78787878787882
				// 91.51515151515154
				// 44.34654919236415
				// 30.049751243781092
				// 34.0370631011025
				// 39.23284710967042
				// 19.013955436860133
				// 10.723963524938148
				//
				//For high and low limit to be 30, and 70. the following corresponding dates are expected to be selected:
				// 2012-8-16 2012-8-17 2012-8-18 2012-8-19.
				new Object[] { 
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", 3, 70, 30, 3, 9, 1, 0, "2012-8-16 2012-8-17 2012-8-18 2012-8-19" },
				// With end value.
				new Object[] { 
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", 3, 70, 30, 3, 9, 1, 2, "2012-8-16 2012-8-17" },
				
				//For high and low limit to be 70, and 100. the following corresponding dates are expected to be selected:
				// 2012-8-13 2012-8-14 2012-8-15.
				new Object[] { 
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", 3, 100, 70, 3, 9, 1, 0, "2012-8-13 2012-8-14 2012-8-15" },
				//With different start and end.	
				new Object[] { 
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", 3, 100, 70, 3, 9, 3, 5, "2012-8-15" },
					
				//For high and low limit to be 0, and 30. the following corresponding dates are expected to be selected:
				// 2012-8-20 2012-8-21.
				new Object[] { 
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", 3, 30, 0, 3, 9, 1, 0, "2012-8-20 2012-8-21" },
				//With different start and end.	
				new Object[] { 
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", 3, 30, 0, 3, 9, 3, 5, "" },
					
				//For the given stock records, the output for RSI with look-back period = 5 should be:
				// 88.88888888888891
				// 55.49132947976878
				// 43.049327354260086
				// 44.97698348226373
				// 47.21049555108137
				// 32.41001861491644
				// 23.285156273462025
				//
				//For high and low limit to be 30, and 70. the following corresponding dates are expected to be selected:
				// 2012-8-16 2012-8-17 2012-8-18 2012-8-19 2012-8-20.			
				new Object[] { 
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", 5, 70, 30, 5, 7, 1, 0, "2012-8-16 2012-8-17 2012-8-18 2012-8-19 2012-8-20" },
				//With different start and end.
				new Object[] { 
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", 5, 70, 30, 5, 7, 2, 4, "2012-8-17 2012-8-18 2012-8-19" },
		};
	}
	@Test(dataProvider = "testRSIp")
	public void testRSI(BarDataWrapper barSeries, String dates, int lookBackPeriod, int highLimit, int lowLimit, int indexBegin,
			int resultLength, int start, int end, String expectedDates) throws Exception {
		
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new RSICompute(barSeries);
		RSI rsi = null;
		
		if (end == 0) {
			rsi = new RSI(ta, lookBackPeriod, highLimit, lowLimit, start);
		} else {
			rsi = new RSI(ta, lookBackPeriod, highLimit, lowLimit, start, end);
		}
		
		FilterResult result = rsi.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("rsiResult").length, resultLength);
	}
	
	@Test
	public void testInvalidRSIData() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new RSICompute(barSeries);
		RSI rsi;
		
		// Test lookBackPeriod less than 1.
		try {
			rsi = new RSI(ta, 0, 70, 30, 1, 1);
			Assert.fail("look-back period less than 1 fail");
		} catch(IllegalArgumentException e){}
		
		// Test low limit less than 1.
		try {
			rsi = new RSI(ta, 14, -1, 30, 1, 1);
			Assert.fail("low limit less than 1 fail");
		} catch(IllegalArgumentException e){}
		
		// Test high limit greater than 100.
		try {
			rsi = new RSI(ta, 14, 30, 101, 1, 1);
			Assert.fail("high limit greater than 100 fail");
		} catch(IllegalArgumentException e){}
		
		// Test if low limit is smaller than high limit.
		try {
			rsi = new RSI(ta, 14, 30, 70, 1, 1);
			Assert.fail("low limit greater than high limit fail");
		} catch(IllegalArgumentException e){}
			
		// Test if low limit is smaller than high limit.
		try {
			rsi = new RSI(ta, 14, 100, 70, 3, 1);
			Assert.fail("end should not be smaller than start");
		} catch(IllegalArgumentException e){}
	}
}
