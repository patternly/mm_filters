package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.TMACompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.TMA;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

/**
 * Testing TMA filter
 * 
 * @author Yang Meng
 *
 */
public class TestTMA {

	@DataProvider
	public Object[][] testTMAdp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 13.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 13.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 13.4);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 13.5);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 15.5, 10, 13.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 15.5, 10, 13.7);

		return new Object[][] {

				new Object[] { // Test 0 day
				new BarDataWrapper(), "", 5, Position.above, 1, 0, 0, 0, "" },
				new Object[] { // Test given dates are not enough to generate
						// result.
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8), "2012-8-9#2012-8-21", 15,
						Position.above, 1, 0, 0, 0, "" },

				// result for period=5, sr1 - sr12:
				// index begin =4, result length = 8
				// 12.155555555555555, 8-14
				// 12.444444444444445, 8-15
				// 12.688888888888888, 8-16
				// 12.966666666666667, 8-17
				// 13.155555555555555, 8-18
				// 13.311111111111112, 8-19
				// 13.388888888888888, 8-20
				// 13.5, 8-21

				new Object[] { // Test TMA above price
						// start at 1st date.
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9, sr10, sr11, sr12),
						"2012-8-9#2012-8-21", 5, Position.below, 1, 0, 4, 8, "" },
				new Object[] { // Test price above TMA
						// start at 5st date.
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9, sr10, sr11, sr12),
						"2012-8-9#2012-8-21", 5, Position.above, 5, 0, 4, 8, "2012-8-18 2012-8-19 2012-8-20 2012-8-21" },
				new Object[] { // Test price above TMA
						// start at 1st date,end at 2nd date.
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9, sr10, sr11, sr12),
						"2012-8-9#2012-8-21", 5, Position.above, 1, 2, 4, 8, "2012-8-14 2012-8-15" },

				// result for period=2, sr1-sr8:
				// index begin =1, result length = 7
				// 11.45, 8-9
				// 12.099999999999998, 8-10
				// 12.499999999999998, 8-13
				// 12.349999999999998, 8-14
				// 12.849999999999998, 8-15
				// 13.249999999999996, 8-16
				// 13.199999999999996, 8-17

				new Object[] { // Test price above TMA,
						// start at 1st date.
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8), "2012-8-9#2012-8-21", 2,
						Position.above, 1, 0, 1, 7, "2012-8-9 2012-8-10 2012-8-15 2012-8-17" }, 
						
				new Object[] { // Test TMA 	 is above price,start at 2nd date	
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8), "2012-8-9#2012-8-21", 2,
						Position.below, 2, 0, 1, 7, "2012-8-14" }, };

	}

	@Test(dataProvider = "testTMAdp")
	public void testTMA(BarDataWrapper barSeries, String dates, int period, Position position, int start, int end,
			int indexBegin, int resultLength, String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);

		TAlib ta = new TMACompute(barSeries);
		TMA tma;
		if (end == 0) {
			tma = new TMA(ta, period, position, start);
		} else {
			tma = new TMA(ta, period, position, start, end);
		}

		FilterResult result = tma.buildFilterResults(barSeries);

		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("TMAResult").length, resultLength);
	}

	@Test
	public void testInvalidTMA() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new TMACompute(barSeries);
		TMA tma;

		// Test range <=1
		try {
			tma = new TMA(ta, 1, Position.above, 3);
			Assert.fail("period must be larger than 1");
		} catch (IllegalArgumentException e) {
		}

		try {
			tma = new TMA(ta, 5, Position.above, 0);
			Assert.fail("start must be larger than 0");
		} catch (IllegalArgumentException e) {
		}

		try {
			tma = new TMA(ta, 5, Position.above, 3, 2);
			Assert.fail("end must be larger than start");
		} catch (IllegalArgumentException e) {
		}

	}

}
