package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.technical.AbsPrice;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

public class TestAbsPrice {

	@DataProvider
	public Object[][] testAbsPricedp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 14.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.6);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 10.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 10.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 13.3);

		return new Object[][] {

				new Object[] { // Test 0 day
						new BarDataWrapper(), "", 5, Position.above, 1, 0, "" },
				new Object[] {
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8), "2012-8-9#2012-8-21", 15.0,
						Position.above, 1, 0, "" },

				new Object[] { 
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8),
						"2012-8-9#2012-8-21", 12.0, Position.below, 1, 0, "2012-8-8 2012-8-15 2012-8-16" },
				new Object[] { // Test price above TMA
						// start at 5st date.
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8),
						"2012-8-9#2012-8-21", 11.0, Position.above, 3, 0, "2012-8-10 2012-8-13 2012-8-14" }
				};

	}

	@Test(dataProvider = "testAbsPricedp")
	public void testAbsPrice(BarDataWrapper barSeries, String dates, double price, Position position, int start, int end,
			String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);

		AbsPrice absp;
		if (end == 0) {
			absp = new AbsPrice(price, position, start);
		} else {
			absp = new AbsPrice(price, position, start, end);
		}

		FilterResult result = absp.buildFilterResults(barSeries);

		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("absPrice").length, barSeries.getClose().length);
	}
	
	@DataProvider
	public Object[][] testAbsPricewithRecoveragedp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 14.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.6);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 10.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 10.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 13.3);

		return new Object[][] {

				new Object[] { // Test 0 day
						new BarDataWrapper(), "", 5, Position.above, 1, 0, 0, "" },
				new Object[] {
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8), "2012-8-9#2012-8-21", 15.0,
						Position.above, 1, 0, -2.0, "" },
				new Object[] { 
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8),
						"2012-8-9#2012-8-21", 12.0, Position.below, 1, 0, 1.0, "2012-8-8 2012-8-15" },
				new Object[] { // Test price above TMA
						// start at 5st date.
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8),
						"2012-8-9#2012-8-21", 11.0, Position.above, 1, 0, -1.5, "2012-8-8 2012-8-10 2012-8-17" }
				};

	}

	@Test(dataProvider = "testAbsPricewithRecoveragedp")
	public void testAbsPricewithRecoverage(BarDataWrapper barSeries, String dates, double price, Position position, int start, int end,
			double recoveryValue, String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);

		AbsPrice absp;
		if (end == 0) {
			absp = new AbsPrice(price, position, start, recoveryValue);
		} else {
			absp = new AbsPrice(price, position, start, end, recoveryValue);
		}

		FilterResult result = absp.buildFilterResults(barSeries);

		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("absPrice").length, barSeries.getClose().length);
	}
	
}
