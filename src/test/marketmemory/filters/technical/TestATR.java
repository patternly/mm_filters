package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.Filter;
import com.marketmemory.filters.TACompute.ATRCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.ATR;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.datasource.SymbolDataManager;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.StockRecord;

public class TestATR {

	@DataProvider
	public Object[][] testATRDp() {
		StockRecord sr_D1 = TestConfig.stock_D("2011-10-10", 4, 9, 2, 5);
		StockRecord sr_D2 = TestConfig.stock_D("2011-10-11", 5.03, 9, 2, 5);
		StockRecord sr_D3 = TestConfig.stock_D("2011-10-21", 4.97, 9, 5, 5);
		StockRecord sr_D4 = TestConfig.stock_D("2011-10-22", 4.99, 9, 2, 5);
		StockRecord sr_D5 = TestConfig.stock_D("2011-10-23", 4.99, 13, 2, 5);
		return new Object[][] {
				new Object[] { // Higher than 1 ATR
						new BarDataWrapper(sr_D1,sr_D2,sr_D3,sr_D4), "2011-10-11#2011-10-22", 1, 1, ATR.emptyValue, 
						1, 3, true, "2011-10-11 2011-10-21 2011-10-22"},
				new Object[] { // Higher than 1 ATR with absolute value logic.
						new BarDataWrapper(sr_D1,sr_D2,sr_D3,sr_D4), "2011-10-11#2011-10-22", 1, 5.0, ATR.emptyValue, 
						1, 3, false, "2011-10-11 2011-10-22"},
				new Object[] { // Between 1 and 2 
						new BarDataWrapper(sr_D1,sr_D2,sr_D3,sr_D4,sr_D5), "2011-10-12#2011-10-28", 2, 1.2, 1, 
						2, 3, true, "2011-10-22"},
				new Object[] { // Between 1 and 2 with absolute value logic.
						new BarDataWrapper(sr_D1,sr_D2,sr_D3,sr_D4,sr_D5), "2011-10-12#2011-10-28", 2, 8, 6, 
						2, 3, false, "2011-10-22"},
				new Object[] { // Lower than 1 ATR
						new BarDataWrapper(sr_D1,sr_D2,sr_D3,sr_D4,sr_D5), "2011-10-12#2011-10-28", 2, ATR.emptyValue, 1, 
						2, 3, true, "2011-10-21"},
				new Object[] { // Lower than 1 ATR with absolute value logic.
						new BarDataWrapper(sr_D1,sr_D2,sr_D3,sr_D4,sr_D5), "2011-10-12#2011-10-28", 2, ATR.emptyValue, 6, 
						2, 3, false, "2011-10-21"},
				new Object[] { // lower than 0 ATR
						new BarDataWrapper(sr_D1,sr_D2,sr_D3,sr_D4,sr_D5), "2011-10-12#2011-10-28", 1, ATR.emptyValue, 0,  
						1, 4, true, ""},
		};
	}
	@Test(dataProvider = "testATRDp")
	public void testATR(BarDataWrapper barSeries, String dates, int period, double atrUp, double atrDown, int indexBegin,
			int resultLength, boolean isRelative, String expectedDates) throws Exception {
		
		DateList expected = new DateList(expectedDates);
		ATRCompute ta = new ATRCompute(barSeries);
				
		ATR atr = new ATR(ta, period, atrUp, atrDown, isRelative);
		
		FilterResult result = atr.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("atrResult").length, resultLength);
	}
	
	@Test
	public void testInvalidATR() throws Exception {
		try{
			SymbolDataManager sdManager = null;
			TAlib ta = new ATRCompute(sdManager.getEODData());
			Filter atr = new ATR(ta, 2, ATR.emptyValue, ATR.emptyValue, true);
			Assert.fail("No exception");
		} catch(Exception e) {}
	}	
}
