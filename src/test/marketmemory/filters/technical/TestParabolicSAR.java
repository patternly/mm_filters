package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.ParabolicSARCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.ParabolicSAR;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;


public class TestParabolicSAR{
	
	@DataProvider
	public Object[][] testParabolicSARdp(){
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 17, 9, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 8, 11.6); //new low
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 7, 13.4); // new low
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 18, 10, 12.1); // new high
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0);

		return new Object[][]{
				new Object[]{ //test 0th day
						new BarDataWrapper(), 0.02, 0.2, Position.above, 1, 0,
						0, 0, ""},
				
				//AF = 0.02; maxAF = 0.2
				//SAR = [17.0, 16.82, 16.6436, 16.470727999999998, 16.301313439999998, 7.0, 7.0, 7.22]
				//close = [ 11.6, 12.6, 12.4, 12.3, 13.4, 12.1, 11.3, 11.0]
				new Object[]{ //test selecting dates with close prices above SAR
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 0.02, 0.2,
						Position.above, 1, 0,	1, 8, "2012-8-16 2012-8-17 2012-8-18"},
				new Object[]{ //test selecting dates with close prices below SAR
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 0.02, 0.2,
						Position.below, 1, 0,	1, 8, "2012-8-9 2012-8-10 2012-8-13 2012-8-14 2012-8-15"},
				new Object[]{ //test selecting dates with end index
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 0.02, 0.2,
						Position.below, 1, 4,	1, 8, "2012-8-9 2012-8-10 2012-8-13 2012-8-14"},
						
				//AF = 0.02; maxAF = 0.02
				//SAR = [17.0, 16.82, 8.0, 8.2, 8.395999999999999]
				//Close = [11.6, 12.6, 12.1, 11.3, 11.0]
				new Object[]{ //test case for AF = maxAF
						new BarDataWrapper(sr1,sr2,sr3,sr7,sr8,sr9), 0.02, 0.02,
						Position.above, 1, 0, 1, 5, "2012-8-16 2012-8-17 2012-8-18"},
				new Object[]{ //test case for AF = maxAF
						new BarDataWrapper(sr1,sr2,sr3,sr7,sr8,sr9), 0.02, 0.02,
						Position.below, 1, 0, 1, 5, "2012-8-9 2012-8-10"},
				
		};
	}
	
	@Test(dataProvider = "testParabolicSARdp")
	public void testParabolicSAR(BarDataWrapper barSeries, double baseAF, double maxAF,
			Position position, int start, int end, 
			int indexBegin, int resultLength,String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new ParabolicSARCompute(barSeries);
		ParabolicSAR parabolicSAR = null;

		if (end == 0) {
			parabolicSAR = new ParabolicSAR(ta, baseAF, maxAF, position, start);
		} else {
			parabolicSAR = new ParabolicSAR(ta, baseAF, maxAF, position, start, end);
		}
		
		FilterResult result = parabolicSAR.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		
		//System.out.println(result.getDateList());
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("parabolicSAR").length, resultLength);
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testInvalidParabolicSAR() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new ParabolicSARCompute(barSeries);
		ParabolicSAR parabolicSAR;
		
		// AF smaller than 0.
		try {
			parabolicSAR = new ParabolicSAR(ta, 0 , 0, Position.above, 1);
			Assert.fail("Acceleration Factor must be larger than 0");
		} catch(IllegalArgumentException e){}
		
		// start smaller than 0.
		try {
			parabolicSAR = new ParabolicSAR(ta, 0.1 , 1, Position.above, 0);
			Assert.fail("Maximum Acceleration Factor must be larger than 0");
		} catch(IllegalArgumentException e){}
		
		//max AF smaller than 0
		try {
			parabolicSAR = new ParabolicSAR(ta, 0 , 0, Position.above, 1);
			Assert.fail("Maximum Acceleration Factor must be larger than 0");
		} catch(IllegalArgumentException e){}
		
		//end smaller than start.
		try {
			parabolicSAR = new ParabolicSAR(ta, 0 , 0, Position.above, 2, 1);
			Assert.fail("start must be smaller than end");
		} catch(IllegalArgumentException e){}
		
		// max AF smaller than AF 
		try {
			parabolicSAR = new ParabolicSAR(ta, 2 , 1, Position.above, 1);
			Assert.fail("Maximum AF must be greater than AF");
		} catch(IllegalArgumentException e){}
		
		
	}
}