package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.OBVCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.OBV;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

/**
 * Test cases for OBV filter
 * 
 * @author Johnny Jiang
 * @since 2015-04-15
 */

public class TestOBV{
	@Test
	public void testInvalidOBV() throws Exception{
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new OBVCompute(barSeries);
		OBV obv;
				
		// Test sma signal period less than 2.
		try {
			obv = new OBV(ta, true, 1, 30, Position.above, 1);
			Assert.fail("SMA signal period must be larger than 1");
		} catch(IllegalArgumentException e){}
		
		// Test ema signal period less than 2.
		try {
			obv = new OBV(ta, false, 30, 1, Position.above, 1);
			Assert.fail("EMA signal period must be larger than 1");
		} catch (IllegalArgumentException e) {}

		// start must be larger than 0.
		try {
			obv = new OBV(ta, true,  30, 30, Position.above, 0);
			Assert.fail("start must be larger than 0");
		} catch(IllegalArgumentException e){}
		
		//end smaller than start.
		try {
			obv = new OBV(ta, true,  30, 30, Position.above, 2, 1);
			Assert.fail("start must be smaller than end");
		} catch(IllegalArgumentException e){}
	}
	
	@DataProvider
	public Object[][] testOBVdp(){
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 17, 9, 11.3, 100);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6, 80);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6, 60);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4, 120);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3, 150);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 7, 13.4, 75);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 18, 10, 12.1, 50);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3, 55);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0, 60);
		
		//SMA period = 2;
		//SMA period = 3;
		//SMA = [140.0, 210.0, 180.0, 45.0, 7.5, 20.0, -32.5, -90.0]
		//OBV = [180.0, 240.0, 120.0, -30.0, 45.0, -5.0, -60.0, -120.0]
		//EMA = [140.0, 206.6, 148.8, 29.6, 39.8, 9.9, -36.6, -92.2]
		return new Object[][]{
				new Object[]{ // above sma signal line;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						true, 2, 30, Position.above, 1, 0, 1, 8, "2012-8-9 2012-8-10 2012-8-15" },
				new Object[]{ // below sma signal line;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						true, 2, 30, Position.below, 1, 0, 1, 8, "2012-8-13 2012-8-14 2012-8-16 2012-8-17 2012-8-18" },
				new Object[]{ // below sma signal line, start = 2; test emaPeriod to be a don't-care value;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						true, 2, 2, Position.below, 2, 0, 1, 8, "2012-8-14 2012-8-17 2012-8-18" },
				new Object[]{ // below sma signal line, end = 2; test emaPeriod to be a don't-care value;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						true, 2, 2, Position.below, 1, 2, 1, 8, "2012-8-13 2012-8-14 2012-8-16 2012-8-17" },
				new Object[]{ // above EMA signal line;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						false, 30, 2, Position.above, 1, 0, 1, 8, "2012-8-9 2012-8-10 2012-8-15" },
				new Object[]{ // below EMA signal line; test smaPeriod to be a don't-care value;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						false, 2, 2, Position.below, 1, 0, 1, 8, "2012-8-13 2012-8-14 2012-8-16 2012-8-17 2012-8-18" },
				
				//SMA period = 3;
				//EMA period = 3;
				//SMA = [173.3, 180.0, 110.0, 45.0, 3.3, -6.6, -61.6]
				//OBV = [240.0, 120.0, -30.0, 45.0, -5.0, -60.0, -120.0]
				//EMA = [173.3, 146.6, 58.3, 51.6, 23.3, -18.3, -69.1]
				new Object[]{ // above SMA signal line;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						true, 3, 30, Position.above, 1, 0, 2, 7, "2012-8-10" },
				new Object[]{ // above EMA signal line;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						false, 30, 3, Position.above, 1, 0, 2, 7, "2012-8-10" },
				new Object[]{ // below EMA signal line; test smaPeriod to be don't-care value;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						false, 2, 3, Position.below, 1, 0, 2, 7, "2012-8-13 2012-8-14 2012-8-15 2012-8-16 2012-8-17 2012-8-18" },
				new Object[]{ // below EMA signal line; start = 2, end = 4
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						false, 2, 3, Position.below, 2, 4, 2, 7, "2012-8-14 2012-8-15 2012-8-16" },
		};
	}
	
	@Test(dataProvider = "testOBVdp")
	public void testOBV(BarDataWrapper barSeries, boolean isSMALine, int smaPeriod, int emaPeriod,
			Position position, int start, int end, int indexBegin, int resultLength,
			String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new OBVCompute(barSeries);
		OBV obv = null;

		if (end == 0) {
			obv = new OBV(ta, isSMALine, smaPeriod, emaPeriod, position, start);
		} else {
			obv = new OBV(ta, isSMALine, smaPeriod, emaPeriod, position, start, end);
		}
		
		FilterResult result = obv.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("obvResult").length, resultLength);
	}
	
}