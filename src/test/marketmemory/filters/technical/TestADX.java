package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.ADXCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.ADX;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

//Author: Yang Meng
public class TestADX {
	
	
	@DataProvider
	public Object[][] testADXdp() {
		
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 13, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 14, 13.5, 10.3, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 16, 15.5, 12, 13.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 16, 15.5, 11, 12.8);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 15, 15.5, 11, 12.6);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 15, 15.5, 12, 14.1);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 14, 13.5, 11, 12.2);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 13, 12.5, 11, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 12, 10.5, 10, 12.0);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 14, 15.5, 12, 13.9);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 13, 11.5, 10, 12.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 15, 14.5, 13, 14.7);
		return new Object[][] {
			new Object[] { //Test 0 day
				new BarDataWrapper(), "", 9, false, Position.above, 20,25, 1, 0, 0, 0, "" },
			new Object[] { //Test given dates are not enough to generate result.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 14, false, Position.above, 20,25,1, 0, 0, 0, "" },
				
				// ADX results for sr1 to sr12 for Period = 3:   index begin=5, result length=7
				// [4.7619047619047405,         8-15
				// 22.11297474455367, 			8-16
				// 33.680354732986295, 			8-17
				// 49.331060113008164,			8-18
				//  54.49833968403678, 			8-19
				// 40.043654024926916, 			8-20
				// 42.93621696710242, 			8-21
				//0.0, 0.0, 0.0, 0.0, 0.0]
				
				//minus_DI values:     period =3:   index begin =3, result length = 9
                //[15.884476534296027, 9.176225234619393, 6.147397834439398, 14.107077819594513, 
				//11.551269943834187, 24.012873378256057, 13.567836340389256,
				//29.44446546643875, 22.51737033973208, 
				//0.0, 0.0, 0.0]
				
				//plus_DI values
				//[14.440433212996393, 8.342022940563089, 5.588543485854001, 3.8849095544494365,
				//3.1810726179233564, 2.574675651211908, 63.59426054837335, 36.82285642837048, 
				//65.30618664032939, 
				//0.0, 0.0, 0.0]
				
			new Object[] { //Test ADX,  strong trend, and start at 1st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 3, false, Position.above, 20, 25, 1, 0, 5, 7, "2012-8-17 2012-8-18 2012-8-19 2012-8-20 2012-8-21" },
			new Object[] { //Test ADX, no trend, and start at 1st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 3, false, Position.below, 20, 25, 1, 0, 5, 7, "2012-8-15" },
			new Object[] { //Test ADX,  strong trend, and start at 1st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 3, false, Position.above, 20, 25, 1, 3, 5, 7, "2012-8-17 2012-8-18 2012-8-19" },

			new Object[] { //test minus_DI crosses above plus_DI
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 3,true, Position.below, 20,25,1, 0, 3, 9, "2012-8-19 2012-8-20 2012-8-21" },
			new Object[] { //test plus_DI crosses above minus_DI
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 3, true, Position.above, 20, 25, 3,0,3, 9, "2012-8-15 2012-8-16 2012-8-17 2012-8-18" },

// test a different period:
				// for period = 4:
				// minus_DI values:    index_begin= 4, result_length= 8
//				 9.659090909090907,  8-14
//				 7.136194029850744,  8-15
//				 13.058460076045625, 8-16
//				 11.334570957095707, 8-17
//				 20.015709513203944, 8-18
//				 13.568777410309869, 8-19
//				 25.77493111273198,  8-20
//				 21.29739739817943   8-21
				
				// plus_DI values are:  index_begin= 4, result_length= 8
//				 11.363636363636365, 8-14
//				 8.395522388059701,  8-15
//				 6.416349809885932,  8-16
//				 5.56930693069307,   8-17
//				 4.832166719694559,  8-18
//				 49.289129340714645, 8-19
//				 33.33637247751033,  8-20
//				 54.974229565574525  8-21
				
				// ADX result are :  result length = 5, index begin = 7
//				 21.107135201094934, 8-17 
//				 31.10685094065554,  8-18
//				 37.53692370659434, 8-19
//				 31.3506603874587,  8-20
//				 34.55144874570358  8-21
				
			new Object[] { // plusDI above minusDI
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 4, true, Position.below, 20,25, 2, 0, 4, 8, "2012-8-15 2012-8-20 2012-8-21" },
			new Object[] { //minus_DI above plus_DI
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", 4, true, Position.above, 20,25, 1, 0, 4, 8, "2012-8-16 2012-8-17 2012-8-18" },

			new Object[] { //ADX values, start at 2.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 4, false, Position.above, 20, 25, 2, 0, 7, 5, "2012-8-19 2012-8-20 2012-8-21" },
			new Object[] { //ADX values for different limit.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 4, false, Position.below, 25, 30, 1,0, 7, 5, "2012-8-17" },
		};
	}
	
	@Test(dataProvider = "testADXdp")
	public void testADX(BarDataWrapper barSeries, String dates, int period,
			boolean isCrossOver, Position position, int lowerLine, int upperLine, int start, int end, int indexBegin, int resultLength,
			String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new ADXCompute(barSeries);
		ADX adx = null;

		if (end == 0) {
			adx = new ADX(ta, period, isCrossOver, position,  lowerLine, upperLine, start);
		} else {
			adx = new ADX(ta, period, isCrossOver, position,  lowerLine, upperLine, start, end);
		}
		
		FilterResult result = adx.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		
//		if(isCrossOver){
//			Assert.assertEquals(result.getResults().get("minus_DI").length, resultLength);
//			Assert.assertEquals(result.getResults().get("plus_DI").length, resultLength);
//		}
//		else{
//			Assert.assertEquals(result.getResults().get("ADXResult").length, resultLength);
//		}
	}
	
	@Test
	public void testInvalidADX() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new ADXCompute(barSeries);
		ADX adx;
		
		// Test time period less than 1.
		try {
			adx = new ADX(ta, 1, true, Position.above, 20, 25,1);
			Assert.fail("Periods must be larger than 1");
		} catch(IllegalArgumentException e){}
		
		// start must be larger than 0.
		try {
			adx = new ADX(ta, 5, true, Position.above, 20,25,0);
			Assert.fail("start must be larger than 0");
		}catch(IllegalArgumentException e){}
		
		// if lower line < 1
		try {
			adx = new ADX(ta, 5, true, Position.above, 0,25,1,2);
			Assert.fail("upper and lower line must between 0 to 100");						
		} catch(IllegalArgumentException e){}
		
		//if higher line >100
		try {
			adx = new ADX(ta, 5, true, Position.above, 20,101,1,0);
			Assert.fail("upper and lower line must between 0 to 100");
		}catch(IllegalArgumentException e){}
		
		// if higher line is smaller than lower line
		try {
			adx = new ADX(ta, 5, true, Position.above, 25,20,1,0);
			Assert.fail("upper line must larger than lower line");	
		}catch(Exception e){}
		
				
	}

}
