package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;


import com.marketmemory.filters.TACompute.ChaikinOscillatorCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.ChaikinOscillator;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.StockRecord;
import com.patternly.util.Position;

/**
 * 
 * @author Yangqiao Meng
 *
 */
public class TestChaikinOscillator {

	@DataProvider
	public Object[][] testChaikinOscdp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3,1000);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6,1000);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6,900);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4,1100);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3,1100);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4,1000);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 12.1,900);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3,900);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.4,800);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 11.5,1100);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 15.5, 10, 10.6,1000);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 15.5, 10, 9.7,1200);
		return new Object[][] {
			new Object[] { //Test 0 day
				new BarDataWrapper(), "",0, 3, 10, Position.above, 0, 0, 1, 0, "" },
				
				new Object[] { // test if data is not more enough to generate result
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 
						"2012-8-9#2012-8-21",0, 2, 10, Position.above, 0, 0, 1, 0, "" },

				// for fast period = 3,slow period=5,from sr1-sr12, the ChaikinOsc result should be:
					//index begin=4,result length=8
//						-77.20538720538707,    8-14
//						 -34.825236491902956,  8-15
//						 -50.3488589599699,    8-16
//						 -126.22283238023965,  8-17
//						 -195.93156357816838,  8-18
//						 -269.84588004778334,  8-19
//						 -379.81270249938825,  8-20
//						 -574.9843747182067,   8-21

				

				new Object[] { // fast=3,slow=5,overbought:
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", -40,3, 5, Position.above, 4, 8, 1, 0, "2012-8-15" },
				
				new Object[] { // fast=3,slow=5,oversold,start from 5th date:
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
						"2012-8-9#2012-8-21",0, 3, 5, Position.below, 4, 8, 5, 0, "2012-8-18 2012-8-19 2012-8-20 2012-8-21" },

				new Object[] { // fast=3,slow=5,oversold, start from day1, ends at day2:
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
						"2012-8-9#2012-8-21", 0,3, 5, Position.below, 4, 8, 1, 2, "2012-8-14 2012-8-15" },

			
				
				//For fast period=2, slow period=7,  Chaikin oscillator based on sr1-sr9 should be:
//				   index begin =6, result length = 3

//					-120.36896379886821,      8-16
//						-310.23779743985506,  8-17
//						-469.63506991315626   8-18
				
								
				new Object[] { // fast=2.slow=7, overbought:
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 
					"2012-8-9#2012-8-21", 0,2, 7, Position.above, 6, 3, 1, 0, "" },
				//With different start and end.
				new Object[] { // fast=2.slow=7, oversold, start=2,end=3:
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 
					"2012-8-9#2012-8-21", 0,2, 7, Position.below, 6, 3, 2, 3, "2012-8-17 2012-8-18" },
		};
	}
	
	@Test(dataProvider = "testChaikinOscdp")
	public void testChaikinOscillator(BarDataWrapper barSeries, String dates, int signalLine,int fastPeriod, int slowPeriod, Position position, int indexBegin,
			int resultLength, int start, int end, String expectedDates) throws Exception {
		
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new ChaikinOscillatorCompute(barSeries);
		ChaikinOscillator chaikinoscillator = null;
		
		if (end == 0) {
			chaikinoscillator = new ChaikinOscillator(ta, signalLine,fastPeriod,slowPeriod, position, start);
		} else {
			chaikinoscillator = new ChaikinOscillator(ta,signalLine, fastPeriod,slowPeriod, position, start, end);
		}
		
		FilterResult result = chaikinoscillator.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("ChaikinOscResult").length, resultLength);
	}
	
	@Test
	public void testInvalidChaikinOscData() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3,1000));
		TAlib ta = new ChaikinOscillatorCompute(barSeries);
		ChaikinOscillator chaikinoscillator;
		
		// Test a Period less than 1.
		try {
			chaikinoscillator = new ChaikinOscillator(ta, 0,0, 10, Position.above, 1, 0);
			Assert.fail("All periods must be larger than 1");
		} catch(IllegalArgumentException e){}
		
		// Test fast period is larger than slow period.
		try {
			chaikinoscillator = new ChaikinOscillator(ta, 0,3, 2, Position.above, 1, 0);
			Assert.fail("slow periods must be larger than fast period");
		} catch(IllegalArgumentException e){}
				
		// test end before start
		try {
			chaikinoscillator = new ChaikinOscillator(ta, 0,3, 10, Position.below, 3, 2);
			Assert.fail("end should not be smaller than start");
		} catch(IllegalArgumentException e){}
	}
	
	
}
