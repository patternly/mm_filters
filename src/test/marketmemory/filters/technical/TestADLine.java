package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.ADLineCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.ADLine;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

/**
 * Test cases for ADLine;
 * 
 * @author Johnny Jiang
 *
 */

public class TestADLine{
	@Test
	public void testInvalidADLine() throws Exception{
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new ADLineCompute(barSeries);
		ADLine ad;
				
		// Test signal period less than 1.
		try {
			ad = new ADLine(ta, true, 1, Position.above, 1);
			Assert.fail("SMA signal period must be larger than 1");
		} catch(IllegalArgumentException e){}
		
		// start must be larger than 0.
		try {
			ad = new ADLine(ta, true, 2, Position.above, 0);
			Assert.fail("start must be larger than 0");
		} catch(IllegalArgumentException e){}
		
		//end smaller than start.
		try {
			ad = new ADLine(ta, true, 2, Position.above, 2, 1);
			Assert.fail("start must be smaller than end");
		} catch(IllegalArgumentException e){}
	}
	
	@DataProvider
	public Object[][] testADLinedp(){
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 17, 9, 11.3, 100);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6, 80);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6, 60);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4, 120);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3, 150);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 7, 13.4, 75);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 18, 10, 12.1, 50);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3, 55);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0, 60);
		
		return new Object[][]{
				//sma.signalPeriod = 3;
				//AD = [-52.629, -67.902, -92.448, -54.50, -78.25, -107.2, -145.43]
				//sma = [-48.16, -56.62, -70.99, -71.619, -75.070, -80.006, -110.317]
				//obv = [ 240.0, 120.0, -30.0, 45.0, -5.0, -60.0, -120.0]
				new Object[]{ // above sma signal line;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						true, 3, Position.above, 1, 0, 2, 7, "2012-8-15" },
				new Object[]{ // below sma signal line;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						true, 3, Position.below, 1, 0, 2, 7, "2012-8-10 2012-8-13 2012-8-14 2012-8-16 2012-8-17 2012-8-18" },
				new Object[]{ // below sma signal line; test start = 2;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						true, 3, Position.below, 2, 0, 2, 7, "2012-8-13 2012-8-14 2012-8-17 2012-8-18" },
				new Object[]{ // below sma signal line; test end = 2;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						true, 3, Position.below, 1, 2, 2, 7, "2012-8-10 2012-8-13 2012-8-16 2012-8-17" },
				new Object[]{ // below sma signal line; test start = 2, end = 3;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						true, 3, Position.below, 2, 3, 2, 7, "2012-8-13 2012-8-14 2012-8-17 2012-8-18" },
						
				new Object[]{ // above obv signal line;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						false, 3, Position.above, 1, 0, 2, 7, "" },
				new Object[]{ // below sma signal line;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						false, 3, Position.below, 1, 0, 2, 7, "2012-8-10 2012-8-13 2012-8-14 2012-8-15 2012-8-16 2012-8-17 2012-8-18" },
				new Object[]{ // below sma signal line; test start = 2
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						false, 3, Position.below, 2, 0, 2, 7, "2012-8-13 2012-8-14 2012-8-15 2012-8-16 2012-8-17 2012-8-18" },
				new Object[]{ // above sma signal line; end = 2;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						false, 3, Position.below, 1, 2, 2, 7, "2012-8-10 2012-8-13" },
				new Object[]{ // above sma signal line; start = 3, end = 4;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						false, 3, Position.below, 3, 4, 2, 7, "2012-8-14 2012-8-15" },
		};
	}
	
	@Test(dataProvider = "testADLinedp")
	public void testADLine(BarDataWrapper barSeries, boolean isSMALine,
			int signalPeriod, Position position, int start, int end, 
			int indexBegin, int resultLength,String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new ADLineCompute(barSeries);
		ADLine ad = null;

		if (end == 0) {
			ad = new ADLine(ta, isSMALine, signalPeriod, position, start);
		} else {
			ad = new ADLine(ta, isSMALine, signalPeriod, position, start, end);
		}
		
		FilterResult result = ad.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("adLineResult").length, resultLength);
	}
	
}