package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.TACompute.UltimateOscCompute;
import com.marketmemory.filters.technical.UltimateOsc;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.StockRecord;

/**
 * 
 * @author Johnny Jiang
 *
 */
public class TestUltimateOsc {

	@DataProvider
	public Object[][] testUltimateOscdp() {

		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 12.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 13.5);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 15.5, 10, 13.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 15.5, 10, 14.7);
		
		
		return new Object[][] { 
				
				//timePeriods = 1, 2, 4
				//UltimateOsc = [42.46103896103897, 57.07792207792208, 42.727272727272734,
				//					28.246753246753258, 21.428571428571427.81818181818181]
				new Object[] { // test low limit = 0; high limit = 100
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						1, 2, 4, 0, 100, 1, 0, 4, 5, "2012-8-14 2012-8-15 2012-8-16 2012-8-17 2012-8-18" }, 
				new Object[] { // test low limit = 50; high limit = 100
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						1, 2, 4, 50, 100, 1, 0, 4, 5, "2012-8-15" }, 
				new Object[] { // test low limit = 50; high limit = 100
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						1, 2, 4, 0, 50, 1, 0, 4, 5, "2012-8-14 2012-8-16 2012-8-17 2012-8-18" },
				new Object[] { // test start = 2;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						1, 2, 4, 0, 50, 2, 0, 4, 5, "2012-8-17 2012-8-18" },
				new Object[] { // test end = 2;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						1, 2, 4, 0, 50, 1, 2, 4, 5, "2012-8-14 2012-8-16 2012-8-17" },
				
				//timePeriods = 1, 3, 5
				//UltimateOsc = [56.24140565317036, 41.97402597402597, 31.255411255411268, 23.255411255411257, 
				//				52.277056277056275, 57.4025974025974, 76.58874458874458]
				new Object[] {
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						1, 3, 5, 50, 100, 1, 0, 5, 7, "2012-8-15 2012-8-19 2012-8-20 2012-8-21" },
				new Object[] {
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						1, 3, 5, 0, 50, 1, 0, 5, 7, "2012-8-16 2012-8-17 2012-8-18" },
				new Object[] { // test start = 2
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						1, 3, 5, 50, 100, 2, 0, 5, 7, "2012-8-20 2012-8-21" },
				new Object[] {
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						1, 3, 5, 50, 100, 1, 2, 5, 7, "2012-8-15 2012-8-19 2012-8-20" },						

		};
	}

	@Test(dataProvider = "testUltimateOscdp")
	public void testUltimateOscdp(BarDataWrapper barSeries, int timePeriod1, int timePeriod2,
			int timePeriod3, double lowLimit, double highLimit, int start, int end, 
			int indexBegin, int resultLength, String expectedDates)throws Exception {
		DateList expected = new DateList(expectedDates);

		TAlib ta = new UltimateOscCompute(barSeries);
		UltimateOsc ultimateOsc = null;

		if (end == 0) {
			ultimateOsc = new UltimateOsc(ta, timePeriod1, timePeriod2, timePeriod3, lowLimit, highLimit, start);
		} else {
			ultimateOsc = new UltimateOsc(ta, timePeriod1, timePeriod2, timePeriod3, lowLimit, highLimit, start, end);
		}

		FilterResult result = ultimateOsc.buildFilterResults(barSeries);

		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("UltimateOscResult").length, resultLength);
	}

	@Test
	public void testInvalidUltimateOsc() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new UltimateOscCompute(barSeries);
		UltimateOsc ultimateOsc;

		// start smaller than 1.
		try {
			ultimateOsc = new UltimateOsc(ta, 7, 14, 28, 50, 100, 0);
			Assert.fail("start must be greater than 0");
		} catch (IllegalArgumentException e) {
		}

		// end smaller than start.
		try {
			ultimateOsc = new UltimateOsc(ta, 7, 14, 28, 50, 100, 2, 1);;
			Assert.fail("start must be smaller than end");
		} catch (IllegalArgumentException e) {
		}

		// timePeriod smaller than 1.
		try {
			ultimateOsc = new UltimateOsc(ta, 0, 1, 4, 50, 100, 1);
			Assert.fail("Time periods must be greater than 1");
		} catch (IllegalArgumentException e) {
		}

		// low limit larger than high limit.
		try {
			ultimateOsc = new UltimateOsc(ta, 7, 14, 28, 50, 0, 1);
			Assert.fail("Low limit must be smaller than high limit");
		} catch (IllegalArgumentException e) {
		}
		
		// low limit not in valid range.
		try {
			ultimateOsc = new UltimateOsc(ta, 7, 14, 28, -1, 0, 1);
			Assert.fail("Low limit must be between 0 and 100");
		} catch (IllegalArgumentException e) {
		}
		
		// high limit not in valid range.
		try {
			ultimateOsc = new UltimateOsc(ta, 7, 14, 28, 50, 120, 1);
			Assert.fail("High limit must be between 0 and 100");
		} catch (IllegalArgumentException e) {
		}
	}
}