package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;
import com.marketmemory.filters.TACompute.CMOCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.CMO;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

//Author: Yang Meng

public class TestCMO {
	
    @DataProvider
	public Object[][] testCMOdp() {
		
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 12.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 13.5);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 15.5, 10, 13.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 15.5, 10, 14.7);
		
		return new Object[][] {
		new Object[] { //Test 0 day
				new BarDataWrapper(), "", 5, 50, false, Position.above, 1, 0, 0, 0, "" },
			new Object[] { //Test given dates are not enough to generate result.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 13, 50, false, Position.above, 1, 0, 0, 0, "" },
				
			//CMO result for period=5: index_begin=5, result_length=7
//				77.77777777777783,   8-15
//				 10.982658959537554,  8-16
//				 -13.901345291479823, 8-17
//				 -22.08977935582047,  8-18
//				 38.66929961461286,   8-19
//				 40.15833676474015,   8-20
//				 55.135592455226025,  8-21
//				
						 
			new Object[] { //Test over-bought, limit=50
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 5, 50, false, Position.above, 1, 0, 5, 7, "2012-8-15 2012-8-21" },
			new Object[] { //Test over-sold, limit= -50
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 5, 50, false, Position.below, 1, 0, 5, 7, "" },
			new Object[] { ////Test center line crossovers, above zero, end at 2
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 5, 50, true, Position.above, 1, 2, 5, 7, "2012-8-15 2012-8-16 2012-8-19 2012-8-20" },
			new Object[] { //Test center line crossovers, below zero
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 5, 50, true, Position.below, 1, 0, 5, 7, "2012-8-17 2012-8-18" },

			// try different input values, such as period, limit, start, end	
			// for period=3, CMO results are:    index begin = 3, result length = 9
//				 73.3333333333334,    8-13
//				 57.57575757575767,   8-14
//				 83.03030303030306,   8-15
//				 -11.306901615271697, 8-16
//				 -39.900497512437816, 8-17
//				 -49.1261318172247,   8-18
//				 48.908848248746516,  8-19
//				 50.84760467093504,   8-20
//				 69.7733104989943,    8-21
				
			new Object[] { //Test over-bought,limit=40, start from 3
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 3, 40, false, Position.above, 3, 0, 3, 9, "2012-8-15 2012-8-21" },
			new Object[] { //Test central line crossovers, below zero, and start at 1st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 3, 50, true, Position.below, 1, 0, 3, 9, "2012-8-16 2012-8-17 2012-8-18" },
			new Object[] { //Test central line crossovers, above zero, and start at 2, end at 3
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 3, 50, true, Position.above, 2, 3, 3, 9, "2012-8-14 2012-8-15 2012-8-20 2012-8-21" },

		};
	}

    @Test(dataProvider = "testCMOdp")
	public void testCMO(BarDataWrapper barSeries, String dates, int period, int limit,
			boolean isCentralLine, Position position, int start, int end, int indexBegin, int resultLength,
			String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new CMOCompute(barSeries);
		CMO cmo = null;

		if (end == 0) {
			cmo = new CMO(ta, period, limit, isCentralLine, position, start);
		} else {
			cmo = new CMO(ta, period, limit, isCentralLine, position, start, end);
		}
		
		FilterResult result = cmo.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("CMOResult").length, resultLength);
	}
	
	@Test
	public void testInvalidCMO() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new CMOCompute(barSeries);
		CMO cmo;
		
		// Test time period less than 1.
		try {
			cmo = new CMO(ta, 1 ,50, true, Position.above, 1);
			Assert.fail("Periods must be larger than 1");
		} catch(IllegalArgumentException e){}
		
		// Test limit is out of range
		try {
			cmo = new CMO(ta, 5 ,101, false, Position.above, 1);
			Assert.fail("Limit must between 0 to 100");
		} catch(IllegalArgumentException e){}
		
		// Test start < 0
		try {
			cmo = new CMO(ta, 5 ,50, true, Position.above, 5,3);
			Assert.fail("start must be larger than 0");
		} catch(Exception e){}
		
		// test start is later than end
		try {
			cmo = new CMO(ta, 5 ,50, true, Position.above, 0);
			Assert.fail("End should not be smaller than start");
		} catch(Exception e){}
	}
}
