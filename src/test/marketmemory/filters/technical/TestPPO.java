package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.PPOCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.PPO;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;


public class TestPPO{
	@DataProvider
	public Object[][] testPPOdp() {
		
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 12.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0);

		return new Object[][] {
				new Object[] { //Test 0 day
						new BarDataWrapper(), 12, 26, 9, false, Position.above, 1, 0, 0, 0, "" },
				
				//slow = 2; fast = 3; signal = 2
				//PPO = [1.8340210912425425, 0.8570345089116402, 1.823528702799768, -0.3174721204948335,
				//				-1.7277742223897048, -1.8744851907006355]
				//PPOSignal = [2.536728855480424, 1.4169326244345681, 1.6879966766780345, 0.3510174785627891, 
				//				-1.0348436554055402, -1.5946046789356036]
				new Object[] {  //test center line crossover; below
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						2, 3, 2, false, Position.below, 1, 0, 3, 6, "2012-8-16 2012-8-17 2012-8-18" },
				new Object[] { //test center line crossover; above
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9), 
						2, 3, 2, false, Position.above, 1, 0, 3, 6, "2012-8-13 2012-8-14 2012-8-15" },
		
				new Object[] {  //test above signal line crossover;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						2, 3, 2, true, Position.above, 1, 0, 3, 6, "2012-8-15" },
				new Object[] {  //test below signal line crossover;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						2, 3, 2, true, Position.below, 1, 0, 3, 6, "2012-8-13 2012-8-14 2012-8-16 2012-8-17 2012-8-18" },
				new Object[] {  //test below signal line crossover with end = 2
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						2, 3, 2, true, Position.below, 1, 2, 3, 6, "2012-8-13 2012-8-14 2012-8-16 2012-8-17" },
				new Object[] {  //test below signal line crossover with start = 2
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						2, 3, 2, true, Position.below, 2, 0, 3, 6, "2012-8-14 2012-8-17 2012-8-18" },
				
				//fastPeriod = 3; slowPeriod = 4; signalPeriod = 3;
				//PPO = [1.4352108584858274, 0.3083933471888875, -0.7706899682605398, -1.2343283106098515]
				//PPOsignal = [1.1572909284177961, 0.7328421378033418, -0.01892391522859893, -0.6266261129192252]
				new Object[] {  //test center line crossover; above
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						3, 4, 3, false, Position.above, 1, 0, 5, 4, "2012-8-15 2012-8-16" },
				new Object[] {  //test center line crossover; below
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						3, 4, 3, false, Position.below, 1, 0, 5, 4, "2012-8-17 2012-8-18" },
				new Object[] {  //test signal line crossover; above
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						3, 4, 3, true, Position.above, 1, 0, 5, 4, "2012-8-15" },
				new Object[] {  //test signal line crossover; below
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9),
						3, 4, 3, true, Position.below, 1, 0, 5, 4, "2012-8-16 2012-8-17 2012-8-18" },
		};
	}
	
	@Test(dataProvider = "testPPOdp")
	public void testPPO(BarDataWrapper barSeries, int fastPeriod, int slowPeriod, int signalPeriod,
			boolean isSignalLine, Position position, int start, int end, int indexBegin, int resultLength,
			String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new PPOCompute(barSeries);
		PPO ppo = null;

		if (end == 0) {
			ppo = new PPO(ta, fastPeriod, slowPeriod, signalPeriod, isSignalLine, position, start);
		} else {
			ppo = new PPO(ta, fastPeriod, slowPeriod, signalPeriod, isSignalLine, position, start, end);
		}
		
		FilterResult result = ppo.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("ppoResult").length, resultLength);
	} 
	
		@SuppressWarnings("unused")
		@Test
		public void testInvalidPPO() throws Exception {
			BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
			TAlib ta = new PPOCompute(barSeries);
			PPO ppo;
			
			// Test time period less than 1.
			try {
				ppo = new PPO(ta, 1 , 26, 9, true, Position.above, 1);
				Assert.fail("All periods must be larger than 1");
			} catch(IllegalArgumentException e){}
			
			// Test start less than 1.
			try {
				ppo = new PPO(ta, 1 , 26, 9, true, Position.above, 0);
				Assert.fail("start must be larger than 0");
			} catch(Exception e){}
			
			// End smaller than start.
			try {
				ppo = new PPO(ta, 1 , 26, 9, true, Position.above, 2, 1);
				Assert.fail("start must be smaller than end");
			} catch(Exception e){}
			
			// Fast period larger than slow period
			try {
				ppo = new PPO(ta, 5, 5, 9, true, Position.above, 1);
				Assert.fail("slow period must be larger than fast period");
			} catch(Exception e){}
		}
}