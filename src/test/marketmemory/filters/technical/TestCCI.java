package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.CCICompute;
import com.marketmemory.filters.TACompute.RSICompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.CCI;
import com.marketmemory.filters.technical.RSI;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.StockRecord;


//Author: Yang Meng
public class TestCCI {
	
	@DataProvider
	public Object[][] testCCIdp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 12.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.4);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 11.5);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 15.5, 10, 10.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 15.5, 10, 9.7);
		return new Object[][] {
			new Object[] { //Test 0 day
				new BarDataWrapper(), "", 2, 100, true, 0, 0, 1, 0, "" },
				
				new Object[] { // test if data is not more enough to generate result
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5), 
						"2012-8-9#2012-8-21", 8, 100, true, 0, 0, 1, 0, "" },

				// for look-back period = 3, the CCI result should be:

						// index begin=2, result = 10
						//	[99.99999999999973, 8-10
//							44.827586206896214, 8-3
//							-80.00000000000065, 8-14
//							99.99999999999923,  8-15
//							-62.500000000000256, 8-16
//							-85.29411764705888, 8-17
//							-39.99999999999979, 8-18
//							100.00000000000267, 8-19
//							-99.99999999999953, 8-20
//							-99.99999999999972, 8-21
//							0.0, 0.0]
				

				new Object[] { // period=3,limit=90,overbought:
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", 3, 90, true, 2, 10, 1, 0, "2012-8-10 2012-8-15 2012-8-19" },
				
				new Object[] { // period=3,limit=100,overbought:
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
						"2012-8-9#2012-8-21", 3, 100, true, 2, 10, 1, 0, "2012-8-19" },

				new Object[] { // period=3,limit=-100,oversold:
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
						"2012-8-9#2012-8-21", 3, 85, false, 2, 10, 1, 0, "2012-8-17 2012-8-20 2012-8-21" },

				new Object[] { //// period=3, limit=-70, oversold, start=2:
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", 3, 70, false, 2, 10, 2, 0, "2012-8-21" },
				
				
				
				//For the given stock records,  CCI with look-back period = 6 should be:
//				   index begin =5, result length = 7

//					[86.55462184873939, 8-15
//					2.702702702702273,  8-16
//					-155.55555555555583,8-17 
//					-90.90909090909079, 8-18
//					-55.55555555555531, 8-19
//					-108.06451612903187, 8-20
//					-147.3684210526302,  8-21
//					0.0, 0.0, 0.0, 0.0, 0.0]
				
								
				new Object[] { // for lookback period=6, overbought, limit=100:
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", 6, 100, true, 5, 7, 1, 0, "" },
				//With different start and end.
				new Object[] { // for lookback period=6, oversold, limit=-100,start=1,end=2:
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
					"2012-8-9#2012-8-21", 6, 100, false, 5, 7, 1, 2, "2012-8-17 2012-8-20 2012-8-21" },
		};
	}
	
	@Test(dataProvider = "testCCIdp")
	public void testCCI(BarDataWrapper barSeries, String dates, int lookBackPeriod, int limit, boolean isOverBought, int indexBegin,
			int resultLength, int start, int end, String expectedDates) throws Exception {
		
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new CCICompute(barSeries);
		CCI cci = null;
		
		if (end == 0) {
			cci = new CCI(ta, lookBackPeriod, limit, isOverBought, start);
		} else {
			cci = new CCI(ta, lookBackPeriod, limit, isOverBought, start, end);
		}
		
		FilterResult result = cci.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("cciResult").length, resultLength);
	}
	
	@Test
	public void testInvalidRSIData() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new CCICompute(barSeries);
		CCI cci;
		
		// Test lookBackPeriod less than 1.
		try {
			cci = new CCI(ta, 0, 100, true, 1, 0);
			Assert.fail("look-back period less than 1 fail");
		} catch(IllegalArgumentException e){}
		
		// Test limit less than 1.
		try {
			cci = new CCI(ta, 5, -1, true, 1, 0);
			Assert.fail("limit must be larger than 0");
		} catch(IllegalArgumentException e){}
				
		// test end before start
		try {
			cci = new CCI(ta, 5, 100, true, 3, 2);
			Assert.fail("end should not be smaller than start");
		} catch(IllegalArgumentException e){}
	}
	

}
