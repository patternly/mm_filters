package test.marketmemory.filters.technical;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.AroonCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.Aroon;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.StockRecord;

public class TestAroon{

	@DataProvider
	public Object[][] testAroondp() {
		
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 17, 9, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 8, 11.6); //new low
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 7, 13.4); // new low
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 18, 10, 12.1); // new high
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 13.5);

		return new Object[][] {
				new Object[] { //Test 0 day
					new BarDataWrapper(), 4, true, 1, 0, 0, 0, "" },
					
				//case only used for data extraction
//				new Object[] { //Test given dates are not enough to generate result.
//					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12
//							,sr13,sr14,sr15,sr16),
//					5, true, 1, 0, 0, 0, "" },
					
				//peroid = 4days
				//aroon up = [0.0, 100.0, 100.0, 75.0, 50.0, 25.0, 0.0, 100.0, 100.0, 
				//100.0, 100.0, 100.0]
				//aroon down = [25.0, 100.0, 75.0, 50.0, 25.0, 0.0, 100.0, 100.0, 100.0,
				//100.0, 100.0, 100.0]
				new Object[] { //Test 1st day aroonUp below aroonDown building AroonUp datelist
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5), 4, false, 1, 0, 4, 1, "2012-8-14" },
				new Object[] { //Test 1st day aroonUp below aroonDown building AroonDown datelist
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5), 4,  true, 1, 0, 4, 1, "" },
				
				new Object[] { //test with 5 day period; AroonUp over AroonDown
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), 4, true, 
					1, 0, 4, 4, "2012-8-16 2012-8-17" },
				new Object[] { //test with 5 day period; AroonUp below AroonDown
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8), 4, false, 
					1, 0, 4, 4, "2012-8-14" },
					
//				//Period = 5 days
				//AroonUp = [0.0, 100.0, 80.0, 60.0, 40.0, 20.0, 0.0, 100.0, 100.0, 100.0, 100.0]
				//AroonDown = [100.0, 80.0, 60.0, 40.0, 20.0, 0.0, 100.0, 100.0, 100.0, 100.0, 100.0]
				new Object[] { //
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10), 5, false, 
					1, 0, 5, 5, "2012-8-15 "},
				new Object[] { //
					new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10), 5, true, 
					1, 0, 5, 5, "2012-8-16 2012-8-17 2012-8-18 2012-8-19" },
		};
	}
	
	@Test(dataProvider = "testAroondp")
	public void testAroon(BarDataWrapper barSeries, int period,boolean isAroonUp, 
			int start, int end, int indexBegin, int resultLength,
			String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new AroonCompute(barSeries);
		Aroon aroon = null;

		if (end == 0) {
			aroon = new Aroon(ta, period, isAroonUp, start);
		} else {
			aroon = new Aroon(ta, period, isAroonUp, start, end);
		}
		
		FilterResult result = aroon.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("aroonUp").length, resultLength);
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testInvalidAroon() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new AroonCompute(barSeries);
		Aroon aroon;
		
		// Test time period less than 1.
		try {
			aroon = new Aroon(ta, 1 , true, 1);
			Assert.fail("All periods must be larger than 1");
		} catch(IllegalArgumentException e){}
		
		// start must be larger than 0.
		try {
			aroon = new Aroon(ta, 1 ,true, 0);
			Assert.fail("start must be larger than 0");
		} catch(IllegalArgumentException e){}
		
		//end smaller than start.
		try {
			aroon = new Aroon(ta, 1 ,true, 10, 2);
			Assert.fail("start must be smaller than end");
		} catch(IllegalArgumentException e){}
	}
}