package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;


import com.marketmemory.filters.TACompute.BOPCompute;
import com.marketmemory.filters.TACompute.TAlib;

import com.marketmemory.filters.technical.BOP;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

public class TestBOP {
	
	@DataProvider
	public Object[][] testBOPdp() {
		
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 12.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.4);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 11.5);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 15.5, 10, 10.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 15.5, 10, 9.7);
		return new Object[][] {
			new Object[] { //Test 0 day
				new BarDataWrapper(), "",Position.above, 1, 0, 0, 0, "" },
			// BOP values for given stock data are:
//				 -0.34999999999999964, 8-8
//				 0.17142857142857132,  8-9
//				 0.29090909090909084,  8-10
//				 0.25454545454545463,  8-13
//				 0.2363636363636365,   8-14
//				 0.43636363636363645,  8-15
//				 0.19999999999999993,  8-16
//				 0.054545454545454675, 8-17
//				 0.0727272727272728,   8-18
//				 0.09090909090909091,  8-19
//				 -0.0727272727272728,  8-20
//				 -0.2363636363636365]  8-21
				
				
				
			new Object[] { //Test BOP,  above zero, and start at 1st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", Position.above, 5, 0, 0, 12, "2012-8-15 2012-8-16 2012-8-17 2012-8-18 2012-8-19" },
			new Object[] { //Test BOP, below, and start at 1st date.
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", Position.below, 1, 0, 0, 12, "2012-8-8 2012-8-20 2012-8-21" },
			
			// try different start and end
			new Object[] { 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", Position.above, 4, 6, 0, 12, "2012-8-14 2012-8-15 2012-8-16" },

			new Object[] { 
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", Position.below, 2, 0, 0, 12, "2012-8-21" },
		};
	}
	
	@Test(dataProvider = "testBOPdp")
	public void testBOP(BarDataWrapper barSeries, String dates, Position position,int start, int end, int indexBegin, int resultLength,
			String expectedDates) throws Exception {
		
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new BOPCompute(barSeries);
		BOP bop ;

		if (end == 0) {
			bop = new BOP(ta, position, start);
		} else {
			bop = new BOP(ta,  position,  start, end);
		}
		
		FilterResult result = bop.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("BOPResult").length, resultLength);
	}
	
	@Test
	public void testInvalidADX() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new BOPCompute(barSeries);
		BOP bop;
		

		
		// start must be larger than 0.
		try {
			bop = new BOP(ta,  Position.above, 0);
			Assert.fail("start must be larger than 0");
		}catch(IllegalArgumentException e){}
		
		// if end< start
		try {
			bop = new BOP(ta,Position.above, 10,9);
			Assert.fail("end should not be smaller than start");						
		}catch(IllegalArgumentException e){}
		
				
				
	}


}
