package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.TACompute.TEMACompute;
import com.marketmemory.filters.technical.TEMA;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

/**
 * 
 * @author Johnny Jiang
 * @lastUpdate Mar 6, 2015
 *
 */
public class TestTEMA {
	@Test
	public void testInvalidTEMA() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D(
				"2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new TEMACompute(barSeries);
		TEMA tema;
		
		// timePeriod smaller than 2
		try { 
			tema = new TEMA(ta, 1, Position.above, 1);
			Assert.fail("period must be greater or equal to 2");
		} catch (IllegalArgumentException e) {
		}
		
		// start smaller than 1
		try { 
			tema = new TEMA(ta, 2, Position.above, 0);
			Assert.fail("start must be greater than 0");
		} catch (IllegalArgumentException e) {
		}
		
		//end smaller than start
		try{
			tema = new TEMA(ta, 2, Position.above, 2, 1);
			Assert.fail("end must be greater than start");
		} catch (IllegalArgumentException e) {
		}
			
		}
	
	@DataProvider
	public Object[][] testTEMAdp(){
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 17, 9, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 8, 11.6); // new low
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 7, 13.4); // new low
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 18, 10, 12.1); // new high
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 13.5);

		return new Object[][]{
				//timePeriod = 2;
				//TEMA = [12.50740740740741, 12.320164609053498, 13.35157750342935, 12.182167352537725,
				//			11.291647614692884, 10.962465071381393, 13.38310724991109]
				//Close = [12.4, 12.3, 13.4, 12.1, 11.3, 11.0, 13.5]
				new Object[]{ // test for Position = above
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9, sr10),
						2, Position.above, 1, 0, 3, 7, "2012-8-15 2012-8-17 2012-8-18 2012-8-19" },
				new Object[]{ // test for Position = below
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9, sr10),
						2, Position.below, 1, 0, 3, 7, "2012-8-13 2012-8-14 2012-8-16" }, 
				new Object[]{ // start = 2;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9, sr10),
						2, Position.above, 2, 0, 3, 7, "2012-8-18 2012-8-19" },
				new Object[]{ // end = 2;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9, sr10),
						2, Position.above, 1, 2, 3, 7, "2012-8-15 2012-8-17 2012-8-18" },
				new Object[]{ // start = 2, end = 3;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9, sr10),
						2, Position.above, 2, 3, 3, 7, "2012-8-18 2012-8-19" },
				
				//timePeriod = 3;
				//TEMA = [12.342592592592597, 11.38631365740741, 10.935170717592587, 13.080844907407414]
				//Close = [12.1, 11.3, 11.0, 13.5]
				new Object[]{ // test for Position = above
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9, sr10),
						3, Position.above, 1, 0, 6, 4, "2012-8-18 2012-8-19" },
				new Object[]{ // test for Position = below
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9, sr10),
						3, Position.below, 1, 0, 6, 4, "2012-8-16 2012-8-17" },
				new Object[]{ // start = 2;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9, sr10),
						3, Position.above, 2, 0, 6, 4, "2012-8-19" },
				new Object[]{ // end = 2;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9, sr10),
						3, Position.above, 1, 2, 6, 4, "2012-8-18 2012-8-19" },
		};
	}
	
	@Test(dataProvider = "testTEMAdp")
	public void testTEMA(BarDataWrapper barSeries, int timePeriod, Position position,
			int start, int end, int indexBegin, int resultLength, String expectedDates) throws Exception{
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new TEMACompute(barSeries);
		TEMA tema = null;
		
		if(end == 0){
			tema = new TEMA(ta, timePeriod, position, start);
		} else {
			tema = new TEMA(ta, timePeriod, position, start, end);
		}
		
		FilterResult result = tema.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("TEMA").length, resultLength);
	}
}