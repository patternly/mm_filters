package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.Filter;
import com.marketmemory.filters.TACompute.SMACompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.MA;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

public class TestMA {
	@Test
	public void testInvalidSMAGetData() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new SMACompute(barSeries);
		MA sma;
		
		// Test range <=1
		try {
			sma = new MA(ta, 1, Position.above, 3);
			Assert.fail("Test period=0 fails");
		} catch(IllegalArgumentException e){}
		
		try {
			sma = new MA(ta, 0, Position.above, 3);
			Assert.fail("Test period=0 fails");
		} catch(IllegalArgumentException e){}
	}
	
	@DataProvider
	public Object[][] testSMADp() {
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 13.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 13.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 13.4);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 13.5);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 15.5, 10, 13.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 15.5, 10, 13.7);
		return new Object[][] {
			new Object[] { //Test 0 day
				new BarDataWrapper(), "", 2, Position.above, 1, 1, 0, 0, "" },
			new Object[] { //Test 1 day, above
				new BarDataWrapper(sr1,sr2), "2012-8-9", 2, Position.above, 1, 0, 1, 1, "2012-8-9" },
			new Object[] { //Test 2 days, above
				new BarDataWrapper(sr1,sr2,sr3), "2012-8-9 2012-8-10", 2, Position.above, 1, 0, 1, 2, "2012-8-9 2012-8-10" },
			new Object[] { //Test 3 days, below
				new BarDataWrapper(sr1,sr2,sr3,sr4), "2012-8-9#2012-8-13", 2, Position.below, 1, 0, 1, 3, "2012-8-13" },
			new Object[] { //Test n to infinity (test bigger than n will be selected, test smaller than n won't)
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7), "2012-8-9#2012-8-16", 2, Position.below, 2, 0, 1, 6, "2012-8-14" },
			new Object[] { //Test n to m
				new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12), 
				"2012-8-9#2012-8-21", 2, Position.above, 2, 4, 1, 11, "2012-8-10 2012-8-18#2012-8-20" },
		};
	}
	@Test(dataProvider = "testSMADp")
	public void testSMA(BarDataWrapper barSeries, String dates, int period, Position position, int start, int end, int indexBegin,
			int resultLength, String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new SMACompute(barSeries);
		MA sma = null;
		if(end == 0) {
			sma = new MA(ta, period, position, start);
		} else {
			sma = new MA(ta, period, position, start, end);
		}
		
		FilterResult result = sma.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("maResult").length, resultLength);
	}
	
	@Test
	public void testInvalidSMA() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper();
		TAlib ta = new SMACompute(barSeries);
		try {
			Filter sma = new MA(ta, 2, Position.above, 0);
		} catch(IllegalArgumentException e){}
		
		try {
			Filter sma = new MA(ta, 2, Position.above, 2, 1);
		} catch(IllegalArgumentException e){}
	}
}
