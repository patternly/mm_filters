package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.TACompute.TRIXCompute;
import com.marketmemory.filters.technical.TRIX;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;


public class TestTRIX{
		
	@DataProvider
	public Object[][] testTRIXdp() {
		
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 17, 9, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 7, 13.4);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 18, 10, 12.1);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0);
		StockRecord sr10 = TestConfig.stock_D("2012-8-19", 11, 15.5, 10, 13.5);
		StockRecord sr11 = TestConfig.stock_D("2012-8-20", 11, 15.5, 10, 13.6);
		StockRecord sr12 = TestConfig.stock_D("2012-8-21", 11, 15.5, 10, 14.7);

		return new Object[][]{
//				new Object[]{//Test 0 day
//						new BarDataWrapper(),"", 12, true, 9, Position.above, 1, 0, 0, 0, "" },
				
				//SignalPeriod = 3; timePeriod = 3; sr1 - sr12
				//trix= [[-0.6054037278763191, -1.77629913828381, 0.49175018031242246,
				//2.1100855872425495, 3.6352342033389995]
				//trixSignal = [-0.6299842286159022, 0.7400506793133236, 2.1876424413261617]
				
				//indexBegin = 3(n-1)+1-(m-1); n = timePeriod, m = signalPeriod
				//resultLength = signalLength
				new Object[]{//test trix above center line;SignalPeriod = 3; timePeriod = 3;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						3, false, 3, Position.above, 1, 0, 9, 3, "2012-8-19 2012-8-20 2012-8-21" },
				new Object[]{//test trix below center line;SignalPeriod = 3; timePeriod = 3;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						3, false, 3, Position.below, 1, 0, 9, 3, "" },
				new Object[]{//tet TRIX above signal line ;SignalPeriod = 3; timePeriod = 3;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						3, true, 3, Position.above, 1, 0, 9, 3, "2012-8-19 2012-8-20 2012-8-21" },
				new Object[]{//tet TRIX above signal line; end = 2
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						3, true, 3, Position.above, 1, 2, 9, 3, "2012-8-19 2012-8-20" },
				new Object[]{//tet TRIX above signal line; start = 2
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						3, true, 3, Position.above, 2, 0, 9, 3, "2012-8-20 2012-8-21" },
				new Object[]{//tet TRIX below signal line ;SignalPeriod = 3; timePeriod = 3;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						3, true, 3, Position.below, 1, 0, 9, 3, "" },
				
				//timePeriod = 2, signalPeriod =3
				//trix =[1.4641087623652238, 3.2993708778984887, -0.23445124777135007, -3.139070909337105, 
				//		-3.76875249331442, 3.529201505075741, 4.794022898849581, 5.9248212776431775,
				//trixSgianl = 1.5096761308307876, -0.8146973892531586, -2.291724941283789,
				//		0.6187382818959759, 2.706380590372779, 4.315600934007978
				new Object[]{//tet TRIX above signal line ;SignalPeriod = 3; timePeriod = 2;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						2, true, 3, Position.above, 1, 0, 6, 6, "2012-8-19 2012-8-20 2012-8-21" },
				new Object[]{//tet TRIX above signal line ;SignalPeriod = 3; timePeriod = 2;
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						2, true, 3, Position.below, 1, 0, 6, 6, "2012-8-16 2012-8-17 2012-8-18" },
				new Object[]{//tet TRIX above signal line; start = 2 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						2, true, 3, Position.below, 2, 0, 6, 6, "2012-8-17 2012-8-18" },
				new Object[]{//tet TRIX above signal line; end = 2 
						new BarDataWrapper(sr1,sr2,sr3,sr4,sr5,sr6,sr7,sr8,sr9,sr10,sr11,sr12),
						2, true, 3, Position.below, 1, 2, 6, 6, "2012-8-16 2012-8-17" },
		};
		}
	
	@Test(dataProvider = "testTRIXdp")
	public void testTRIX(BarDataWrapper barSeries, int timePeriod,boolean isSignalLine,
			int signalPeriod, Position position,int start, int end, 
			int indexBegin, int resultLength,String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new TRIXCompute(barSeries);
		TRIX trix = null;

		if (end == 0) {
			trix = new TRIX(ta, timePeriod, isSignalLine, signalPeriod, position, start);
		} else {
			trix = new TRIX(ta, timePeriod, isSignalLine, signalPeriod, position, start, end);
		}
		
		FilterResult result = trix.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("trixResult").length, resultLength);
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testInvalidTRIX() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new TRIXCompute(barSeries);
		TRIX trix;
		
		// Test time period less than 1.
		try {
			trix = new TRIX(ta, 1 , true, 2, Position.above, 1);
			Assert.fail("All periods must be larger than 1");
		} catch(IllegalArgumentException e){}
		
		// Test signal period less than 1.
		try {
			trix = new TRIX(ta, 2 , true, 1, Position.above, 1);
			Assert.fail("All periods must be larger than 1");
		} catch(IllegalArgumentException e){}
		
		
		// start must be larger than 0.
		try {
			trix = new TRIX(ta, 2 , true, 2, Position.above, 0);
			Assert.fail("start must be larger than 0");
		} catch(IllegalArgumentException e){}
		
		//end smaller than start.
		try {
			trix = new TRIX(ta, 2 , true, 2, Position.above, 3 , 1);
			Assert.fail("start must be smaller than end");
		} catch(IllegalArgumentException e){}
	}
}