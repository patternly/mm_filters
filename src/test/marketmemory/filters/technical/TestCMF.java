package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.CMFCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.CMF;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.StockRecord;

/**
 * Test suite for CMF filter
 * 
 * @author Johnny Jiang
 * @lastUpdated 2015-06-06
 *
 */
public class TestCMF {

	@SuppressWarnings("unused")
	@Test
	public void testInvalidCMF() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3, 100));
		TAlib ta = new CMFCompute(barSeries);
		CMF cmf;

		// start smaller than 0.
		try {
			cmf = new CMF(ta, 2, 0, 0, 0);
			Assert.fail("start must be greater than 0");
		} catch (IllegalArgumentException e) {
		}

		// end smaller than start.
		try {
			cmf = new CMF(ta, 2, 0, 0, 2, 1);
			Assert.fail("start must be smaller than end");
		} catch (IllegalArgumentException e) {
		}

		// period smaller than 2.
		try {
			cmf = new CMF(ta, 1, 0, 0, 1);
			Assert.fail("Time period must be greater than 1");
		} catch (IllegalArgumentException e) {
		}

		// low limit larger than high limit.
		try {
			cmf = new CMF(ta, 2, 1, 0, 1);
			Assert.fail("Low limit must be smaller than high limit");
		} catch (IllegalArgumentException e) {
		}

		// low limit not in valid range.
		try {
			cmf = new CMF(ta, 2, -2, 0, 1);
			Assert.fail("Low limit must be between -100 and 0");
		} catch (IllegalArgumentException e) {
		}

		// high limit not in valid range.
		try {
			cmf = new CMF(ta, 2, 0, 2, 1);
			Assert.fail("High limit must be between -100 and 0");
		} catch (IllegalArgumentException e) {
		}
	}

	@DataProvider
	public Object[][] testCMFdp() {

		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3, 100);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 10, 11.6, 200);
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6, 250);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4, 100);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3, 50);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 10, 13.4, 150);
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 15.5, 10, 12.1, 200);
		StockRecord sr8 = TestConfig.stock_D("2012-8-17", 11, 15.5, 10, 11.3, 100);
		StockRecord sr9 = TestConfig.stock_D("2012-8-18", 11, 15.5, 10, 11.0, 150);

		return new Object[][] {

				// Period = 2
				// AD = [-87.14, -30.77, -26.363, -20.90, 27.27, -11.81, -100.0, -148.1]
				// Sum = [300.0, 450.0, 350.0, 150.0, 200.0, 350.0, 300.0, 250.0]
				// CMF = [-0.290, -0.0683, -0.0753, -0.139, 0.136, -0.0337, -0.333, -0.592]
				new Object[] { 
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9), 
						2, -0.5, 0, 1, 0, 1, 8, "2012-8-9 2012-8-10 2012-8-13 2012-8-14 2012-8-16 2012-8-17"},
				new Object[] { //test filtering last element of bar series
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9), 
						2, -1, -0.5, 1, 0, 1, 8, "2012-8-18"}, 
				new Object[] { //test filtering above 0
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9), 
						2, 0, 1, 1, 0, 1, 8, "2012-8-15"}, 
				new Object[] { //test start = 2
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9), 
						2, -0.5, 0, 2, 0, 1, 8, "2012-8-10 2012-8-13 2012-8-14 2012-8-17"}, 
				new Object[] { //test start = 2, end = 3
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9), 
						2, -0.5, 0, 2, 3, 1, 8, "2012-8-10 2012-8-13 2012-8-17"}, 
				new Object[] { //test filtering over the critical value 0; 
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9), 
						2, -0.1, 0.1, 1, 0, 1, 8, "2012-8-10 2012-8-13 2012-8-16"}, 
						
				// Period = 3
				// CMF = [-0.183, -0.0791, -0.0863, 0.0484, -0.0499, -0.143, -0.434]
				new Object[] { 
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9), 
						3, -0.5, 0, 1, 0, 2, 7, "2012-8-10 2012-8-13 2012-8-14 2012-8-16 2012-8-17 2012-8-18"},
				new Object[] { 
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9), 
						3, 0, 0.5, 1, 0, 2, 7, "2012-8-15"},
				new Object[] {//test start = 2
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9), 
						3, -0.5, 0, 2, 0, 2, 7, "2012-8-13 2012-8-14 2012-8-17 2012-8-18"},
				new Object[] {//test start = 2, end = 2;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9), 
						3, -0.5, 0, 2, 2, 2, 7, "2012-8-13 2012-8-17"},
				new Object[] {//test filtering across center line 0;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7, sr8, sr9), 
						3, -0.1, 0.1, 1, 0, 2, 7, "2012-8-13 2012-8-14 2012-8-15 2012-8-16"},
		};
	
	}

	@Test(dataProvider = "testCMFdp")
	public void testCMF(BarDataWrapper barSeries, int timePeriod, double lowLimit, double highLimit, int start,
			int end, int indexBegin, int resultLength, String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);

		TAlib ta = new CMFCompute(barSeries);
		CMF cmf = null;

		if (end == 0) {
			cmf = new CMF(ta, timePeriod, lowLimit, highLimit, start);
		} else {
			cmf = new CMF(ta, timePeriod, lowLimit, highLimit, start, end);
		}

		FilterResult result = cmf.buildFilterResults(barSeries);

		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("CMFResult").length, resultLength);
	}

}