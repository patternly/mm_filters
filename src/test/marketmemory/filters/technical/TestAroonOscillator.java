package test.marketmemory.filters.technical;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import test.patternly.TestConfig;

import com.marketmemory.filters.TACompute.AroonOscillatorCompute;
import com.marketmemory.filters.TACompute.TAlib;
import com.marketmemory.filters.technical.AroonOscillator;
import com.patternly.datasource.BarDataWrapper;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.Position;
import com.patternly.util.StockRecord;

public class TestAroonOscillator{
	
	@DataProvider
	public Object[][] testAroonOscDP(){
		
		StockRecord sr1 = TestConfig.stock_D("2012-8-8", 12, 17, 9, 11.3);
		StockRecord sr2 = TestConfig.stock_D("2012-8-9", 11, 13.5, 8, 11.6); //new low
		StockRecord sr3 = TestConfig.stock_D("2012-8-10", 11, 15.5, 10, 12.6);
		StockRecord sr4 = TestConfig.stock_D("2012-8-13", 11, 15.5, 10, 12.4);
		StockRecord sr5 = TestConfig.stock_D("2012-8-14", 11, 15.5, 10, 12.3);
		StockRecord sr6 = TestConfig.stock_D("2012-8-15", 11, 15.5, 7, 13.4); // new low
		StockRecord sr7 = TestConfig.stock_D("2012-8-16", 11, 18, 10, 12.1); // new high

		
		return new Object[][]{
				new Object[]{ //test 0th day
						new BarDataWrapper(), 3, 0, Position.above, 1, 0,
						0, 0, ""},
						
				//timePeriod = 3;
				//AroonOsc = [-33.333333333333336, 100.0, 0.0, 33.333333333333336]
				new Object[]{ //test timePeriod=3; signalLine=0; above signalLine;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7), 3, 0, 
						Position.above, 1, 0, 3, 4, "2012-8-14 2012-8-16"},
				new Object[]{ //test timePeriod=3; signalLine=0; below signalLine;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7), 3, 0,
						Position.below, 1, 0, 3, 4, "2012-8-13"},
				new Object[]{ //test timePeriod=3; signalLine=50; above signalLine;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7), 3, 50, 
						Position.above, 1, 0, 3, 4, "2012-8-14"},
				new Object[]{ //test timePeriod=3; signalLine=50; below signalLine;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7), 3, 50, 
						Position.below, 1, 0, 3, 4, "2012-8-13 2012-8-15 2012-8-16"},
				new Object[]{ //test start = 2; signalLine=50; below signalLine;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7), 3, 50, 
						Position.below, 2, 0, 3, 4, "2012-8-16"},
				
				//timePeriod = 2;
				//AroonOsc = [-50.0, 100.0, 0.0, 0.0, 50.0]
				new Object[]{ //test  signalLine=-10; above signalLine;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7), 2, -10, 
						Position.above, 1, 0, 2, 5, "2012-8-13 2012-8-14 2012-8-15 2012-8-16"},
				new Object[]{ //test signalLine = -10; below signalLine;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7), 2, -10, 
						Position.below, 1, 0, 2, 5, "2012-8-10"},
				new Object[]{ //test end = 3; signalLine=-10; above signalLine;
						new BarDataWrapper(sr1, sr2, sr3, sr4, sr5, sr6, sr7), 2, -10, 
						Position.above, 1, 3, 2, 5, "2012-8-13 2012-8-14 2012-8-15"},											
		};
	}
	
	@Test(dataProvider = "testAroonOscDP")
	public void testAroonOsc(BarDataWrapper barSeries, int timePeriod, double signalLine,
			Position position, int start, int end, 
			int indexBegin, int resultLength,String expectedDates) throws Exception {
		DateList expected = new DateList(expectedDates);
		
		TAlib ta = new AroonOscillatorCompute(barSeries);
		AroonOscillator aroonOsc = null;

		if (end == 0) {
			aroonOsc = new AroonOscillator(ta, timePeriod, signalLine, position, start);
		} else {
			aroonOsc = new AroonOscillator(ta, timePeriod, signalLine, position, start, end);
		}
		
		FilterResult result = aroonOsc.buildFilterResults(barSeries);
		
		Assert.assertEquals(result.getIndexBegin(), indexBegin);
		Assert.assertEquals(result.getResultLength(), resultLength);
		Assert.assertEquals(result.getDateList(), expected);
		Assert.assertEquals(result.getResults().get("aroonOscillator").length, resultLength);
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testInvalidAroonOsc() throws Exception {
		BarDataWrapper barSeries = new BarDataWrapper(TestConfig.stock_D("2012-8-8", 12, 13, 11, 11.3));
		TAlib ta = new AroonOscillatorCompute(barSeries);
		AroonOscillator aroonOsc;
		
		// Test time period less than 1.
		try {
			aroonOsc = new AroonOscillator(ta, 1 , 0, Position.above, 1);
			Assert.fail("Time period must be larger than 1");
		} catch(IllegalArgumentException e){}
		
		// start must be larger than 0.
		try {
			aroonOsc = new AroonOscillator(ta, 2 , 0, Position.above, 0);
			Assert.fail("start must be larger than 0");
		} catch(IllegalArgumentException e){}
		
		//end smaller than start.
		try {
			aroonOsc = new AroonOscillator(ta, 2 , 0, Position.above, 2, 1);
			Assert.fail("start must be smaller than end");
		} catch(IllegalArgumentException e){}
		
		// signal line must be between -100 and 100
		try {
			aroonOsc = new AroonOscillator(ta, 2 , -128, Position.above, 1);
			Assert.fail("Invalid value of signal line");
		} catch(IllegalArgumentException e){}
		
		
	}
	
}