package test.marketmemory.filters.event;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.marketmemory.filters.event.CorporateEvents;
import com.patternly.datasource.SymbolDataManager;
import com.patternly.datasource.barchart.CsvDividendBean;
import com.patternly.datasource.barchart.CsvEarningBean;
import com.patternly.datasource.barchart.CsvSplitBean;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.CorporateEventsType;
import com.patternly.util.MultiTimeFrame;
import com.patternly.util.TimeCoverage;

public class TestCorporateEvents {
	SymbolDataManager mockDataService;
	
	@DataProvider
	public Object[][] testCorporateEventsDp() {
		
		CsvEarningBean earning1 = new CsvEarningBean();
		earning1.setDatetime(DateList.convertToDate("2012-7-21"));
		CsvEarningBean earning2 = new CsvEarningBean();
		earning2.setDatetime(DateList.convertToDate("2012-7-28"));
		CsvEarningBean earning3 = new CsvEarningBean();
		earning3.setDatetime(DateList.convertToDate("2012-8-21"));
		
		CsvDividendBean dividend1 = new CsvDividendBean();
		dividend1.setDatetime(DateList.convertToDate("2011-7-21"));
		CsvDividendBean dividend2 = new CsvDividendBean();
		dividend2.setDatetime(DateList.convertToDate("2011-8-28"));
		
		CsvSplitBean split1 = new CsvSplitBean();
		split1.setSplit("1-2");
		split1.setDatetime(DateList.convertToDate("2011-7-20"));
		CsvSplitBean split2 = new CsvSplitBean();
		split2.setSplit("1-3");
		split2.setDatetime(DateList.convertToDate("2011-8-2"));
		CsvSplitBean split3 = new CsvSplitBean();
		split3.setSplit("4-1");
		split3.setDatetime(DateList.convertToDate("2011-9-1"));
		
		List<CsvEarningBean> earningList1 = new ArrayList<CsvEarningBean>();
		earningList1.add(earning1);
		
		List<CsvEarningBean> earningList2 = new ArrayList<CsvEarningBean>();
		earningList2.add(earning1);
		earningList2.add(earning2);
		earningList2.add(earning3);
		
		List<CsvDividendBean> dividendList1 = new ArrayList<CsvDividendBean>();
		dividendList1.add(dividend1);
		
		List<CsvDividendBean> dividendList2 = new ArrayList<CsvDividendBean>();
		dividendList2.add(dividend1);
		dividendList2.add(dividend2);
		
		List<CsvSplitBean> splitList1 = new ArrayList<CsvSplitBean>();
		splitList1.add(split1);
		
		List<CsvSplitBean> splitList2 = new ArrayList<CsvSplitBean>();
		splitList2.add(split1);
		splitList2.add(split2);
		splitList2.add(split3);
		
		return new Object[][] {
				new Object[] { "2012-7-20", "2012-9-21", earningList1, null, null, CorporateEventsType.Earnings,
						new String[] {"2012-7-21"} },
				new Object[] { "2012-7-20", "2012-9-21", earningList2, null, null, CorporateEventsType.Earnings,
						new String[] {"2012-7-21", "2012-7-28", "2012-8-21"} },
				new Object[] { "2011-7-20", "2011-9-21", earningList1, new ArrayList<CsvDividendBean>(), null,
						CorporateEventsType.Dividend, new String[] {} },
				new Object[] { "2011-7-20", "2011-9-21", earningList1, dividendList1, null, CorporateEventsType.Dividend,
						new String[] {"2011-7-21"} },
				new Object[] { "2011-7-20", "2011-9-21", earningList1, dividendList2, null, CorporateEventsType.Dividend,
						new String[] {"2011-7-21", "2011-8-28"} },
				new Object[] { "2011-7-20", "2011-9-21", earningList1, null, new ArrayList<CsvSplitBean>(),
						CorporateEventsType.Split, new String[] {} },
				new Object[] { "2011-7-20", "2011-9-21", earningList1, null, splitList1, CorporateEventsType.Split,
						new String[] {"2011-7-20"} },
				new Object[] { "2011-7-20", "2011-9-21", earningList1, null, splitList2, CorporateEventsType.Split,
						new String[] {"2011-7-20", "2011-8-2"} },
				new Object[] { "2011-7-20", "2011-9-21", earningList1, null, splitList1, CorporateEventsType.ReverseSplit,
						new String[] {} },
				new Object[] { "2011-7-20", "2011-9-21", earningList1, null, splitList2, CorporateEventsType.ReverseSplit,
						new String[] {"2011-9-1"} },
		};
	}
	@BeforeMethod(groups = {"g1"}, alwaysRun = false)
	public void setUpEarning(){
		this.mockDataService = mock(SymbolDataManager.class);
	}
	@Test(dataProvider = "testCorporateEventsDp", groups = {"g1"})
	public void testCorporateEvents(String startDate, String endDate, List<CsvEarningBean> mockeRarningResult,
			List<CsvDividendBean> mockeDividendResult, List<CsvSplitBean> mockeSplitResult,
			CorporateEventsType ceventsType, String[] expectedResult) throws Exception {
		
		DateTime start = DateList.convertToDate(startDate);
		DateTime end = DateList.convertToDate(endDate);
		
		BarIndexSeries dl = new BarIndexSeries(start, end, null, new TimeCoverage(MultiTimeFrame.day, 1));
		CorporateEvents filter = new CorporateEvents(mockDataService, ceventsType);
		
		when(mockDataService.getEarnings(start, end)).thenReturn(mockeRarningResult);
		when(mockDataService.getDividend(start, end)).thenReturn(mockeDividendResult);
		when(mockDataService.getSplit(start, end)).thenReturn(mockeSplitResult);

		DateList expected = new DateList(expectedResult);
		
		dl = new BarIndexSeries(start, end, null, new TimeCoverage(MultiTimeFrame.day, 1));
		FilterResult result = filter.buildFilterResults(dl);
		Assert.assertEquals(result.getDateList(), expected);
	}
	
}
