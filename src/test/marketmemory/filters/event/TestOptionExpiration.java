package test.marketmemory.filters.event;

import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.marketmemory.filters.event.OptionExpiration;
import com.patternly.filters.models.BarIndexSeries;
import com.patternly.filters.models.DateList;
import com.patternly.filters.models.FilterResult;
import com.patternly.util.MultiTimeFrame;
import com.patternly.util.TimeCoverage;

public class TestOptionExpiration {
	@DataProvider
	public Object[][] testOptionExpirationDp() {
		return new Object[][] {
				new Object[] { "2012-7-20", "2012-9-21", new String[] 
						{ "2012-7-20", "2012-8-17", "2012-9-21"} },
				
				new Object[] { "2012-7-21", "2012-9-20", new String[]
						{ "2012-8-17"} },
		};
	}
	@Test(dataProvider = "testOptionExpirationDp")
	public void testOptionExpiration(String startDate, String endDate, String[] expectedResult) throws Exception {
		DateTime start = DateList.convertToDate(startDate);
		DateTime end = DateList.convertToDate(endDate);
		BarIndexSeries dl = new BarIndexSeries(start, end, null, new TimeCoverage(MultiTimeFrame.day, 1));
		OptionExpiration filter = new OptionExpiration();
		DateList expected = new DateList(expectedResult);
		
		FilterResult result = filter.buildFilterResults(dl);
		Assert.assertEquals(result.getDateList(), expected);
	}
	
}
