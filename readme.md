# MarketMemory Filters Library

***
## How to setup developing environment
### Install Eclipse
* Download eclipse from <http://www.eclipse.org>

### Install Eclipse Plugins
* In eclipse, open “Help -> Install new software”
* add source:http://beust.com/eclipse as TestNG
* select --All available sites--
* Select
    * Web, XML, Java EE Development and OSGi Enterprise Development, 
    * TestNG 
    * Collaboration -> choose all git related options

### Setup MM Filters Library
* Navigate to your eclipse workspace folder `cd /path/to/eclipse_workspace`
* Download the project source code `git clone git@bitbucket.org:patternly/patternly_wpsite.git`
* Open eclipse
* File -> New -> Java project
* In the new java project window, uses "mm_filters" as the project name. Eclipse should notify that the project already exists
* Click finish, you will see the mm_filters project is created and showed on the Package Explorer view
* Right click on the project, choose build path -> Configure build path... 
* Under the Libraries tab, click Add Library... , then choose TestNG

### Setup the Unit Tests
#### Setup Database
* Install mysql
* Create database by running install.sql script
* Create tables by running create_table.sql script

#### Running the Unit Tests
* Right click on the unit test file (For example, TestMACD.java)
* choose run as -> TestNG Test
* In the console window (normal in the bottom of eclipse), if you see "Total tests run: 120, Failures: 0, Skips: 0", then your project has been setup properly.