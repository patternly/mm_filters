# install database and users for development environment.

CREATE USER econoday;
CREATE DATABASE econoday;
GRANT ALL ON econoday.* TO econoday@localhost IDENTIFIED BY "ec0N#lio";

CREATE USER financial_wolf;
CREATE DATABASE financial_wolf;
GRANT ALL ON financial_wolf.* TO financial_wolf@localhost IDENTIFIED BY "d8dd633e0e6f";

USE econoday;
CREATE TABLE IF NOT EXISTS `econoday_event` (
 id INT,
 `date_published` date ,
 `name` VARCHAR(50) NOT NULL,
 `date_scraped` date 
);

USE financial_wolf;