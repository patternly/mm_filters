# install database tables for any environment

CREATE TABLE IF NOT EXISTS `stock_1` (
 `symbol` varchar(20) NOT NULL,
 `datetime`  DATETIME NOT NULL,
 `open` float NOT NULL,
 `high` float NOT NULL,
 `low` float NOT NULL,
 `close` float NOT NULL,
 PRIMARY KEY (`symbol`,`datetime`)
);

CREATE TABLE IF NOT EXISTS `stock_5` (
 `symbol` varchar(20) NOT NULL,
 `datetime`  DATETIME NOT NULL,
 `open` float NOT NULL,
 `high` float NOT NULL,
 `low` float NOT NULL,
 `close` float NOT NULL,
 PRIMARY KEY (`symbol`,`datetime`)
);

CREATE TABLE IF NOT EXISTS `stock_10` (
 `symbol` varchar(20) NOT NULL,
 `datetime`  DATETIME NOT NULL,
 `open` float NOT NULL,
 `high` float NOT NULL,
 `low` float NOT NULL,
 `close` float NOT NULL,
 PRIMARY KEY (`symbol`,`datetime`)
);

CREATE TABLE IF NOT EXISTS `stock_15` (
 `symbol` varchar(20) NOT NULL,
 `datetime`  DATETIME NOT NULL,
 `open` float NOT NULL,
 `high` float NOT NULL,
 `low` float NOT NULL,
 `close` float NOT NULL,
 PRIMARY KEY (`symbol`,`datetime`)
);

CREATE TABLE IF NOT EXISTS `stock_0` (
 `symbol` varchar(20) NOT NULL,
 `datetime`  DATETIME NOT NULL,
 `open` float NOT NULL,
 `high` float NOT NULL,
 `low` float NOT NULL,
 `close` float NOT NULL,
 PRIMARY KEY (`symbol`,`datetime`)
);